﻿using System;

namespace Modelo.Dto {

    public class CoreEnteDto {

        public virtual long Cente { get; set; }
        public virtual string Tipoidentificacioncdetalle { get; set; }
        public virtual string Identificacion { get; set; }
        public virtual string Primernombre { get; set; }
        public virtual string Segundonombre { get; set; }
        public virtual string Primerapellido { get; set; }
        public virtual string Segundoapellido { get; set; }
        public virtual string Nombre { get; set; }
        public virtual DateTime Fnacimiento { get; set; }
        public virtual string Celular { get; set; }
        public virtual string Email { get; set; }

    }

}
