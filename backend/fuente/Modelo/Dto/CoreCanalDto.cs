﻿namespace Modelo.Dto {

    public class CoreCanalDto {

        public virtual int Longitud { get; set; }
        public virtual int Repeticiones { get; set; }
        public virtual int Numeros { get; set; }
        public virtual int Especiales { get; set; }
        public virtual int Minusculas { get; set; }
        public virtual int Mayusculas { get; set; }

    }

}
