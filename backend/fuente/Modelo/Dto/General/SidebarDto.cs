﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Dto.General {
    public class SidebarDto {
        public virtual int Cmenu { get; set; }
        public virtual string Nombre { get; set; }
        public virtual int Orden { get; set; }
        public virtual string Icono { get; set; }
        public virtual int Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        public virtual bool Sidebaractive { get; set; }
        public virtual bool Notificacion { get; set; }
        public virtual bool Cargarpantalla { get; set; }
        public virtual bool Essubmenu { get; set; }
        public virtual int Numnotifiacion { get; set; }
        public virtual bool Mostrar { get; set; }

        public virtual IList<MenuDto> lmenu { get; set; } = new List<MenuDto>();

    }
}
