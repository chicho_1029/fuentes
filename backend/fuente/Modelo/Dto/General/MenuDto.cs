﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Modelo.Dto.General {

    public class MenuDto {

        private string label;
        private string icon;
        private string target;
        private string command;
        private bool expanded;
        private IList<MenuDto> items = new List<MenuDto>();

        public MenuDto(string label, string icon, string target, string command, bool expanded) {
            this.label = label;
            this.icon = icon;
            this.target = target;
            this.command = command;
            this.expanded = expanded;
        }

        [JsonProperty(PropertyName = "label")]
        public virtual string Label { get => label; set => label = value; }

        [JsonProperty(PropertyName = "icon")]
        public virtual string Icon { get => icon; set => icon = value; }

        [JsonProperty(PropertyName = "target")]
        public virtual string Target { get => target; set => target = value; }

        [JsonProperty(PropertyName = "command")]
        public virtual string Command { get => command; set => command = value; }

        [JsonProperty(PropertyName = "expanded")]
        public virtual bool Expanded { get => expanded; set => expanded = value; }

        [JsonProperty(PropertyName = "items")]
        public virtual IList<MenuDto> Items { get => items; set => items = value; }

    }

}
