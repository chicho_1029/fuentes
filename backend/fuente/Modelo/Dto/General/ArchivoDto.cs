﻿using Newtonsoft.Json;
using System;

namespace Modelo.Dto.General {

    [Serializable]
    public class ArchivoDto {

        private int? codigo;
        private string nombre;
        private string extension;
        private decimal? tamanio;
        private byte[] archivobytes;

        [JsonProperty(PropertyName = "Codigo")]
        public int? Codigo { get => codigo; set => codigo = value; }

        [JsonProperty(PropertyName = "Nombre")]
        public string Nombre { get => nombre; set => nombre = value; }

        [JsonProperty(PropertyName = "extension")]
        public string Extension { get => extension; set => extension = value; }

        [JsonProperty(PropertyName = "Tamanio")]
        public decimal? Tamanio { get => tamanio; set => tamanio = value; }

        [JsonProperty(PropertyName = "Archivobytes")]
        public byte[] Archivobytes { get => archivobytes; set => archivobytes = value; }

    }
}
