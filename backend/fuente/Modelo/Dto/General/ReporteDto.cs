﻿using Newtonsoft.Json;

namespace Modelo.Dto.General {

    public class ReporteDto {

        public virtual string Servidor { get; set; }
        public virtual string Bdd { get; set; }
        public virtual string Usuario { get; set; }
        public virtual string Password { get; set; }

    }

    public class ReporteParametroDto {

        private string parametro;
        private object valor;

        public ReporteParametroDto(string name, object value) {
            parametro = name;
            valor = value;
        }

        [JsonProperty(PropertyName = "parametro")]
        public virtual string Nombre { get => parametro; set => parametro = value; }

        [JsonProperty(PropertyName = "value")]
        public virtual object Valor { get => valor; set => valor = value; }

    }

}
