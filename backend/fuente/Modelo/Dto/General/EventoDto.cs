﻿using Newtonsoft.Json;
using System;

namespace Modelo.Dto.General {

    [Serializable]
    public class EventoDto {

        private string id;
        private string evento;
        private string fechaCalendario;

        [JsonProperty(PropertyName = "id")]
        public string Id { get => id; set => id = value; }

        [JsonProperty(PropertyName = "title")]
        public string Evento { get => evento; set => evento = value; }

        [JsonProperty(PropertyName = "start")]
        public string FechaCalendario { get => fechaCalendario; set => fechaCalendario = value; }

    }
}
