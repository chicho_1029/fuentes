﻿using Newtonsoft.Json;

namespace Modelo.Dto.General {

    public class CatalogoDto {

        private string etiqueta;
        private object valor;

        public CatalogoDto() { }

        public CatalogoDto(string label, object value) {
            etiqueta = label;
            valor = value;
        }

        [JsonProperty(PropertyName = "label")]
        public virtual string Label { get => etiqueta; set => etiqueta = value; }

        [JsonProperty(PropertyName = "value")]
        public virtual object Value { get => valor; set => valor = value; }

    }

}
