﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Modelo.Dto.General {

    [Serializable]
    public class ChartDto {

        private IList<string> labels = new List<string>();
        private IList<ChartDataDto> datasets = new List<ChartDataDto>();

        [JsonProperty(PropertyName = "labels")]
        public IList<string> Labels { get => labels; set => labels = value; }

        [JsonProperty(PropertyName = "datasets")]
        public IList<ChartDataDto> Datasets { get => datasets; set => datasets = value; }

    }

    [Serializable]
    public class ChartDataDto {

        private string label;
        private bool fill = false;
        private IList<decimal> data = new List<decimal>();
        private IList<string> borderColor = new List<string>();
        private IList<string> backgroundColor = new List<string>();
        private IList<string> hoverBackgroundColor = new List<string>();

        [JsonProperty(PropertyName = "label")]
        public string Label { get => label; set => label = value; }

        [JsonProperty(PropertyName = "fill")]
        public bool Fill { get => fill; set => fill = value; }

        [JsonProperty(PropertyName = "data")]
        public IList<decimal> Data { get => data; set => data = value; }

        [JsonProperty(PropertyName = "borderColor")]
        public IList<string> BorderColor { get => borderColor; set => borderColor = value; }

        [JsonProperty(PropertyName = "backgroundColor")]
        public IList<string> BackgroundColor { get => backgroundColor; set => backgroundColor = value; }

        [JsonProperty(PropertyName = "hoverBackgroundColor")]
        public IList<string> HoverBackgroundColor { get => hoverBackgroundColor; set => hoverBackgroundColor = value; }

    }

}
