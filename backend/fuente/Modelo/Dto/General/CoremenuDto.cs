﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Dto.General {
    public class CoremenuDto {
        public virtual int Cmenu { get; set; }
        public virtual int Cmenupadre { get; set; }
        public virtual short Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Icono { get; set; }
        public virtual int Orden { get; set; }
        public virtual bool Mostrar { get; set; }
        public virtual bool Activado { get; set; }
        public virtual bool Izquierda { get; set; }
    }
}
