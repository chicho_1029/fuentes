using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coremensaje {
        public virtual string Cmensaje { get; set; }
        public virtual string Tipo { get; set; }
        public virtual string Mensaje { get; set; }
    }
}
