using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreperfilrol {
        public virtual short Crol { get; set; }
        public virtual int Perfil { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coreperfilrol;
			if (t == null) return false;
			if (Crol == t.Crol
			 && Perfil == t.Perfil)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Crol.GetHashCode();
			hash = (hash * 397) ^ Perfil.GetHashCode();

			return hash;
        }
        #endregion
    }
}
