using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreclasetransaccion {
        public virtual short Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        public virtual string Cclase { get; set; }
        public virtual string Centidad { get; set; }
        public virtual int Secuencia { get; set; }
        public virtual bool Consulta { get; set; }
        public virtual string Permiso { get; set; }
        public virtual string Metodo { get; set; }
        public virtual bool Parametro { get; set; }
        public virtual bool Activado { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coreclasetransaccion;
			if (t == null) return false;
			if (Cmodulo == t.Cmodulo
			 && Ctransaccion == t.Ctransaccion
			 && Cclase == t.Cclase
			 && Centidad == t.Centidad
			 && Secuencia == t.Secuencia)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Cmodulo.GetHashCode();
			hash = (hash * 397) ^ Ctransaccion.GetHashCode();
			hash = (hash * 397) ^ Cclase.GetHashCode();
			hash = (hash * 397) ^ Centidad.GetHashCode();
			hash = (hash * 397) ^ Secuencia.GetHashCode();

			return hash;
        }
        #endregion
    }
}
