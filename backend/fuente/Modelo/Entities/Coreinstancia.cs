using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreinstancia {
        public virtual short Cinstancia { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Servidor { get; set; }
        public virtual string Base { get; set; }
        public virtual string Usuario { get; set; }
        public virtual string Password { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
    }
}
