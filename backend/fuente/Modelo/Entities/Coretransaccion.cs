using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coretransaccion {
        public Coretransaccion() { }
        public virtual short Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Ruta { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coretransaccion;
			if (t == null) return false;
			if (Cmodulo == t.Cmodulo
			 && Ctransaccion == t.Ctransaccion)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Cmodulo.GetHashCode();
			hash = (hash * 397) ^ Ctransaccion.GetHashCode();

			return hash;
        }
        #endregion
    }
}
