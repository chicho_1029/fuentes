using System;
using System.Text;
using System.Collections.Generic;
using Modelo.Util;

namespace Modelo.Entities {
    
    public class Coreusuariosesion : IEntity {
        public virtual string Cusuario { get; set; }
        public virtual short Cmodulologin { get; set; }
        public virtual string Token { get; set; }
        public virtual int Fsistema { get; set; }
        public virtual string Terminal { get; set; }
        public virtual string Terminallocal { get; set; }
        public virtual string Navegador { get; set; }
        public virtual DateTime? Finicio { get; set; }
        public virtual short? Tiemposesion { get; set; }
        public virtual DateTime? Fultimaaccion { get; set; }
        public virtual object Clone() {
            return this.MemberwiseClone();
        }
    }
}
