using System;
using System.Text;
using System.Collections.Generic;
using Modelo.Util;

namespace Modelo.Entities {
    
    public class Coreforti : IEntity {
        public virtual string Cusuarioforti { get; set; }
        public virtual short? Cmodulo { get; set; }
        public virtual int? Ctransaccion { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual string Token { get; set; }
        public virtual string Identificador { get; set; }
        public virtual string Identificacion { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Celular { get; set; }
        public virtual string Correo { get; set; }
        public virtual DateTime? Fcaducidad { get; set; }

        public virtual object Clone() {
            return this.MemberwiseClone();
        }
    }
}
