using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreusuariootp {
        public virtual long Cotp { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual short Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        public virtual string Codigootp { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual DateTime Fcaducidad { get; set; }
        public virtual bool Validado { get; set; }
        public virtual DateTime? Fvalidado { get; set; }
    }
}
