using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Corecorreo {
        public Corecorreo() { }
        public virtual short Ccorreo { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Direccion { get; set; }
        public virtual string Remitente { get; set; }
        public virtual string Password { get; set; }
        public virtual string Hostsmtp { get; set; }
        public virtual int Puertosmtp { get; set; }
        public virtual string Hostimap { get; set; }
        public virtual int? Puertoimap { get; set; }
        public virtual bool Ssl { get; set; }
    }
}
