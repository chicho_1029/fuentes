using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreente {
        public virtual long Cente { get; set; }
        public virtual string Tipoente { get; set; }
        public virtual int Tipoidentificacionccatalogo { get; set; }
        public virtual string Tipoidentificacioncdetalle { get; set; }
        public virtual string Identificacion { get; set; }
        public virtual string Primernombre { get; set; }
        public virtual string Segundonombre { get; set; }
        public virtual string Primerapellido { get; set; }
        public virtual string Segundoapellido { get; set; }
        public virtual string Nombre { get; set; }
        public virtual DateTime? Fnacimiento { get; set; }
        public virtual string Celular { get; set; }
        public virtual string Email { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
    }
}
