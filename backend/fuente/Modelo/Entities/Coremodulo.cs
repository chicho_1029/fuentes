using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coremodulo {
        public Coremodulo() { }
        public virtual short Cmodulo { get; set; }
        public virtual string Nombre { get; set; }
        public virtual bool Activado { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cclase { get; set; }
        public virtual string Icono { get; set; }
    }
}
