using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreparametro {
        public virtual string Cparametro { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Valor { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
    }
}
