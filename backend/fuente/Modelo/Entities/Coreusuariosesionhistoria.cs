using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreusuariosesionhistoria {
        public virtual string Cusuario { get; set; }
        public virtual string Token { get; set; }
        public virtual bool Ingreso { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Terminal { get; set; }
        public virtual string Terminallocal { get; set; }
        public virtual string Navegador { get; set; }
        public virtual short? Cmodulologin { get; set; }
        public virtual DateTime? Finicio { get; set; }
        public virtual DateTime? Fsalida { get; set; }
        public virtual string Radicacion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coreusuariosesionhistoria;
			if (t == null) return false;
			if (Cusuario == t.Cusuario
			 && Token == t.Token
			 && Ingreso == t.Ingreso)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Cusuario.GetHashCode();
			hash = (hash * 397) ^ Token.GetHashCode();
			hash = (hash * 397) ^ Ingreso.GetHashCode();

			return hash;
        }
        #endregion
    }
}
