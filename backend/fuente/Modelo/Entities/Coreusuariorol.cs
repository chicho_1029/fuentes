using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreusuariorol {
        public virtual short Crol { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coreusuariorol;
			if (t == null) return false;
			if (Crol == t.Crol
			 && Cusuario == t.Cusuario)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Crol.GetHashCode();
			hash = (hash * 397) ^ Cusuario.GetHashCode();

			return hash;
        }
        #endregion
    }
}
