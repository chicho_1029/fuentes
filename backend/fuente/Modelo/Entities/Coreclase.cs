using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreclase {
        public Coreclase() { }
        public virtual string Cclase { get; set; }
        public virtual string Tipo { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Clasenegocio { get; set; }
        public virtual bool Activado { get; set; }
    }
}
