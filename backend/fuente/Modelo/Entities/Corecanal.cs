using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Corecanal {
        public Corecanal() { }
        public virtual string Ccanal { get; set; }
        public virtual string Nombre { get; set; }
        public virtual bool Activado { get; set; }
        public virtual short? Longitud { get; set; }
        public virtual short? Diasvalidez { get; set; }
        public virtual short? Diasmensajeinvalidez { get; set; }
        public virtual short? Intentos { get; set; }
        public virtual short? Repeticiones { get; set; }
        public virtual short? Numeros { get; set; }
        public virtual short? Especiales { get; set; }
        public virtual short? Minusculas { get; set; }
        public virtual short? Mayusculas { get; set; }
        public virtual short? Tiemposesion { get; set; }
        public virtual bool Requiereotp { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
    }
}
