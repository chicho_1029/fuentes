using System;
using System.Text;
using System.Collections.Generic;
using Modelo.Util;

namespace Modelo.Entities {
    
    public class Coreusuariosesiontransaccion : IEntity {
        public virtual string Cusuario { get; set; }
        public virtual string Token { get; set; }
        public virtual string Operaciontransaccion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coreusuariosesiontransaccion;
			if (t == null) return false;
			if (Cusuario == t.Cusuario
			 && Token == t.Token
			 && Operaciontransaccion == t.Operaciontransaccion)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Cusuario.GetHashCode();
			hash = (hash * 397) ^ Token.GetHashCode();
			hash = (hash * 397) ^ Operaciontransaccion.GetHashCode();

			return hash;
        }
        #endregion

        public virtual object Clone() {
            return this.MemberwiseClone();
        }

    }
}
