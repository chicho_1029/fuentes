using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coremenu {
        public Coremenu() { }
        public virtual int Cmenu { get; set; }
        public virtual short? Cmodulo { get; set; }
        public virtual int? Ctransaccion { get; set; }
        public virtual string Nombre { get; set; }
        public virtual bool Mostrar { get; set; }
        public virtual bool Activado { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
    }
}
