using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Corecatalogo {
        public Corecatalogo() { }
        public virtual int Ccatalogo { get; set; }
        public virtual short Cmodulo { get; set; }
        public virtual string Nombre { get; set; }
        public virtual bool Activado { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
    }
}
