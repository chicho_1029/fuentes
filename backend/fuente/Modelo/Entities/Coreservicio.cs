using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreservicio {
        public virtual short Cservicio { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Url { get; set; }
        public virtual string Metodo { get; set; }
        public virtual string Tipocontenido { get; set; }
        public virtual bool Credenciales { get; set; }
        public virtual bool Autorizacion { get; set; }
        public virtual bool Autenticacion { get; set; }
        public virtual string Autenticacionusuario { get; set; }
        public virtual string Autenticacionpassword { get; set; }
        public virtual bool Encriptar { get; set; }
        public virtual short Timeout { get; set; }
        public virtual bool Registralog { get; set; }
    }
}
