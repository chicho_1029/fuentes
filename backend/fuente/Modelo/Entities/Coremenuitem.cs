using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coremenuitem {
        public virtual int Cmenuitem { get; set; }
        public virtual int Cmenu { get; set; }
        public virtual short Crol { get; set; }
        public virtual int? Cmenupadre { get; set; }
        public virtual string Icono { get; set; }
        public virtual short? Orden { get; set; }
        public virtual bool Essubmenu { get; set; }
        public virtual bool Izquierda { get; set; }
        public virtual bool Sidebaractive { get; set; }
        public virtual bool Notificacion { get; set; }
        public virtual bool Cargarpantalla { get; set; }
        public virtual bool Activado { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coremenuitem;
			if (t == null) return false;
			if (Cmenuitem == t.Cmenuitem
			 && Cmenu == t.Cmenu
			 && Crol == t.Crol)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Cmenuitem.GetHashCode();
			hash = (hash * 397) ^ Cmenu.GetHashCode();
			hash = (hash * 397) ^ Crol.GetHashCode();

			return hash;
        }
        #endregion
    }
}
