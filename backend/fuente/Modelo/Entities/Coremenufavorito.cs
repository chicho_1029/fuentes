using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coremenufavorito {
        public virtual string Cusuario { get; set; }
        public virtual short Crol { get; set; }
        public virtual short Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Coremenufavorito;
			if (t == null) return false;
			if (Cusuario == t.Cusuario
			 && Crol == t.Crol
			 && Cmodulo == t.Cmodulo
			 && Ctransaccion == t.Ctransaccion)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Cusuario.GetHashCode();
			hash = (hash * 397) ^ Crol.GetHashCode();
			hash = (hash * 397) ^ Cmodulo.GetHashCode();
			hash = (hash * 397) ^ Ctransaccion.GetHashCode();

			return hash;
        }
        #endregion
    }
}
