using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Corecatalogodetalle {
        public virtual int Ccatalogo { get; set; }
        public virtual string Cdetalle { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Calterno1 { get; set; }
        public virtual string Calterno2 { get; set; }
        public virtual bool Activado { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as Corecatalogodetalle;
			if (t == null) return false;
			if (Ccatalogo == t.Ccatalogo
			 && Cdetalle == t.Cdetalle)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ Ccatalogo.GetHashCode();
			hash = (hash * 397) ^ Cdetalle.GetHashCode();

			return hash;
        }
        #endregion
    }
}
