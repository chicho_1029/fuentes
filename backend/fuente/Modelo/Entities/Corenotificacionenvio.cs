using System;
using System.Text;
using System.Collections.Generic;
using Modelo.Util;

namespace Modelo.Entities {

    public class Corenotificacionenvio : IEntity {
        public virtual long Cenvio { get; set; }
        public virtual short Cnotificacion { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual string Token { get; set; }
        public virtual DateTime Freal { get; set; }
        public virtual string Correo { get; set; }
        public virtual string Parametros { get; set; }
        public virtual bool Error { get; set; }
        public virtual string Respuesta { get; set; }

        public virtual object Clone() {
            return this.MemberwiseClone();
        }

    }
}
