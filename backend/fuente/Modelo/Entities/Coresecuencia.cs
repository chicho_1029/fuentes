using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coresecuencia {
        public virtual int Csecuencia { get; set; }
        public virtual string Nombre { get; set; }
        public virtual long Valorinicial { get; set; }
        public virtual long Valoractual { get; set; }
    }
}
