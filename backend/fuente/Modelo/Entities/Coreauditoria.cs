using System;
using System.Text;
using System.Collections.Generic;
using Modelo.Util;

namespace Modelo.Entities {

    public class Coreauditoria : IEntity {
        public virtual long Cauditoria { get; set; }
        public virtual short Cmodulo { get; set; }
        public virtual int Ctransaccion { get; set; }
        public virtual string Centidad { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual string Token { get; set; }
        public virtual int Fsistema { get; set; }
        public virtual DateTime Freal { get; set; }
        public virtual string Tipo { get; set; }
        public virtual string Roriginal { get; set; }
        public virtual string Rmodificado { get; set; }


        public virtual object Clone() {
            return this.MemberwiseClone();
        }
    }
}
