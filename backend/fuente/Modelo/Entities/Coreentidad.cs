using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Coreentidad {
        public Coreentidad() { }
        public virtual string Centidad { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Esquema { get; set; }
        public virtual bool Manejaauditoria { get; set; }
        public virtual bool Permitecatalogo { get; set; }
        public virtual bool Permiteconsulta { get; set; }
        public virtual bool Permiteseguridad { get; set; }
    }
}
