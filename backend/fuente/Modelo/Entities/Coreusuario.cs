using System;
using System.Text;
using System.Collections.Generic;
using Modelo.Util;

namespace Modelo.Entities {
    
    public class Coreusuario : IEntity {
        public Coreusuario() { }
        public virtual string Cusuario { get; set; }
        public virtual long Cente { get; set; }
        public virtual int? Estatuscusuariocatalogo { get; set; }
        public virtual string Estatuscusuariocdetalle { get; set; }
        public virtual string Ccanal { get; set; }
        public virtual int Cclave { get; set; }
        public virtual bool Cambioclave { get; set; }
        public virtual int? Numerointentos { get; set; }
        public virtual string Observacion { get; set; }
        public virtual string Cusuariocre { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        public virtual string Cusuariomod { get; set; }
        public virtual DateTime? Fmodificacion { get; set; }
        public virtual DateTime Fultimoingreso { get; set; }

        public virtual object Clone() {
            return this.MemberwiseClone();
        }
    }
}
