﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Entities {
    public class Coreusuarioclave {
        public virtual int Cclave { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual string Password { get; set; }
        public virtual bool? Temporal { get; set; }
        public virtual DateTime Fcreacion { get; set; }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
            if (obj == null) return false;
            var t = obj as Coreusuarioclave;
            if (t == null) return false;
            if (Cclave == t.Cclave
             && Cusuario == t.Cusuario)
                return true;

            return false;
        }
        public override int GetHashCode() {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ Cclave.GetHashCode();
            hash = (hash * 397) ^ Cusuario.GetHashCode();

            return hash;
        }
        #endregion
    }
}
