using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Corelog {
        public virtual long Clog { get; set; }
        public virtual string Ccanal { get; set; }
        public virtual string Cusuario { get; set; }
        public virtual string Csession { get; set; }
        public virtual short? Cmodulo { get; set; }
        public virtual int? Ctransaccion { get; set; }
        public virtual string Cclase { get; set; }
        public virtual string Token { get; set; }
        public virtual DateTime Freal { get; set; }
        public virtual string Recurso { get; set; }
        public virtual bool Error { get; set; }
        public virtual string Mensaje { get; set; }
        public virtual string Excepcion { get; set; }
    }
}
