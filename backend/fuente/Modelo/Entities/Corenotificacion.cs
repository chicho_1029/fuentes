using System;
using System.Text;
using System.Collections.Generic;


namespace Modelo.Entities {
    
    public class Corenotificacion {
        public Corenotificacion() { }
        public virtual short Cnotificacion { get; set; }
        public virtual short Ccorreo { get; set; }
        public virtual string Asunto { get; set; }
        public virtual string Texto { get; set; }
        public virtual bool Registraenvio { get; set; }
    }
}
