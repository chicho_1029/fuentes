﻿using Newtonsoft.Json;
using NHibernate.Transform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Modelo.Util {

    [Serializable]
    public class ResultTransformer : IResultTransformer {

        public IList TransformList(IList collection) {
            return collection;
        }

        public object TransformTuple(object[] tuple, string[] aliases) {
            var result = new Dictionary<string, object>();
            for (int i = 0; i < aliases.Length; i++) {
                var a = tuple[i];
                if (a is null || a is string || a is decimal || a is int || a is long || a is bool || a is DateTime) {
                    result.Add(aliases[i], tuple[i]);
                } else {
                    var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(JsonConvert.SerializeObject(tuple[i]));
                    result = result.Concat(dictionary).ToLookup(x => x.Key, x => x.Value).ToDictionary(x => x.Key, g => g.First());
                }
            }
            return result;
        }

    }

}
