﻿namespace Modelo.Util {

    /// <summary>
    /// Metodo que permite clonar.
    /// </summary>
    public interface IEntity {
        object Clone();
    }

}
