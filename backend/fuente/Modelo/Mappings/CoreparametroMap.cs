using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreparametroMap : ClassMap<Coreparametro> {
        
        public CoreparametroMap() {
			Table("CoreParametro");
			LazyLoad();
			Id(x => x.Cparametro).GeneratedBy.Assigned().Column("cparametro");
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Valor).Column("valor");
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
