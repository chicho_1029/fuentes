using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreusuariorolMap : ClassMap<Coreusuariorol> {
        
        public CoreusuariorolMap() {
			Table("CoreUsuarioRol");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Crol, "crol")
			             .KeyProperty(x => x.Cusuario, "cusuario");
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
        }
    }
}
