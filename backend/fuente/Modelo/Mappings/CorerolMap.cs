using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorerolMap : ClassMap<Corerol> {
        
        public CorerolMap() {
			Table("CoreRol");
			LazyLoad();
			Id(x => x.Crol).GeneratedBy.Assigned().Column("crol");
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
			Map(x => x.Cmodulo).Column("cmodulo").Not.Nullable();
        }
    }
}
