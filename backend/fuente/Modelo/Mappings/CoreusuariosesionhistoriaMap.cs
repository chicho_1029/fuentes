using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreusuariosesionhistoriaMap : ClassMap<Coreusuariosesionhistoria> {
        
        public CoreusuariosesionhistoriaMap() {
			Table("CoreUsuarioSesionHistoria");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cusuario, "cusuario")
			             .KeyProperty(x => x.Token, "token")
			             .KeyProperty(x => x.Ingreso, "ingreso");
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Terminal).Column("terminal");
			Map(x => x.Terminallocal).Column("terminallocal");
			Map(x => x.Navegador).Column("navegador");
			Map(x => x.Cmodulologin).Column("cmodulologin");
			Map(x => x.Finicio).Column("finicio");
			Map(x => x.Fsalida).Column("fsalida");
			Map(x => x.Radicacion).Column("radicacion");
        }
    }
}
