using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoremensajeMap : ClassMap<Coremensaje> {
        
        public CoremensajeMap() {
			Table("CoreMensaje");
			LazyLoad();
			Id(x => x.Cmensaje).GeneratedBy.Assigned().Column("cmensaje");
			Map(x => x.Tipo).Column("tipo").Not.Nullable();
			Map(x => x.Mensaje).Column("mensaje");
        }
    }
}
