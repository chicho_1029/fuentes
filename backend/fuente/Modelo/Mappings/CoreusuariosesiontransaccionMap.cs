using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreusuariosesiontransaccionMap : ClassMap<Coreusuariosesiontransaccion> {
        
        public CoreusuariosesiontransaccionMap() {
			Table("CoreUsuarioSesionTransaccion");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cusuario, "cusuario")
			             .KeyProperty(x => x.Token, "token")
			             .KeyProperty(x => x.Operaciontransaccion, "operaciontransaccion");
        }
    }
}
