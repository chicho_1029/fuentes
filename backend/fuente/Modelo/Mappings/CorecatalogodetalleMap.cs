using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorecatalogodetalleMap : ClassMap<Corecatalogodetalle> {
        
        public CorecatalogodetalleMap() {
			Table("CoreCatalogoDetalle");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Ccatalogo, "ccatalogo")
			             .KeyProperty(x => x.Cdetalle, "cdetalle");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Calterno1).Column("calterno1");
			Map(x => x.Calterno2).Column("calterno2");
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
        }
    }
}
