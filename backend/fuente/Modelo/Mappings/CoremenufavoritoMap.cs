using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoremenufavoritoMap : ClassMap<Coremenufavorito> {
        
        public CoremenufavoritoMap() {
			Table("CoreMenuFavorito");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cusuario, "cusuario")
			             .KeyProperty(x => x.Crol, "crol")
			             .KeyProperty(x => x.Cmodulo, "cmodulo")
			             .KeyProperty(x => x.Ctransaccion, "ctransaccion");
        }
    }
}
