using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorecorreoMap : ClassMap<Corecorreo> {
        
        public CorecorreoMap() {
			Table("CoreCorreo");
			LazyLoad();
			Id(x => x.Ccorreo).GeneratedBy.Assigned().Column("ccorreo");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Direccion).Column("direccion").Not.Nullable();
			Map(x => x.Remitente).Column("remitente").Not.Nullable();
			Map(x => x.Password).Column("password").Not.Nullable();
			Map(x => x.Hostsmtp).Column("hostsmtp").Not.Nullable();
			Map(x => x.Puertosmtp).Column("puertosmtp").Not.Nullable();
			Map(x => x.Hostimap).Column("hostimap");
			Map(x => x.Puertoimap).Column("puertoimap");
			Map(x => x.Ssl).Column("ssl").Not.Nullable();
        }
    }
}
