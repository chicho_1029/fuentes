using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorecatalogoMap : ClassMap<Corecatalogo> {
        
        public CorecatalogoMap() {
			Table("CoreCatalogo");
			LazyLoad();
			Id(x => x.Ccatalogo).GeneratedBy.Identity().Column("ccatalogo");
			Map(x => x.Cmodulo).Column("cmodulo").Not.Nullable();
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
        }
    }
}
