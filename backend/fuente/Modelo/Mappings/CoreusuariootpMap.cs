using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreusuariootpMap : ClassMap<Coreusuariootp> {
        
        public CoreusuariootpMap() {
			Table("CoreUsuarioOtp");
			LazyLoad();
			Id(x => x.Cotp).GeneratedBy.Identity().Column("cotp");
			Map(x => x.Cusuario).Column("cusuario").Not.Nullable();
			Map(x => x.Cmodulo).Column("cmodulo").Not.Nullable();
			Map(x => x.Ctransaccion).Column("ctransaccion").Not.Nullable();
			Map(x => x.Codigootp).Column("codigootp").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Fcaducidad).Column("fcaducidad").Not.Nullable();
			Map(x => x.Validado).Column("validado").Not.Nullable();
			Map(x => x.Fvalidado).Column("fvalidado");
        }
    }
}
