using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreinstanciaMap : ClassMap<Coreinstancia> {
        
        public CoreinstanciaMap() {
			Table("CoreInstancia");
			LazyLoad();
			Id(x => x.Cinstancia).GeneratedBy.Assigned().Column("cinstancia");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Servidor).Column("servidor").Not.Nullable();
			Map(x => x.Base).Column("base").Not.Nullable();
			Map(x => x.Usuario).Column("usuario").Not.Nullable();
			Map(x => x.Password).Column("password").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
