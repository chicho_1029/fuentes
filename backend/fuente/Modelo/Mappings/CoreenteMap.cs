using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreenteMap : ClassMap<Coreente> {
        
        public CoreenteMap() {
			Table("CoreEnte");
			LazyLoad();
			Id(x => x.Cente).GeneratedBy.Identity().Column("cente");
			Map(x => x.Tipoente).Column("tipoente").Not.Nullable();
			Map(x => x.Tipoidentificacionccatalogo).Column("tipoidentificacionccatalogo").Not.Nullable();
			Map(x => x.Tipoidentificacioncdetalle).Column("tipoidentificacioncdetalle");
			Map(x => x.Identificacion).Column("identificacion").Not.Nullable();
			Map(x => x.Primernombre).Column("primernombre");
			Map(x => x.Segundonombre).Column("segundonombre");
			Map(x => x.Primerapellido).Column("primerapellido");
			Map(x => x.Segundoapellido).Column("segundoapellido");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Fnacimiento).Column("fnacimiento");
			Map(x => x.Celular).Column("celular");
			Map(x => x.Email).Column("email");
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
