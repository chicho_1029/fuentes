using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreentidadMap : ClassMap<Coreentidad> {
        
        public CoreentidadMap() {
			Table("CoreEntidad");
			LazyLoad();
			Id(x => x.Centidad).GeneratedBy.Assigned().Column("centidad");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Esquema).Column("esquema");
			Map(x => x.Manejaauditoria).Column("manejaauditoria").Not.Nullable();
			Map(x => x.Permitecatalogo).Column("permitecatalogo").Not.Nullable();
			Map(x => x.Permiteconsulta).Column("permiteconsulta").Not.Nullable();
			Map(x => x.Permiteseguridad).Column("permiteseguridad").Not.Nullable();
        }
    }
}
