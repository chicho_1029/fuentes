using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoremoduloMap : ClassMap<Coremodulo> {
        
        public CoremoduloMap() {
			Table("CoreModulo");
			LazyLoad();
			Id(x => x.Cmodulo).GeneratedBy.Assigned().Column("cmodulo");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cclase).Column("cclase");
			Map(x => x.Icono).Column("icono");
        }
    }
}
