using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorenotificacionenvioMap : ClassMap<Corenotificacionenvio> {
        
        public CorenotificacionenvioMap() {
			Table("CoreNotificacionEnvio");
			LazyLoad();
			Id(x => x.Cenvio).GeneratedBy.Identity().Column("cenvio");
			Map(x => x.Cnotificacion).Column("cnotificacion").Not.Nullable();
			Map(x => x.Cusuario).Column("cusuario");
			Map(x => x.Token).Column("token");
			Map(x => x.Freal).Column("freal").Not.Nullable();
			Map(x => x.Correo).Column("correo").Not.Nullable();
			Map(x => x.Parametros).Column("parametros");
			Map(x => x.Error).Column("error").Not.Nullable();
			Map(x => x.Respuesta).Column("respuesta");
        }
    }
}
