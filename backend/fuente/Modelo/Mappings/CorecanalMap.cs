using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorecanalMap : ClassMap<Corecanal> {
        
        public CorecanalMap() {
			Table("CoreCanal");
			LazyLoad();
			Id(x => x.Ccanal).GeneratedBy.Assigned().Column("ccanal");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Longitud).Column("longitud");
			Map(x => x.Diasvalidez).Column("diasvalidez");
			Map(x => x.Diasmensajeinvalidez).Column("diasmensajeinvalidez");
			Map(x => x.Intentos).Column("intentos");
			Map(x => x.Repeticiones).Column("repeticiones");
			Map(x => x.Numeros).Column("numeros");
			Map(x => x.Especiales).Column("especiales");
			Map(x => x.Minusculas).Column("minusculas");
			Map(x => x.Mayusculas).Column("mayusculas");
			Map(x => x.Tiemposesion).Column("tiemposesion");
			Map(x => x.Requiereotp).Column("requiereotp").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
