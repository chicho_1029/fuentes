using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorenotificacionMap : ClassMap<Corenotificacion> {
        
        public CorenotificacionMap() {
			Table("CoreNotificacion");
			LazyLoad();
			Id(x => x.Cnotificacion).GeneratedBy.Assigned().Column("cnotificacion");
			Map(x => x.Ccorreo).Column("ccorreo").Not.Nullable();
			Map(x => x.Asunto).Column("asunto").Not.Nullable();
			Map(x => x.Texto).Column("texto").Not.Nullable();
			Map(x => x.Registraenvio).Column("registraenvio").Not.Nullable();
        }
    }
}
