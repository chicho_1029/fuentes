using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreusuariosesionMap : ClassMap<Coreusuariosesion> {
        
        public CoreusuariosesionMap() {
			Table("CoreUsuarioSesion");
			LazyLoad();
			Id(x => x.Cusuario).GeneratedBy.Assigned().Column("cusuario");
			Map(x => x.Cmodulologin).Column("cmodulologin").Not.Nullable();
			Map(x => x.Token).Column("token").Not.Nullable();
			Map(x => x.Fsistema).Column("fsistema").Not.Nullable();
			Map(x => x.Terminal).Column("terminal");
			Map(x => x.Terminallocal).Column("terminallocal");
			Map(x => x.Navegador).Column("navegador");
			Map(x => x.Finicio).Column("finicio");
			Map(x => x.Tiemposesion).Column("tiemposesion");
			Map(x => x.Fultimaaccion).Column("fultimaaccion");
        }
    }
}
