using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreauditoriaMap : ClassMap<Coreauditoria> {
        
        public CoreauditoriaMap() {
			Table("CoreAuditoria");
			LazyLoad();
			Id(x => x.Cauditoria).GeneratedBy.Identity().Column("cauditoria");
			Map(x => x.Cmodulo).Column("cmodulo").Not.Nullable();
			Map(x => x.Ctransaccion).Column("ctransaccion").Not.Nullable();
			Map(x => x.Centidad).Column("centidad").Not.Nullable();
			Map(x => x.Cusuario).Column("cusuario").Not.Nullable();
			Map(x => x.Token).Column("token").Not.Nullable();
			Map(x => x.Fsistema).Column("fsistema").Not.Nullable();
			Map(x => x.Freal).Column("freal").Not.Nullable();
			Map(x => x.Tipo).Column("tipo").Not.Nullable();
			Map(x => x.Roriginal).Column("roriginal");
			Map(x => x.Rmodificado).Column("rmodificado");
        }
    }
}
