using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorefortiMap : ClassMap<Coreforti> {
        
        public CorefortiMap() {
			Table("CoreForti");
			LazyLoad();
			Id(x => x.Cusuarioforti).GeneratedBy.Assigned().Column("cusuarioforti");
			Map(x => x.Cmodulo).Column("cmodulo");
			Map(x => x.Ctransaccion).Column("ctransaccion");
			Map(x => x.Cusuario).Column("cusuario");
			Map(x => x.Token).Column("token");
			Map(x => x.Identificador).Column("identificador");
			Map(x => x.Identificacion).Column("identificacion");
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Celular).Column("celular");
			Map(x => x.Correo).Column("correo");
			Map(x => x.Fcaducidad).Column("fcaducidad");
        }
    }
}
