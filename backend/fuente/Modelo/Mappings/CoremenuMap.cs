using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoremenuMap : ClassMap<Coremenu> {
        
        public CoremenuMap() {
			Table("CoreMenu");
			LazyLoad();
			Id(x => x.Cmenu).GeneratedBy.Identity().Column("cmenu");
			Map(x => x.Cmodulo).Column("cmodulo");
			Map(x => x.Ctransaccion).Column("ctransaccion");
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Mostrar).Column("mostrar").Not.Nullable();
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
