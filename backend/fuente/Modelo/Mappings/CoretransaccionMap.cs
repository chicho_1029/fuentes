using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoretransaccionMap : ClassMap<Coretransaccion> {
        
        public CoretransaccionMap() {
			Table("CoreTransaccion");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cmodulo, "cmodulo")
			             .KeyProperty(x => x.Ctransaccion, "ctransaccion");
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Ruta).Column("ruta");
        }
    }
}
