using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoremenuitemMap : ClassMap<Coremenuitem> {
        
        public CoremenuitemMap() {
			Table("CoreMenuItem");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cmenuitem, "cmenuitem")
			             .KeyProperty(x => x.Cmenu, "cmenu")
			             .KeyProperty(x => x.Crol, "crol");
			Map(x => x.Cmenupadre).Column("cmenupadre");
			Map(x => x.Icono).Column("icono");
			Map(x => x.Orden).Column("orden");
			Map(x => x.Essubmenu).Column("essubmenu").Not.Nullable();
			Map(x => x.Izquierda).Column("izquierda").Not.Nullable();
			Map(x => x.Sidebaractive).Column("sidebaractive").Not.Nullable();
			Map(x => x.Notificacion).Column("notificacion").Not.Nullable();
			Map(x => x.Cargarpantalla).Column("cargarpantalla").Not.Nullable();
			Map(x => x.Activado).Column("activado").Not.Nullable();
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
