using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreperfilrolMap : ClassMap<Coreperfilrol> {
        
        public CoreperfilrolMap() {
			Table("CorePerfilRol");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Crol, "crol")
			             .KeyProperty(x => x.Perfil, "perfil");
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
        }
    }
}
