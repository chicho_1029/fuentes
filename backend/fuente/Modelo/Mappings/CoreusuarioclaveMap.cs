﻿using FluentNHibernate.Mapping;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Modelo.Mappings {
	public class CoreusuarioclaveMap : ClassMap<Coreusuarioclave> {

		public CoreusuarioclaveMap() {
			Table("CoreUsuarioClave");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cclave, "cclave")
						 .KeyProperty(x => x.Cusuario, "cusuario");
			Map(x => x.Password).Column("password").Not.Nullable();
			Map(x => x.Temporal).Column("temporal");
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
		}
	}
}
