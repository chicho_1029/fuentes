using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreclasetransaccionMap : ClassMap<Coreclasetransaccion> {
        
        public CoreclasetransaccionMap() {
			Table("CoreClaseTransaccion");
			LazyLoad();
			CompositeId().KeyProperty(x => x.Cmodulo, "cmodulo")
			             .KeyProperty(x => x.Ctransaccion, "ctransaccion")
			             .KeyProperty(x => x.Cclase, "cclase")
			             .KeyProperty(x => x.Centidad, "centidad")
			             .KeyProperty(x => x.Secuencia, "secuencia");
			Map(x => x.Consulta).Column("consulta").Not.Nullable();
			Map(x => x.Permiso).Column("permiso");
			Map(x => x.Metodo).Column("metodo");
			Map(x => x.Parametro).Column("parametro").Not.Nullable();
			Map(x => x.Activado).Column("activado").Not.Nullable();
        }
    }
}
