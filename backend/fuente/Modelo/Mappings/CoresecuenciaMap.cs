using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoresecuenciaMap : ClassMap<Coresecuencia> {
        
        public CoresecuenciaMap() {
			Table("CoreSecuencia");
			LazyLoad();
			Id(x => x.Csecuencia).GeneratedBy.Identity().Column("csecuencia");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Valorinicial).Column("valorinicial").Not.Nullable();
			Map(x => x.Valoractual).Column("valoractual").Not.Nullable();
        }
    }
}
