using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreclaseMap : ClassMap<Coreclase> {
        
        public CoreclaseMap() {
			Table("CoreClase");
			LazyLoad();
			Id(x => x.Cclase).GeneratedBy.Assigned().Column("cclase");
			Map(x => x.Tipo).Column("tipo").Not.Nullable();
			Map(x => x.Nombre).Column("nombre");
			Map(x => x.Clasenegocio).Column("clasenegocio").Not.Nullable();
			Map(x => x.Activado).Column("activado").Not.Nullable();
        }
    }
}
