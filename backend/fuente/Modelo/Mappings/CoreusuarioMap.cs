using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreusuarioMap : ClassMap<Coreusuario> {
        
        public CoreusuarioMap() {
			Table("CoreUsuario");
			LazyLoad();
			Id(x => x.Cusuario).GeneratedBy.Assigned().Column("cusuario");
			Map(x => x.Cente).Column("cente").Not.Nullable();
			Map(x => x.Estatuscusuariocatalogo).Column("estatuscusuariocatalogo");
			Map(x => x.Estatuscusuariocdetalle).Column("estatuscusuariocdetalle");
			Map(x => x.Ccanal).Column("ccanal").Not.Nullable();
			Map(x => x.Cclave).Column("cclave").Not.Nullable();
			Map(x => x.Cambioclave).Column("cambioclave").Not.Nullable();
			Map(x => x.Numerointentos).Column("numerointentos");
			Map(x => x.Observacion).Column("observacion");
			Map(x => x.Cusuariocre).Column("cusuariocre").Not.Nullable();
			Map(x => x.Fcreacion).Column("fcreacion").Not.Nullable();
			Map(x => x.Cusuariomod).Column("cusuariomod");
			Map(x => x.Fmodificacion).Column("fmodificacion");
			Map(x => x.Fultimoingreso).Column("fultimoingreso").Not.Nullable();
        }
    }
}
