using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CorelogMap : ClassMap<Corelog> {
        
        public CorelogMap() {
			Table("CoreLog");
			LazyLoad();
			Id(x => x.Clog).GeneratedBy.Identity().Column("clog");
			Map(x => x.Ccanal).Column("ccanal").Not.Nullable();
			Map(x => x.Cusuario).Column("cusuario").Not.Nullable();
			Map(x => x.Csession).Column("csession").Not.Nullable();
			Map(x => x.Cmodulo).Column("cmodulo");
			Map(x => x.Ctransaccion).Column("ctransaccion");
			Map(x => x.Cclase).Column("cclase");
			Map(x => x.Token).Column("token").Not.Nullable();
			Map(x => x.Freal).Column("freal").Not.Nullable();
			Map(x => x.Recurso).Column("recurso");
			Map(x => x.Error).Column("error").Not.Nullable();
			Map(x => x.Mensaje).Column("mensaje");
			Map(x => x.Excepcion).Column("excepcion");
        }
    }
}
