using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using Modelo.Entities; 

namespace Modelo.Mappings {
    
    
    public class CoreservicioMap : ClassMap<Coreservicio> {
        
        public CoreservicioMap() {
			Table("CoreServicio");
			LazyLoad();
			Id(x => x.Cservicio).GeneratedBy.Assigned().Column("cservicio");
			Map(x => x.Nombre).Column("nombre").Not.Nullable();
			Map(x => x.Url).Column("url").Not.Nullable();
			Map(x => x.Metodo).Column("metodo").Not.Nullable();
			Map(x => x.Tipocontenido).Column("tipocontenido").Not.Nullable();
			Map(x => x.Credenciales).Column("credenciales").Not.Nullable();
			Map(x => x.Autorizacion).Column("autorizacion").Not.Nullable();
			Map(x => x.Autenticacion).Column("autenticacion").Not.Nullable();
			Map(x => x.Autenticacionusuario).Column("autenticacionusuario");
			Map(x => x.Autenticacionpassword).Column("autenticacionpassword");
			Map(x => x.Encriptar).Column("encriptar").Not.Nullable();
			Map(x => x.Timeout).Column("timeout").Not.Nullable();
			Map(x => x.Registralog).Column("registralog").Not.Nullable();
        }
    }
}
