﻿using Core.Executor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using Util.Configuracion;
using Util.Request;
using WebAPI.Authentication;

namespace WebAPI.Controllers {

    [Route("[controller]")]
    [ApiController]
    public class ApiController : ControllerBase {

        private readonly ILogger log;
        private readonly AppSettings configuration;

        public ApiController(IOptions<AppSettings> settings, ILogger<ApiController> logger) {
            SettingsManager.SetConfiguration(settings.Value, logger);
            log = logger;
            configuration = settings.Value;
        }

        #region CORE

        [Route("Mantener"), BasicAuthorization]
        [HttpPost]
        public IActionResult Mantener([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                return Ok(CoreExecutor.EjecutarMantener(request, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("MantenerHub")]
        [HttpPost]
        public IActionResult MantenerHub([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                return Ok(HubExecutor.EjecutarMantener(dataRequest, ApiHelper.GetIp(HttpContext, Request)));
            } catch {
                return Ok();
            }
        }

        [Route("Consultar"), BasicAuthorization]
        [HttpPost]
        public IActionResult Consultar([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                return Ok(CoreExecutor.EjecutarQuery(request, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("Menu"), BasicAuthorization]
        [HttpPost]
        public IActionResult Menu([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                return Ok(CoreExecutor.EjecutarMenu(request, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("Login"), BasicAuthorization]
        [HttpPost]
        public IActionResult Login([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                return Ok(LoginExecutor.EjecutarLogin(request, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("Logout"), BasicAuthorization]
        [HttpPost]
        public IActionResult Logout([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                return Ok(LoginExecutor.EjecutarLogout(request, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        #endregion

        #region PUBLIC

        [Route("MantenerPublic"), BasicAuthorization]
        [HttpPost]
        public IActionResult MantenerPublic([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                return Ok(PublicExecutor.EjecutarMantenerEncryp(dataRequest, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("ConsultarPublic"), BasicAuthorization]
        [HttpPost]
        public IActionResult ConsultarPublic([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                return Ok(PublicExecutor.EjecutarQueryEncryp(dataRequest, ApiHelper.GetIp(HttpContext, Request), ApiHelper.GetAutenticate(Request)));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("FacebookInternal")]
        [HttpPost]
        public IActionResult FacebookWebhook([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                //ModelFacebook modelFace = new ModelFacebook();
                //modelFace = ConvertObject.Deserialize<ModelFacebook>(request);

                //if (modelFace.verify_token.Equals(configuration.Public.Facebook.ApiToken)) {
                //    return Ok(modelFace.challenge);
                //} else {
                    return Ok(string.Empty);
                //}
            } catch (Exception ex) {
                return Ok(ex.Message);
            }
        }

        [Route("FacebookCheck")]
        [HttpPost]
        public IActionResult RecibeMensajesFacebook([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);

                // Instancia request
                RequestBase requestBase = new RequestBase();
                requestBase.Cclase = "SCMESSENGERFACEBOOK";
                requestBase.Token = configuration.Public.Facebook.ApiToken;
                requestBase.SetCanal(configuration.Public.Canal);
                requestBase.SetUsuario(configuration.Public.Facebook.Usuario);
                requestBase.AddDatos("datosFacebook", request);

                string dataRequest = ConvertObject.Serialize(requestBase);

                return Ok(PublicExecutor.EjecutarMantenerCanales(dataRequest, ApiHelper.GetIp(HttpContext, Request), configuration, log));
            } catch {
                return BadRequest(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("InstagramInternal")]
        [HttpPost]
        public IActionResult InstagramWebhook([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);
                //ModelInstagram modelInstagaram = new ModelInstagram();
                //modelInstagaram = ConvertObject.Deserialize<ModelInstagram>(request);

                //if (modelInstagaram.verify_token.Equals(configuration.Public.Instagram.ApiToken)) {
                //    return Ok(modelInstagaram.challenge);
                //} else {
                    return Ok(string.Empty);
                //}
            } catch (Exception ex) {
                return Ok(ex.Message);
            }
        }

        [Route("InstagramCheck")]
        [HttpPost]
        public IActionResult RecibeComentariosInstagram([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);

                // Instancia request
                RequestBase requestBase = new RequestBase();
                requestBase.Cclase = "SCINSTAGRAM";
                requestBase.Token = configuration.Public.Instagram.ApiToken;
                requestBase.SetCanal(configuration.Public.Canal);
                requestBase.SetUsuario(configuration.Public.Instagram.Usuario);
                requestBase.AddDatos("datosInstagram", request);

                string dataRequest = ConvertObject.Serialize(requestBase);
                return Ok(PublicExecutor.EjecutarMantenerCanales(dataRequest, ApiHelper.GetIp(HttpContext, Request), configuration, log));
            } catch {
                return BadRequest(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("WhatsappCheck")]
        [HttpPost]
        public IActionResult RecibeMensajesWhatsapp([FromBody] string data) {
            try {
                string request = RequestHelper.GetRequest(data);

                // Instancia request
                RequestBase requestBase = new RequestBase();
                requestBase.Cclase = "SCRECIBIRMENSAJESWHATSAPP";
                requestBase.Token = configuration.Public.Whatsapp.KeyApi.ToUpper();
                requestBase.SetCanal(configuration.Public.Canal);
                requestBase.SetUsuario(configuration.Public.Whatsapp.Usuario);
                requestBase.AddDatos("datosWhatsapp", request);

                string dataRequest = ConvertObject.Serialize(requestBase);
                string dataResponse = PublicExecutor.EjecutarMantenerCanales(dataRequest, ApiHelper.GetIp(HttpContext, Request), configuration, log);

                return Ok(dataResponse);
            } catch {
                return BadRequest(ResponseHelper.GetResponseError(Request));
            }
        }

        #endregion

    }

}
