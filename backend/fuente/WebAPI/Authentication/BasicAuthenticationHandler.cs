﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Util.Configuracion;

namespace WebAPI.Authentication {
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions> {
        private readonly AuthorizationBasic _authorizationbasic;
        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IOptions<AppSettings> settings
            )
    : base(options, logger, encoder, clock) {
            this._authorizationbasic = settings.Value.AuthorizationBasic;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync() {
            Response.Headers.Add("WWW-Authenticate", "Basic");

            if (!Request.Headers.ContainsKey("Authorization")) {
                return Task.FromResult(AuthenticateResult.Fail("Falta el encabezado de autorización."));
            }

            // Get authorization key
            var authorizationHeader = Request.Headers["Authorization"].ToString();
            var authHeaderRegex = new Regex(@"Basic (.*)");

            if (!authHeaderRegex.IsMatch(authorizationHeader)) {
                return Task.FromResult(AuthenticateResult.Fail("El código de autorización no está formateado correctamente."));
            }

            var authBase64 = Encoding.UTF8.GetString(Convert.FromBase64String(authHeaderRegex.Replace(authorizationHeader, "$1")));
            var authSplit = authBase64.Split(Convert.ToChar(":"), 2);
            var authUsername = authSplit[0];
            var authPassword = authSplit.Length > 1 ? authSplit[1] : throw new Exception("No se puede obtener la contraseña");

            if (authUsername != this._authorizationbasic.UsrWebAPI || authPassword != this._authorizationbasic.PwdWebAPI) {
                return Task.FromResult(AuthenticateResult.Fail("El nombre de usuario o la contraseña no son correctos."));
            }

            var authenticatedUser = new AuthenticatedUser("BasicAuthentication", true, "uwsavserviciocliente");
            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(authenticatedUser));

            return Task.FromResult(AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, Scheme.Name)));
        }
    }

    public class AuthenticatedUser : IIdentity {
        public AuthenticatedUser(string authenticationType, bool isAuthenticated, string name) {
            AuthenticationType = authenticationType;
            IsAuthenticated = isAuthenticated;
            Name = name;
        }

        public string AuthenticationType { get; }

        public bool IsAuthenticated { get; }

        public string Name { get; }
    }
}
