﻿using Microsoft.AspNetCore.Authorization;
namespace WebAPI.Authentication {
    public class BasicAuthorizationAttribute : AuthorizeAttribute {
        public BasicAuthorizationAttribute() {
            Policy = "BasicAuthentication";
        }
    }
}