﻿using Dal.Core;
using Modelo.Dto.General;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using Util.Componentes;
using Util.Excepcion;
using Util.Request;

namespace Business.Seguridad.Menu {

    /// <summary>
    /// Clase que procesa menu.
    /// </summary>
    public class ProcesarMenu {

        private static readonly string command = "(event) => { this.itemClick(event) }";
        private static List<short> lroles;
        public Response EjecutarMenu(RequestBase request) {
            lroles = CoreRolDal.GetRolesLogeado(request.Cmodulologin, request.Cusuario, request.Token);
            GenerarSidebar(request);
            ProcesarInicio(request);

            return request.Response;
        }

        private static void GenerarSidebar(RequestBase request) {
            List<short> lroles = CoreRolDal.GetRolesLogeado(request.Cmodulologin, request.Cusuario, request.Token);
            IList<SidebarDto> Lsidebar = CoreMenuDal.GetSidebar(lroles);
            foreach (SidebarDto sidebar in Lsidebar.Where(x => x.Essubmenu == true)) {
                sidebar.lmenu = GenerarMenu(request, sidebar.Cmenu);
            }
            request.Response.Add("Sidebar", Lsidebar);
        }

        private static IList<MenuDto> GenerarMenu(RequestBase request, int cmenusidebar) {
            int i = -1, j;
            MenuDto menu = new MenuDto("Menú principal", "fa fa-home", "/0-0", command, true);

            IList<CoremenuDto> lMenuPadres = CoreMenuDal.GetMenuPadreSidebar(lroles, cmenusidebar);
            foreach (CoremenuDto item1 in lMenuPadres.Distinct()) {
                string target = CompletarTarget(item1.Cmodulo, item1.Ctransaccion);
                menu.Items.Add(new MenuDto(item1.Nombre, item1.Icono, target, string.IsNullOrEmpty(target) ? target : command, false));
                i++; j = -1;

                IList<CoremenuDto> lMenuSegundoNivel = CoreMenuDal.GetMenuPorRolPadre(item1.Cmenu);
                if (lMenuSegundoNivel.Count == 0) {
                    continue;
                }
                foreach (CoremenuDto item2 in lMenuSegundoNivel.Distinct()) {
                    target = CompletarTarget(item2.Cmodulo, item2.Ctransaccion);
                    menu.Items[i].Items.Add(new MenuDto(item2.Nombre, string.Empty, target, string.IsNullOrEmpty(target) ? target : command, false));
                    j++;

                    IList<CoremenuDto> lMenuTercernivel = CoreMenuDal.GetMenuPorRolPadre(item2.Cmenu);
                    if (lMenuTercernivel.Count == 0) {
                        continue;
                    }
                    foreach (CoremenuDto item3 in lMenuTercernivel.Distinct()) {
                        target = CompletarTarget(item3.Cmodulo, item3.Ctransaccion);
                        menu.Items[i].Items[j].Items.Add(new MenuDto(item3.Nombre, string.Empty, target, string.IsNullOrEmpty(target) ? target : command, false));
                    }
                }
            }

            MenuDto favoritos = GenerarFavoritos(request.Cusuario);

            // Completar menú en response
            IList<MenuDto> lmenu = new List<MenuDto> { menu, favoritos };
            return lmenu;
            //request.Response.Add("Menu", lmenu);
        }

        private static string CompletarTarget(short? cmodulo, int? ctransaccion) {
            if (ctransaccion == null || cmodulo == null) {
                return "/0-0";
            }
            return "/" + cmodulo + "-" + ctransaccion;
        }

        private static MenuDto GenerarFavoritos(string cusuario) {
            MenuDto favoritos = new MenuDto("Favoritos", "fa fa-star", "/0-0", command, false);

            //IList<Coremenufavorito> lfavoritos = CoreMenuFavoritoDal.GetUsuarioRolFavorito(cusuario, crol);
            //if (lfavoritos.Count > 0) {
            //    IList<Coremenu> lmenu = CoreMenuDal.GetMenuActivoPorRol(crol);

            //    foreach (Coremenufavorito item in lfavoritos) {
            //        Coremenu itemmenu = lmenu.FirstOrDefault(x => x.Cmodulo == item.Cmodulo && x.Ctransaccion == item.Ctransaccion);
            //        string target = CompletarTarget(itemmenu.Cmodulo, itemmenu.Ctransaccion);
            //        favoritos.Items.Add(new MenuDto(itemmenu.Nombre, string.Empty, target, string.IsNullOrEmpty(target) ? target : command, true));
            //    }
            //}

            return favoritos;
        }

        private static void ProcesarInicio(RequestBase request) {
            Coremodulo modulo = CoreModuloDal.GetModulo(request.Cmodulologin);

            if (!string.IsNullOrEmpty(modulo.Cclase)) {
                Coreclase clase = CoreClaseDal.GetClase(modulo.Cclase);
                if (clase == null) {
                    throw new CoreExcepcion("1031", $"CLASE ROL INCORRECTA: {modulo.Cclase}");
                }

                if (clase.Activado) {
                    string cnegocio = clase.Clasenegocio;
                    string assembly = cnegocio.Substring(0, cnegocio.IndexOf("."));
                    ObjectHandle handle = Activator.CreateInstance(assembly, cnegocio);
                    ClaseBusinessQuery c = (ClaseBusinessQuery)handle.Unwrap();

                    RequestQuery rq = ConvertObject.SerializeDeserialize<RequestQuery>(request);
                    request.Response = c.Ejecutar(rq);
                }
            }
        }

    }

}
