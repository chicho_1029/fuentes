﻿using Dal.Core;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using Util.Componentes;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Business.Seguridad.Menu {

    /// <summary>
    /// Metodo que se encarga de realizar el mantenimiento de las opciones de Menu.
    /// </summary>
    public class MantenimientoMenu : ClaseBusinessMantener {

        public override Response Ejecutar(RequestMantener rq) {

            Coremenu menuMantener;

            switch (rq.Mantener.Operacion) {
                case EnumOperacion.INSERT:
                    menuMantener = ConvertObject.Deserialize<Coremenu>(Convert.ToString(rq.Mantener.Registro));
                    //menuMantener.Cmenu = CoreMenuDal.GetMenuMaximoPorRol(menuMantener.Crol) + 1;

                    CoreMenuDal.CrearMenu(menuMantener);
                    break;
                case EnumOperacion.UPDATE:
                    menuMantener = ConvertObject.Deserialize<Coremenu>(Convert.ToString(rq.Mantener.Registro));

                    Coremenu menu = new Coremenu();
                        //CoreMenuDal.GetMenu(menuMantener.Crol, menuMantener.Cmenu);
                    if (menu == null) {
                        //throw new CoreExcepcion("1031", $"REGISTRO NO EXISTE: {menuMantener.Crol} | {menuMantener.Cmenu}");
                    }

                    menu.Cmodulo = menuMantener.Cmodulo;
                    menu.Ctransaccion = menuMantener.Ctransaccion;
                    menu.Nombre = menuMantener.Nombre;
                    //menu.Icono = menuMantener.Icono;
                    //menu.Orden = menuMantener.Orden;
                    menu.Mostrar = menuMantener.Mostrar;
                    menu.Activado = menuMantener.Activado;

                    CoreMenuDal.ActualizarMenu(menu);
                    break;
                case EnumOperacion.DELETE:
                    IList<Coremenu> lmenu = ConvertObject.SerializeDeserialize<List<Coremenu>>(rq.GetDatosObject("lregistroseliminar"));
                    foreach (Coremenu obj in lmenu) {
                        CoreMenuDal.EliminarMenu(obj);
                    }
                    break;
            }

            return rq.Response;
        }

    }

}
