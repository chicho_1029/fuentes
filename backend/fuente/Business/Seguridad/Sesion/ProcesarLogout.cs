﻿using Dal.Core;
using Modelo.Entities;
using Util.Request;

namespace Business.Seguridad.Sesion {

    /// <summary>
    /// Clase que procesa logout de la plataforma.
    /// </summary>
    public class ProcesarLogout {

        public Response Ejecutar(RequestLogin request) {
            Response resp = new Response();

            Coreusuariosesion sesion = CoreUsuarioSesionDal.GetSesionPorUsuarioToken(request.Cusuario, request.Token);
            if (sesion != null) {
                CoreUsuarioSesionHistoriaDal.ActualizarSesionHistoriaSalida(sesion);
                CoreUsuarioSesionDal.EliminarSesion(sesion);
            }

            return resp;
        }
    }
}
