﻿using AlianzaCoreMensajes;
using Business.Util.Alianza;
using Dal.Base;
using Dal.Core;
using Modelo.Entities;
using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Util.Excepcion;
using Util.General;
using Util.Request;
using Util.Seguridad;

namespace Business.Seguridad.Sesion {

    /// <summary>
    /// Clase que procesa login de la plataforma.
    /// </summary>
    public class ProcesarLogin {
        bool? diasvalidez = false;
        bool? mensajevalidez = false;
        int diasmensaje;
        public Response Ejecutar(RequestLogin request) {
            Response response = new Response();

            ValidarPoliticaCanal(request.Ccanal);

            Coreusuario usuario = CoreUsuarioDal.GetUsuario(request.Cusuario, request.Ccanal);
            if (usuario == null) throw new CoreExcepcion("1001", "USUARIO NO EXISTE");
            Coreusuarioclave usuarioclave = CoreUsuarioClaveDal.GetClave(request.Cusuario);
            if (usuarioclave == null) throw new CoreExcepcion("1001", "CLAVE NO EXISTE");

            request.AddDatos("Cambioclave", usuario.Cambioclave);
            request.AddDatos("Cente", usuario.Cente);

            // Login
            Corecanal canal = CoreCanalDal.GetCanal(request.Ccanal);
            request.AddDatos("timeoutminutos", canal.Tiemposesion);

            // Dias de validez clave
            DiasValidez(request, canal, usuario, usuarioclave);

            Corecatalogodetalle catalogo = CoreCatalogoDetalleDal.GetDetalle((int)usuario.Estatuscusuariocatalogo, usuario.Estatuscusuariocdetalle);
            if (usuario.Estatuscusuariocdetalle.Equals("INA")) {
                throw new CoreExcepcion("1027", "El usuario {0} se encuentra en estatus {1}", usuario.Cusuario, catalogo.Nombre);
            }
            if (usuario.Estatuscusuariocdetalle.Equals("BLO")) {
                throw new CoreExcepcion("1027", "El usuario {0} se encuentra en estatus {1}", usuario.Cusuario, catalogo.Nombre);
            }
            if ((usuario.Numerointentos == null ? 1 : usuario.Numerointentos) >= canal.Intentos) {
                usuario.Estatuscusuariocdetalle = "INA";
                ThreadUpdate.Update(usuario);
                throw new CoreExcepcion("1001", "El usuario {0} se encuentra en estatus {1}", usuario.Cusuario, "INACTIVO");
            }

            if (!usuarioclave.Password.Equals(Password.Encriptar(request.Password.ToString()))) {
                usuario.Numerointentos = usuario.Numerointentos == null ? 1 : usuario.Numerointentos + 1;
                ThreadUpdate.Update(usuario);
                int intentos = (int)usuario.Numerointentos;
                if (usuario.Estatuscusuariocdetalle.CompareTo("ACT") == 0 && intentos.CompareTo(canal.Intentos - 1) == 0) {
                    throw new CoreExcepcion("1001", "CLAVE INCORRECTA");
                }
                throw new CoreExcepcion("1001", "CLAVE INCORRECTA");
            }

            usuario.Numerointentos = 0;
            usuario.Estatuscusuariocdetalle = "ACT";


            // Ente
            Coreente ente = CoreEnteDal.GetEnte(usuario.Cente);

            RadicacionLogin radicacionLogin = CompletaRadicacionLogin(request, usuario, ente);

            ProcesarSesion(request, radicacionLogin);

            response.Add("radicacion", radicacionLogin);
            return response;
        }

        private RadicacionLogin CompletaRadicacionLogin(RequestLogin request, Coreusuario usuario, Coreente ente) {
            //DateTime fultimoingreso = CoreUsuarioSesionHistoriaDal.GetUltimoIngreso(request.Cusuario, request.Token);
            IList<Corerol> roles = CoreRolDal.GetRoles(request.Cusuario);
            bool existecmodulo;

            RadicacionLogin radicacionLogin = ConvertObject.SerializeDeserialize<RadicacionLogin>(request);
            radicacionLogin.Cusuario = request.Cusuario;
            radicacionLogin.Token = request.Token;
            foreach (Corerol rol in roles.OrderByDescending(x => x.Cmodulo)) {
                existecmodulo = false;
                RadicacionModulo radicacionModulo = new RadicacionModulo();
                RadicacionRol radicacionRol = new RadicacionRol();
                radicacionRol.Crol = rol.Crol;
                radicacionRol.Nombre = rol.Nombre;
                radicacionModulo.lroles.Add(radicacionRol);

                foreach (RadicacionModulo rModulo in radicacionLogin.Lmodulos) {
                    if (rModulo.Cmodulologin == rol.Cmodulo) {
                        existecmodulo = true;
                        rModulo.lroles.Add(radicacionRol);
                        continue;
                    }
                }

                Coremodulo modulo = CoreModuloDal.GetModulo(rol.Cmodulo);
                radicacionModulo.Cmodulologin = rol.Cmodulo;
                radicacionModulo.Icono = modulo.Icono;
                radicacionModulo.Nombre = modulo.Nombre;
                if (!existecmodulo && modulo.Activado) radicacionLogin.Lmodulos.Add(radicacionModulo);
            }

            if (radicacionLogin.Lmodulos.Count == 0) throw new CoreExcepcion("1005", $"NO SE PUEDE CRGAR ROLES: {request.Cusuario}");
            return radicacionLogin;
        }


        private RadicacionTemporal CompletaRadicacionLoginTemporal(RequestLogin request, Coreusuario usuario) {
            RadicacionTemporal radicacion = new RadicacionTemporal();
            radicacion.Token = request.Token;
            return radicacion;
        }

        private void ProcesarSesion(RequestLogin request, RadicacionLogin radicacionLogin) {
            CoreUsuarioSesionHistoriaDal.CrearSesionHistoriaIngreso(request, radicacionLogin.Lmodulos);

            Coreusuariosesion session = CoreUsuarioSesionDal.GetSesionPorUsuario(request.Cusuario);
            if (session == null) {
                return;
            }

            CoreUsuarioSesionDal.EliminarSesion(session);
        }

        private void ValidarPoliticaCanal(string ccanal) {
            Corecanal canal = CoreCanalDal.GetCanal(ccanal);

            if (canal == null) {
                throw new CoreExcepcion("1022", "CANAL NO DISPONIBLE");
            }
            if (Convert.ToInt32(canal.Tiemposesion) == 0) {
                throw new CoreExcepcion("1013", "TIEMPO DE SESIÓN NO DEFINIDO EN LA POLÍTICA DE SEGURIDAD");
            }
            if (Convert.ToBoolean(canal.Requiereotp)) {
                throw new CoreExcepcion("1023", "CANAL NO DISPONIBLE PARA OTP CAMBIE LA POLÍTICA DE SEGURIDAD");
            }
        }

        private void DiasValidez(RequestLogin request, Corecanal canal, Coreusuario usuario, Coreusuarioclave usuarioclave) {
            diasvalidez = false;
            mensajevalidez = false;
            diasmensaje = 0;

            if (canal.Diasvalidez != null) {
                if (usuarioclave != null) {
                    DateTime item = usuarioclave.Fcreacion;
                    int fcambio = Fecha.GetFechaEntero(item);
                    int factual = Fecha.GetFechaActualEntero();
                    short dias = Fecha.Resta365(factual, fcambio);
                    short restadias = Convert.ToInt16(canal.Diasvalidez - dias);
                    if (dias >= canal.Diasvalidez) {
                        usuario.Cambioclave = true;
                        diasvalidez = usuario.Cambioclave;
                        request.Datos["Cambioclave"] = usuario.Cambioclave;
                    }

                    if (canal.Diasmensajeinvalidez >= restadias) {
                        mensajevalidez = true;
                        diasmensaje = restadias;
                    }

                    ThreadUpdate.Update(usuario);
                }
            }
        }
    }
}
