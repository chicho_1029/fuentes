﻿using AlianzaCoreMensajes;
using System;
using Util.General;
using Util.Request;

namespace Business.Util.Alianza {

    /// <summary>
    /// Clase helper de mensajes de entrada para llamadas a servicios core
    /// </summary>
    internal static class Entrada {

        public static MELogin LoginAlianzaValle(RequestLogin request) {
            MELogin login = new MELogin {
                Login = request.Cusuario,
                Password = request.Password,
                Canal = request.Ccanal
            };
            return login;
        }

        public static MEIdentificacion Identificacion(string canal, string identificacion, string usuario) {
            MEIdentificacion me = new MEIdentificacion();
            me.Canal = canal;
            me.Identificacion = identificacion;
            me.Usuario = usuario;
            return me;
        }

        public static MECuenta Cuenta(RequestBase request, string ccuenta) {
            MECuenta me = new MECuenta {
                Canal = request.Ccanal,
                NumeroCuenta = ccuenta,
                Usuario = request.Cusuario
            };
            return me;
        }

        public static MEConsultaMovimientos Movimientos(string ccuenta, DateTime finicio, DateTime ffin) {
            MEConsultaMovimientos me = new MEConsultaMovimientos {
                NumeroCuenta = ccuenta,
                NumeroRegistros = 10,
                FechaDesde = Fecha.GetFechaCadena(finicio),
                FechaHasta = Fecha.GetFechaCadena(ffin)
            };
            return me;
        }

        public static MEOperacion TablaAmortizacion(RequestBase request, string operacion) {
            MEOperacion me = new MEOperacion {
                Canal = request.Ccanal,
                NumeroOperacion = operacion,
                Usuario = request.Cusuario
            };
            return me;
        }

        public static MESmsParametro Sms(string celular, int ccliente, int producto, string nemonico, string inicio, string intermedio, string intermedio1, string final) {
            MESmsParametro me = new MESmsParametro {
                Celular = celular,
                IdCliente = ccliente,
                IdProducto = producto,
                Producto = nemonico,
                MensajeInicio = inicio,
                MensajeIntermedio = intermedio,
                MensajeIntermedio1 = intermedio1,
                MensajeFinal = final
            };
            return me;
        }
    }

}
