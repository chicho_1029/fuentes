﻿using AlianzaCoreHelper;
using AlianzaCoreMensajes;
using Business.Util.General;
using Dal.Base;
using Microsoft.Extensions.Logging;
using System;
using System.Reflection;
using Util.Configuracion;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Business.Util.Alianza {

    /// <summary>
    /// Clase helper de mensajes de salida para llamadas a servicios core
    /// </summary>
    internal static class Salida {

        private static readonly ILogger log = SettingsManager.GetLogger();
        private static readonly LogHelper logHelper = new LogHelper();

        public static MSDatosUsuarioYPerfiles LoginAlianzaValle(MELogin entrada) {
            MSDatosUsuarioYPerfiles salida = new MSDatosUsuarioYPerfiles();

            salida.DatosUsuario = new MSDatosUsuario();
            salida.DatosUsuario.Nombres = "Usuario";
            salida.DatosUsuario.Apellidos = entrada.Login;
            salida.DatosUsuario.IdUsuario = 123;
            salida.DatosUsuario.Identificacion = "171717171717";
            salida.DatosUsuario.Email = $"{entrada.Login}@info.com";
            salida.DatosUsuario.OficinaTrx = 1;
            salida.DatosUsuario.DescripcionOficinaTrx = "Matriz";


            //try {
            //    salida = HelperCoreApi.CallApiService<MSDatosUsuarioYPerfiles, MELogin>(string.Join("", Constante.SERVIDOR_ALIANZA, ApiSeguridades.AutenticaConRetornoDatosYPerfiles), entrada);
            //} catch (Exception ex) {
            //    log.LogError($"Login: {ApiSeguridades.AutenticaConRetornoDatosYPerfiles} - {ex}");
            //    throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{ConvertObject.Serialize(entrada)} | {ConvertObject.Serialize(salida)}");
            //}

            //if (!salida.Ok) {
            //    throw new CoreExcepcion(EnumMensaje.ERROR, Constante.VACIO, salida.MensajeError);
            //}
            //if (!salida.DatosUsuario.Ok) {
            //    throw new CoreExcepcion(EnumMensaje.ERROR, Constante.VACIO, salida.DatosUsuario.MensajeError);
            //}
            //if (!salida.Perfiles.Ok) {
            //    throw new CoreExcepcion(EnumMensaje.ERROR, Constante.VACIO, salida.Perfiles.MensajeError);
            //}

            return salida;
        }

        public static MSDatosCliente LlenarDatosCliente(MEIdentificacion entrada) {
            MSDatosCliente salida;

            try {
                salida = HelperCoreApi.CallApiService<MSDatosCliente, MEIdentificacion>(string.Join("", Constante.SERVIDOR_ALIANZA, ApiClientes.ConsultarDatosCliente), entrada);
            } catch {
                throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{ApiClientes.ConsultarDatosCliente} | {entrada.Identificacion}");
            }
            return salida;
        }

        public static MSDatosCliente ConsultarDatosBasicosCliente(MEIdentificacion entrada) {
            MSDatosCliente salida;

            try {
                salida = HelperCoreApi.CallApiService<MSDatosCliente, MEIdentificacion>(string.Join("", Constante.SERVIDOR_ALIANZA, ApiClientes.ConsultarDatosBasicosCliente), entrada);
            } catch {
                throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{ApiClientes.ConsultarDatosBasicosCliente} | {entrada.Identificacion}");
            }
            return salida;
        }

        public static MSPosicionConsolidada PosicionConsolidada(MEIdentificacion entrada) {
            MSPosicionConsolidada salida;

            try {
                salida = HelperCoreApi.CallApiService<MSPosicionConsolidada, MEIdentificacion>(string.Join("", Constante.SERVIDOR_ALIANZA, ApiCuentas.ConsultarPosicionConsolidada), entrada);
            } catch {
                throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{ApiCuentas.ConsultarPosicionConsolidada} | {entrada.Identificacion}");
            }
            return salida;
        }

        public static MSMovimientosCuentaLista ConsultaCuentaMovimientos(MEConsultaMovimientos entrada) {
            MSMovimientosCuentaLista salida;
            try {
                salida = HelperCoreApi.CallApiService<MSMovimientosCuentaLista, MEConsultaMovimientos>(string.Join("", Constante.SERVIDOR_ALIANZA, ApiCuentas.ConsultarMovimientos), entrada);
            } catch {
                throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{ApiCuentas.ConsultarMovimientos} | {entrada.NumeroCuenta} | {entrada.FechaDesde} | {entrada.FechaHasta}");
            }
            return salida;
        }

        public static MSTablaAmortizacionCreditoLista ConsultaCuentaMovimientos(MEOperacion entrada) {
            MSTablaAmortizacionCreditoLista salida;
            try {
                salida = HelperCoreApi.CallApiService<MSTablaAmortizacionCreditoLista, MEOperacion>(string.Join("", Constante.SERVIDOR_ALIANZA, ApiCreditos.ConsultaTablaAmortizacion), entrada);
            } catch {
                throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{ApiCuentas.ConsultarMovimientos} | {entrada.NumeroOperacion}");
            }
            return salida;
        }

        public static MSDatosBasicosClienteDigital ConsultarDatosBasicosClienteDigital(MEIdentificacion entrada, bool validar) {
            MSDatosBasicosClienteDigital salida = new MSDatosBasicosClienteDigital();
            string metodo = "ClienteDigital/ConsultarClienteBasicoPorIdentificacion";

            try {
                salida = HelperCoreApi.CallApiService<MSDatosBasicosClienteDigital, MEIdentificacion>(string.Join("", Constante.SERVIDOR_ALIANZA_CENTRAL, metodo), entrada);
            } catch (Exception ex) {
                if (validar) {
                    throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{entrada.Identificacion}");
                } else {
                    logHelper.GrabarLog(MethodBase.GetCurrentMethod(), ConvertObject.Serialize(entrada), ex, true);
                }
            }
            return salida;
        }

        public static MSCreditoDigital ConsultarSolicitudCreditoDigital(MEIdentificacion entrada, bool validar) {
            MSCreditoDigital salida = new MSCreditoDigital();
            string metodo = "SolicitudCreditoDigital/ConsultarCreditoDigitalPorIdentificacion";

            try {
                salida = HelperCoreApi.CallApiService<MSCreditoDigital, MEIdentificacion>(string.Join("", Constante.SERVIDOR_ALIANZA_CENTRAL, metodo), entrada);
            } catch (Exception ex) {
                if (validar) {
                    throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{entrada.Identificacion}");
                } else {
                    logHelper.GrabarLog(MethodBase.GetCurrentMethod(), ConvertObject.Serialize(entrada), ex, true);
                }
            }
            return salida;
        }

        public static MSBase EnviarSms(MESmsParametro entrada, bool validar = true) {
            MSBase salida = new MSBase();
            string metodo = "Mensajeria/EnviarSms";

            try {
                salida = HelperCoreApi.CallApiService<MSBase, MESmsParametro>(string.Join("", Constante.SERVIDOR_ALIANZA_CENTRAL, metodo), entrada);
            } catch (Exception ex) {
                if (validar) {
                    throw new CoreExcepcion(EnumMensaje.ERROR_CORE, $"{entrada.Celular} | {entrada.IdProducto}");
                } else {
                    logHelper.GrabarLog(MethodBase.GetCurrentMethod(), ConvertObject.Serialize(entrada), ex, true);
                }
            }
            return salida;
        }

    }

}
