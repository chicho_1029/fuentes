﻿using AlianzaCoreMensajes;
using Util.Excepcion;
using Util.Request;

namespace Business.Util.Alianza {

    /// <summary>
    /// Clase utilitaria de datos del cliente
    /// </summary>
    internal static class Cliente {

        public static MSDatosCliente DatosCliente(RequestBase request, string identificacion, bool validar = true) {
            MEIdentificacion entrada = Entrada.Identificacion(request.Ccanal, identificacion, request.Cusuario);
            MSDatosCliente salida = Salida.LlenarDatosCliente(entrada);

            if (validar) ValidarDatosCliente(salida);
            return salida;
        }

        public static MSDatosCliente DatosCliente(string ccanal, string identificacion, string cusuario, bool validar = true) {
            MEIdentificacion entrada = Entrada.Identificacion(ccanal, identificacion, cusuario);
            MSDatosCliente salida = Salida.LlenarDatosCliente(entrada);

            if (validar) ValidarDatosCliente(salida);
            return salida;
        }

        public static MSDatosBasicosClienteDigital DatosBasicosClienteDigital(string ccanal, string identificacion, string cusuario, bool validar = true) {
            MEIdentificacion entrada = Entrada.Identificacion(ccanal, identificacion, cusuario);
            MSDatosBasicosClienteDigital salida = Salida.ConsultarDatosBasicosClienteDigital(entrada, validar);

            return salida;
        }

        private static void ValidarDatosCliente(MSDatosCliente cliente) {
            if (cliente == null) {
                throw new CoreExcepcion("3002", "CLIENTE NO REGISTRADO");
            }

            if (string.IsNullOrEmpty(cliente.Email)) {
                throw new CoreExcepcion("3011", $"NO POSEE CORREO ELECTRONICO: {cliente.Identificacion}");
            }

            if (string.IsNullOrEmpty(cliente.Celular)) {
                throw new CoreExcepcion("3014", $"NO REGISTRA NUMERO CELULAR: {cliente.Identificacion}");
            }
        }
    }

}
