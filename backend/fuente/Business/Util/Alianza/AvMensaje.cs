﻿using System;
using System.Collections.Generic;

namespace Business.Util.Alianza {

    #region MENSAJES ENTRADAS

    public class MESmsParametro {

        public virtual string Celular { get; set; }
        public virtual int IdCliente { get; set; }
        public virtual int IdProducto { get; set; }
        public virtual string Producto { get; set; }
        public virtual string MensajeInicio { get; set; }
        public virtual string MensajeIntermedio { get; set; }
        public virtual string MensajeIntermedio1 { get; set; }
        public virtual string MensajeFinal { get; set; }

    }

    #endregion

    #region MENSAJES SALIDAS

    public class MSRespuesta {

        public virtual string Code { get; set; }
        public virtual List<MSRespuestaData> Data { get; set; } = new List<MSRespuestaData>();

    }

    public class MSRespuestaData {

        public virtual string CodigoRespuesta { get; set; }
        public virtual string MensajeRespuesta { get; set; }
        public virtual bool EsProcesoExitoso { get; set; }

    }

    public class MSDatosBasicosClienteDigital {

        public virtual string Identificacion { get; set; }
        public virtual string NombreCompleto { get; set; }
        public virtual string Mail { get; set; }
        public virtual string TelefonoMovil { get; set; }

    }

    public class MSClienteDigital {

        public virtual int IdClienteDigital { get; set; }
        public virtual int? IdCliente { get; set; }
        public virtual bool Titular { get; set; }
        public virtual string Identificacion { get; set; }
        public virtual string CodigoDactilar { get; set; }
        public virtual string ApellidoPaterno { get; set; }
        public virtual string ApellidoMaterno { get; set; }
        public virtual string Nombres { get; set; }
        public virtual string NombreCompleto { get => string.Join(" ", Nombres, ApellidoPaterno, ApellidoMaterno); }
        public virtual string EstadoCivil { get; set; }
        public virtual DateTime? FechaNacimiento { get; set; }
        public virtual string Genero { get; set; }
        public virtual string TelefonoMovil { get; set; }
        public virtual string Mail { get; set; }
        public virtual string Score { get; set; }

    }

    public class MSUbicacionClienteDigital {

        public string DescripcionProvincia { get; set; }
        public string DescripcionCiudad { get; set; }
        public string DescripcionCanton { get; set; }
        public string DescripcionParroquia { get; set; }
        public virtual string GeoLatitud { get; set; }
        public virtual string GeoLongitud { get; set; }
        public virtual string Ubicacion { get => string.Join(" / ", DescripcionProvincia, DescripcionCiudad, DescripcionParroquia); }

    }

    public class MSCreditoDigital {

        public virtual MSCreditoDigitalSolicitud Solicitud { get; set; }
        public virtual MSClienteDigital Cliente { get; set; }
        public virtual MSClienteDigital Conyuge { get; set; }
        public virtual MSUbicacionClienteDigital ClienteUbicacion { get; set; }
        public virtual MSCreditoDigitalEvaluacion Evaluacion { get; set; }
        public virtual MSCredioDigitalDeposito Deposito { get; set; }
        public virtual MSCreditoDigitalSolicitud SolicitudRelacion { get; set; }
        public virtual List<MSCreditoDigitalLog> LogSolicitud { get; set; }
        public virtual List<MSCreditoDigitalRechazo> DatosActualizar { get; set; }

    }

    public class MSResumenCreditoDigital {

        public virtual MSCreditoDigitalSolicitud Solicitud { get; set; }
        public virtual MSCredioDigitalDeposito Deposito { get; set; }
        public virtual MSCreditoDigitalSolicitud SolicitudRelacion { get; set; }
        public virtual List<MSClienteDigital> SolicitudClientes { get; set; } = new List<MSClienteDigital>();
        public virtual List<MSCreditoDigitalLog> LogSolicitud { get; set; }
        public virtual List<MSCreditoDigitalRechazo> DatosActualizar { get; set; }

    }

    public class MSCreditoDigitalSolicitud {

        public virtual int IdSolicitud { get; set; }
        public virtual string IdentificacionTitular { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string FechaSolicitud { get; set; }
        public virtual string FechaExpiracion { get; set; }
        public virtual decimal Monto { get; set; }
        public virtual int PlazoMeses { get; set; }
        public virtual decimal Cuota { get; set; }
        public virtual decimal Tasa { get; set; }
        public virtual string DescripcionEstado { get; set; }
        public virtual string DescripcionEtapa { get; set; }
        public virtual string UsuarioAsignado { get; set; }
        public virtual int NuevoRenovado { get; set; }
        public virtual string DescripcionTipoCredito { get; set; }
        public virtual string Segmentacion { get; set; }
        public virtual string Ubicacion { get; set; }

    }

    public class MSCreditoDigitalEvaluacion {

        public virtual int Score { get; set; }
        public virtual string Identificacion { get; set; }
        public virtual string ResultadoEvaluacion { get; set; }
        public virtual string ResultadoSegmentacion { get; set; }
        public virtual decimal IngresoPromedio { get; set; }
        public virtual decimal TotalDeudaCuota { get; set; }
        public virtual decimal TotalDeudaSaldo { get; set; }
        public virtual string ResultadoMensaje { get; set; }
        public virtual string ScoreDeudor { get; set; }
        public virtual string ScoreConyuge { get; set; }
        public virtual string RangoIngreso { get; set; }
        public virtual DateTime FechaEvaluacion { get; set; }
        public virtual decimal CapacidadPago { get; set; }
        public virtual decimal MontoSugerido { get; set; }
        public virtual int PlazoSugerido { get; set; }
        public virtual decimal CuotaAproximada { get; set; }

    }

    public class MSCredioDigitalDeposito {

        public virtual int IdSolicitud { get; set; }
        public virtual string NumeroCuenta { get; set; }
        public virtual int Sufijo { get; set; }
        public virtual decimal ValorDeposito { get; set; }
        public virtual DateTime FechaExpiracion { get; set; }
        public virtual bool? DepositoCompleto { get; set; }
        public virtual DateTime? FechaDeposito { get; set; }
        public virtual decimal MontoDepositado { get; set; }

    }

    public class MSCreditoDigitalLog {

        public virtual int IdLog { get; set; }
        public virtual DateTime FechaRegistro { get; set; }
        public virtual string DescripcionEtapa { get; set; }
        public virtual string DescripcionEstado { get; set; }
        public virtual string DescripcionEvaluacion { get; set; }

    }

    public class MSCreditoDigitalRechazo {

        public virtual int IdRegistro { get; set; }
        public virtual int IdDetalleCatalogo { get; set; }
        public virtual string DescripcionDetalle { get; set; }
        public virtual DateTime FechaRegistro { get; set; }
        public virtual DateTime? FechaActualizacion { get; set; }
        public virtual bool Estado { get; set; }

    }

    #endregion

}
