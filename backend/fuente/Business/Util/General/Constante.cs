﻿using AlianzaCoreHelper;
using Util.Configuracion;

namespace Business.Util.General {

    /// <summary>
    /// Clase que define constantes y metodos utilitarios.
    /// </summary>
    public struct Constante {

        private static readonly AlianzaValle config = SettingsManager.GetAlianzaValle();

        public static readonly string SERVIDOR_ALIANZA = Servidor.PruebasExterna;

        public static readonly string SERVIDOR_ALIANZA_CENTRAL = config.ServerCentralIn;

        public const string CANAL_OFICINA = "IN";

        public const string CANAL_WEB = "WEB";

        public const string VACIO = "";

        public const string OK = "OK";

        public const string SI = "S";

        public const string NO = "N";

        public const int CERO = 0;

        public const int UNO = 1;

        public const int DOS = 2;

        public const int CODIGO_FACIAL = -70;

        public const string ERROR_VACIO = "{0}";

        public const string CARACTERES_RESERVADOS = "[@&'(\\s)?!<>#*^_$%.]";

        public const string CARACTERES_NOTIFICACION = "[@&'(\\s)?!<>#*^_$%.,{}\\-\"\\/]";

    }

}
