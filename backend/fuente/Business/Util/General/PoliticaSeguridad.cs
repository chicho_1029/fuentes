﻿using Modelo.Entities;
using System;
using System.Linq;
using Util.Excepcion;
using Util.Seguridad;

namespace Business.Util.General {

    /// <summary>
    /// Clase utilitaria que valida las politicas de seguridad por canal.
    /// </summary>
    public class PoliticaSeguridad {

        /* Caracteres en mayusculas. */
        private static readonly string UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZÑÁÉÍÓÚ";
        /** Caracteres en minusculas. */
        private static readonly string LOWER = "abcdefghijklmnopqrstuvwxyzñáéíóú";
        /** Caracteres numericos. */
        private static readonly string NUMBERS = "0123456789";
        /** Numero de caracteres numericos. */
        private int contadornumeros = 0;
        /** Numero de mayusculas. */
        private int contadormayusculas = 0;
        /** Numero de minusculas. */
        private int contadorminusculas = 0;
        /** Numero de caraceres especiales. */
        private int contadorcaracteresespeciales = 0;

        private string nuevoPasswdencriptado = "";

        /// <summary>
        /// Metodo que valida la politica de seguridad del password.
        /// </summary>
        public void Validate(Corecanal canal, string value) {
            nuevoPasswdencriptado = Password.Encriptar(value);
            PoliticaSeguridad.ValidateLength(canal, value);
            for (int i = 0; i < value.Count(); i++) {
                string c = value.Substring(i, 1);
                this.Compute(c);
            }
            this.ValidateNumbers(canal);
            this.ValidateUppercase(canal);
            this.ValidateLowercase(canal);
            this.ValidateSpecial(canal);
        }

        /// <summary>
        /// Metodo que valida la longitud del password.
        /// </summary>
        private static void ValidateLength(Corecanal canal, string value) {
            if (canal.Longitud != null && value.Count() < canal.Longitud) {
                throw new CoreExcepcion("1007", "LONGITUD MINIMA DEL PASSWORD DEBE SER DE {0} CARACTERES", canal.Longitud);
            }
        }

        /// <summary>
        /// Metodo que valida numeros minimos en el password.
        /// </summary>
        private void ValidateNumbers(Corecanal canal) {
            if (canal.Numeros != null && canal.Numeros > 0 && canal.Numeros > this.contadornumeros) {
                throw new CoreExcepcion("1008", "EL PASSWORD DEBE TENER POR LO MENOS {0} NUMEROS", canal.Numeros);
            }
        }

        /// <summary>
        /// Metodo que valida letras mayusculas minimas en el password.
        /// </summary>
        private void ValidateUppercase(Corecanal canal) {
            if (canal.Mayusculas != null && canal.Mayusculas > 0 && canal.Mayusculas > this.contadormayusculas) {
                throw new CoreExcepcion("1009", "EL PASSWORD DEBE TENER POR LO MENOS {0} LETRAS MAYUSCULAS", canal.Mayusculas);
            }
        }

        /// <summary>
        /// Metodo que valida letras minusculas minimas en el password.
        /// </summary>
        private void ValidateLowercase(Corecanal canal) {
            if (canal.Minusculas != null && canal.Minusculas > 0 && canal.Minusculas > this.contadorminusculas) {
                throw new CoreExcepcion("1010", "EL PASSWORD DEBE TENER POR LO MENOS {0} LETRAS MINUSCULAS", canal.Minusculas);
            }
        }

        /// <summary>
        /// Metodo que valida los caracteres especiales.
        /// </summary>
        private void ValidateSpecial(Corecanal canal) {
            if (canal.Especiales != null && canal.Especiales > 0 && canal.Especiales > this.contadorcaracteresespeciales) {
                throw new CoreExcepcion("1011", "EL PASSWORD DEBE TENER POR LO MENOS {0} CARACTERES ESPECIALES", canal.Especiales);
            }
        }

        /// <summary>
        /// Metodo que se encarga de acumular, numeros, mayusculas y/o minusculas.
        /// </summary>
        private void Compute(string c) {
            if (PoliticaSeguridad.Esnumerico(c)) {
                this.contadornumeros++;
                return;
            }
            if (PoliticaSeguridad.Esmayuscula(c)) {
                this.contadormayusculas++;
                return;
            }
            if (PoliticaSeguridad.Esminuscula(c)) {
                this.contadorminusculas++;
                return;
            }
            // Si no es numero, mayuscula o minuscula es un caractwer especial
            this.contadorcaracteresespeciales++;
        }

        /// <summary>
        /// Metdo que verifica si el caracter es un numero.
        /// </summary>
        private static Boolean Esnumerico(string value) {
            if (PoliticaSeguridad.NUMBERS.Contains(value)) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Metdo que verifica si el caracter es una mayuscula, incluye tildes.
        /// </summary>
        private static Boolean Esmayuscula(string value) {
            if (PoliticaSeguridad.UPPER.Contains(value)) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Metdo que verifica si el caracter es una minuscula incluye tildes.
        /// </summary>
        private static Boolean Esminuscula(string value) {
            if (PoliticaSeguridad.LOWER.Contains(value)) {
                return true;
            }
            return false;
        }
    }
}
