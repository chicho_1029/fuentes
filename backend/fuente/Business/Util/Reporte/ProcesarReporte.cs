﻿using Business.Util.Helper;
using Dal.Core;
using Modelo.Dto.General;
using System.Collections.Generic;
using Util.Componentes;
using Util.Request;

namespace Business.Util.Reporte {

    /// <summary>
    /// Clase que procesa y genera reporte.
    /// </summary>
    public class ProcesarReporte : ClaseBusinessQuery {

        public override Response Ejecutar(RequestQuery request) {
            ReporteDatos(request);
            return request.Response;
        }

        private void ReporteDatos(RequestQuery request) {
            string nombreReporte = string.Join("", request.GetDatosString("nombrereporte"), ".rpt");
            string tipoReporte = request.GetDatosString("tipo");
            IList<ReporteParametroDto> parametros = ConvertObject.SerializeDeserialize<List<ReporteParametroDto>>(request.GetDatosObject("parametrosreporte"));

            ReporteHelper exportarReporte = new ReporteHelper(CoreInstanciaDal.GetInstancia(1), parametros, nombreReporte, tipoReporte);
            //byte[] rpt = exportarReporte.GenerarReporte();
            //request.Response.Add("reporte", rpt);
        }

    }

}
