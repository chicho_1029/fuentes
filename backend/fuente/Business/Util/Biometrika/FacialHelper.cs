﻿using Dal.Base;
using System;
using System.Reflection;
using Util.Request;
using WSBiometrika;

namespace Business.Util.Biometrika {

    /// <summary>
    /// Clase utilitaria de datos faciales con Biometrika
    /// </summary>
    internal static class FacialHelper {

        private static readonly LogHelper logHelper = new LogHelper();
        private static readonly BioIntegraSoapClient clientBio = new BioIntegraSoapClient(BioIntegraSoapClient.EndpointConfiguration.BioIntegraSoap);

        public static MSFacialCrear CrearFacial(string identificacion) {
            bool error = false;
            MSFacialCrear salida = new MSFacialCrear();

            try {
                MEFacialCrear entrada = new MEFacialCrear(identificacion);
                salida = ConvertObject.Deserialize<MSFacialCrear>(clientBio.CreaTknXml(ConvertObject.Serialize(entrada)));
            } catch (Exception ex) {
                error = true;
                logHelper.GrabarLog(MethodBase.GetCurrentMethod(), string.Join(" ~ ", identificacion, ConvertObject.Serialize(salida)), ex, error);
            } finally {
                if (!error) {
                    logHelper.GrabarLog(MethodBase.GetCurrentMethod(), string.Join(" ~ ", identificacion, ConvertObject.Serialize(salida)));
                }
            }

            return salida;
        }

        public static MSFacialEstatus EstatusFacial(string token) {
            MSFacialEstatus salida = new MSFacialEstatus();

            try {
                MEFacialBase entrada = new MEFacialBase();
                salida = ConvertObject.Deserialize<MSFacialEstatus>(clientBio.ConsultaTknXml(entrada.origen, token));
            } catch (Exception ex) {
                logHelper.GrabarLog(MethodBase.GetCurrentMethod(), string.Join(" ~ ", token, ConvertObject.Serialize(salida)), ex, true);
            }

            return salida;
        }

        public static MSFacialConsulta ConsultarFacial(string token, string imagenes) {
            MSFacialConsulta salida = new MSFacialConsulta();

            try {
                MEFacialBase entrada = new MEFacialBase();
                salida = ConvertObject.Deserialize<MSFacialConsulta>(clientBio.ConsultaTrIdXml(entrada.origen, token, imagenes));
            } catch (Exception ex) {
                logHelper.GrabarLog(MethodBase.GetCurrentMethod(), string.Join(" ~ ", token, ConvertObject.Serialize(salida)), ex, true);
            }

            return salida;
        }
    }

}
