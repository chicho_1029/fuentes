﻿using Util.Configuracion;

namespace Business.Util.Biometrika {

    #region ENTRADAS FACIAL

    public class MEFacialBase {

        internal static readonly AlianzaValle config = SettingsManager.GetAlianzaValle();
        public virtual string origen { get; set; }

        public MEFacialBase() {
            this.origen = config.Biometrika.Token;
        }

    }

    public class MEFacialCrear : MEFacialBase {

        public virtual string identificacion { get; set; }
        public virtual string nivel { get; set; }

        public MEFacialCrear(string identificacion) {
            this.identificacion = identificacion;
            this.nivel = config.Biometrika.Nivel;
        }

    }

    #endregion

    #region SALIDAS FACIAL

    public class MSFacialBase {

        public virtual int Respuesta { get; set; }
        public virtual string Mensaje { get; set; }
        public virtual string TransaccionId { get; set; }
        public virtual string Fecha { get; set; }
        public virtual string Id { get; set; }

        public MSFacialBase() {
            Respuesta = -1;
        }

    }

    public class MSFacialCrear : MSFacialBase {

        public virtual string Link { get; set; }
        public virtual int Intervalo { get; set; }

    }

    public class MSFacialEstatus : MSFacialBase {

        public virtual string RLink { get; set; }
        public virtual string Foto { get; set; }

    }

    public class MSFacialConsulta : MSFacialBase {

        public virtual byte[] ImagenV { get; set; }
        public virtual byte[] ImagenE { get; set; }
        public virtual int RespuestaEvt { get; set; }
        public virtual string MensajeEvt { get; set; }
        public virtual string Tipo { get; set; }
        public virtual string TransaccionIdEvt { get; set; }
        public virtual string FechaEvt { get; set; }
        public virtual decimal TasaFarEvt { get; set; }
        public virtual string Identificacion { get; set; }

    }

    #endregion

}
