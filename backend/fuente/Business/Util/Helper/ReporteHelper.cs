﻿using Modelo.Dto.General;
using Modelo.Entities;
using System.Collections.Generic;

namespace Business.Util.Helper {

    /// <summary>
    /// Clase utilitaria para la generación de reportes.
    /// </summary>
    public class ReporteHelper {

        private readonly Coreinstancia instancia;
        private readonly IList<ReporteParametroDto> parametros;
        private readonly string nombreReporte;
        private readonly string tipoReporte;

        // Crea una instancia de ReporteHelper.
        public ReporteHelper(Coreinstancia instancia, IList<ReporteParametroDto> parametros, string nombreReporte, string tipoReporte) {
            this.instancia = instancia;
            this.parametros = parametros;
            this.nombreReporte = nombreReporte;
            this.tipoReporte = tipoReporte;
        }
    }
}
