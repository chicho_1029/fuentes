﻿using Business.Util.General;
using CoreSecurity;
using Dal.Base;
using Microsoft.Extensions.Logging;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Util.Configuracion;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Business.Util.Helper {

    /// <summary>
    /// Clase que utilitaria, encargada de ejecutar un servicio externo.
    /// </summary>
    public static class ServicioHelper {

        private static readonly ILogger log = SettingsManager.GetLogger();
        private static readonly LogHelper logHelper = new LogHelper();

        public static T EjecutaServicio<T>(Coreservicio service, Dictionary<string, object> parameters) {
            string requestService = ConvertObject.Serialize(parameters);
            string responseService = string.Empty;

            try {
                responseService = GetServicio(service, requestService);
                return ConvertObject.Deserialize<T>(responseService);
            } finally {
                if (service.Registralog)
                    logHelper.GrabarLog(service.Url, string.Join(" ~ ", requestService, responseService));
            }
        }

        private static string GetServicio(Coreservicio service, string data) {
            string response = string.Empty;

            try {
                HttpWebRequest httpRequest = WebRequest.Create(service.Url) as HttpWebRequest;
                httpRequest.Method = service.Metodo;

                if (service.Autorizacion) {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }

                if (service.Autenticacion) {
                    string authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(string.Join(":", SecurityLib.OpenSSLDecrypt(service.Autenticacionusuario), SecurityLib.OpenSSLDecrypt(service.Autenticacionpassword))));
                    httpRequest.Headers["Authorization"] = "Basic " + authInfo;
                }

                if (service.Metodo == "POST") {
                    byte[] bufferData = Encoding.UTF8.GetBytes(data);

                    httpRequest.KeepAlive = false;
                    httpRequest.UseDefaultCredentials = service.Credenciales;
                    httpRequest.ContentType = service.Tipocontenido;
                    httpRequest.ContentLength = bufferData.Length;

                    using (Stream streamRequest = httpRequest.GetRequestStream()) {
                        streamRequest.Write(bufferData, 0, bufferData.Length);
                        streamRequest.Flush();
                        streamRequest.Close();
                    }
                }

                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                using (Stream streamResponse = httpResponse.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(streamResponse)) {
                    response = streamReader.ReadToEnd();

                    streamReader.Close();
                    streamResponse.Close();
                    httpResponse.Close();
                }
            } catch (WebException ex) {
                StreamReader stream = new StreamReader(ex.Response.GetResponseStream());
                response = stream.ReadToEnd();

                log.LogError($"Servicio: {service.Url} - Response - {response} - WebException {ex}");
            } catch (Exception ex) {
                log.LogError($"Servicio: {service.Url} - Response - {response} - Exception {ex}");
                throw new CoreExcepcion(EnumMensaje.ERROR, Constante.ERROR_VACIO, ex.Message);
            }

            return response;
        }

    }

}
