﻿using CoreSecurity;
using Microsoft.Extensions.Logging;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Util.Configuracion;
using Util.Request;

namespace Business.Util.Helper {

    /// <summary>
    /// Clase que utilitaria, encargada de ejecutar un servicio externo.
    /// </summary>
    public class ServicioThreadHelper {

        private readonly ILogger log = SettingsManager.GetLogger();
        private readonly Coreservicio service;
        private readonly string data;

        // Crea una instancia de ServicioThreadHelper.
        public ServicioThreadHelper(Coreservicio service, Dictionary<string, object> parameters) {
            this.service = service;
            if (service.Encriptar) {
                this.data = ConvertObject.Serialize(SecurityLib.OpenSSLEncrypt(ConvertObject.Serialize(parameters)));
            } else {
                this.data = ConvertObject.Serialize(parameters);
            }
        }

        public void Ejecutar() {
            Thread thread = new Thread(GetServicio);
            thread.Start();
        }

        private void GetServicio() {
            string response = string.Empty;

            try {
                HttpWebRequest httpRequest = WebRequest.Create(service.Url) as HttpWebRequest;
                httpRequest.Method = service.Metodo;

                if (service.Autorizacion) {
                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                }

                if (service.Autenticacion) {
                    string authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(string.Join(":", SecurityLib.OpenSSLDecrypt(service.Autenticacionusuario), SecurityLib.OpenSSLDecrypt(service.Autenticacionpassword))));
                    httpRequest.Headers["Authorization"] = "Basic " + authInfo;
                }

                if (service.Metodo == "POST") {
                    byte[] bufferData = Encoding.UTF8.GetBytes(data);

                    httpRequest.KeepAlive = false;
                    httpRequest.UseDefaultCredentials = service.Credenciales;
                    httpRequest.ContentType = service.Tipocontenido;
                    httpRequest.ContentLength = bufferData.Length;
                    httpRequest.Timeout = service.Timeout * 60000;

                    using (Stream streamRequest = httpRequest.GetRequestStream()) {
                        streamRequest.Write(bufferData, 0, bufferData.Length);
                        streamRequest.Flush();
                        streamRequest.Close();
                    }
                }

                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                using (Stream streamResponse = httpResponse.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(streamResponse)) {
                    response = streamReader.ReadToEnd();

                    streamReader.Close();
                    streamResponse.Close();
                    httpResponse.Close();
                }
            } catch (WebException ex) {
                log.LogError($"Servicio Thread: {service.Url} - WebException {ex}");
            } catch (Exception ex) {
                log.LogError($"Servicio Thread: {service.Url} - Response - {response} - Exception {ex}");
            }
        }

    }

}
