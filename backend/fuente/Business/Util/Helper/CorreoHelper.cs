﻿using CoreSecurity;
using Dal.Base;
using Modelo.Entities;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using Util.General;
using Util.Request;

namespace Business.Util.Helper {

    /// <summary>
    /// Clase utilitaria de envio de correo electronico.
    /// </summary>
    public class CorreoHelper {

        private readonly Corecorreo correo;
        private readonly Corenotificacion notificacion;
        private readonly IList<Corenotificacionenvio> listaEnvios = new List<Corenotificacionenvio>();
        private readonly string usuario;
        private readonly string token;

        public IList<string> ListAddress { get; set; } = new List<string>();
        public IList<string> ListCopyAddress { get; set; } = new List<string>();
        public Dictionary<string, byte[]> ListAttachments { get; set; } = new Dictionary<string, byte[]>();
        public Dictionary<string, object> ListParameters { get; set; } = new Dictionary<string, object>();


        // Crea una instancia de CorreoHelper.
        public CorreoHelper(RequestBase request, Corecorreo correo, Corenotificacion notificacion) {
            this.correo = correo;
            this.notificacion = notificacion;
            usuario = request.Cusuario;
            token = request.Token;
        }

        private void Enviar() {
            try {
                var fromAddress = new MailAddress(correo.Direccion, correo.Remitente);
                var message = new MailMessage();
                message.From = fromAddress;
                message.Subject = notificacion.Asunto;
                message.Body = notificacion.Texto;
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;

                foreach (KeyValuePair<string, byte[]> entry in ListAttachments) {
                    message.Attachments.Add(new Attachment(new MemoryStream(entry.Value), entry.Key));
                }

                foreach (string item in ListAddress) {
                    if (notificacion.Registraenvio) {
                        Corenotificacionenvio envio = new Corenotificacionenvio();
                        message.To.Add(new MailAddress(item.Trim()));
                        envio.Cnotificacion = notificacion.Cnotificacion;
                        envio.Correo = item.Trim();
                        envio.Parametros = FormatObject.Objeto(ListParameters);
                        envio.Freal = Fecha.GetFechaActual();
                        envio.Cusuario = usuario;
                        envio.Token = token;

                        listaEnvios.Add(envio);
                    }
                }

                foreach (string item in ListCopyAddress) {
                    if (notificacion.Registraenvio) {
                        Corenotificacionenvio envio = new Corenotificacionenvio();
                        message.CC.Add(new MailAddress(item.Trim()));
                        envio.Cnotificacion = notificacion.Cnotificacion;
                        envio.Correo = item.Trim();
                        envio.Parametros = FormatObject.Objeto(ListParameters);
                        envio.Freal = Fecha.GetFechaActual();
                        envio.Cusuario = usuario;
                        envio.Token = token;

                        listaEnvios.Add(envio);
                    }
                }

                foreach (var param in ListParameters) {
                    string textoNormalizado = param.Value.ToString().Normalize(NormalizationForm.FormD);
                    message.Subject = message.Subject.Replace(param.Key, textoNormalizado);
                    message.Body = message.Body.Replace(param.Key, textoNormalizado);
                }

                //Envía el mensaje.
                SmtpClient smtp = new SmtpClient();
                smtp.Host = correo.Hostsmtp;
                smtp.Port = correo.Puertosmtp;
                smtp.EnableSsl = correo.Ssl;
                smtp.Credentials = new NetworkCredential(fromAddress.Address, SecurityLib.OpenSSLDecrypt(correo.Password));

                smtp.Send(message);
                message.Dispose();
                NotificacionEnvio(false);
            } catch (System.Exception ex) {
                NotificacionEnvio(true, ex.Message);
            }
        }

        public void Ejecutar() {
            Thread thread = new Thread(Enviar);
            thread.Start();
        }

        public void NotificacionEnvio(bool error, string respuesta = "OK") {
            foreach (Corenotificacionenvio envio in listaEnvios) {
                envio.Respuesta = respuesta;
                envio.Error = error;
                ThreadInsert.Save(envio);
            }
        }

    }
}
