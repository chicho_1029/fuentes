﻿using Business.Util.Alianza;
using Business.Util.General;
using Dal.Core;
using Modelo.Entities;
using System.Collections.Generic;
using Util.Enumeracion;
using Util.Excepcion;

namespace Business.Util.Helper {

    /// <summary>
    /// Clase helper de generacion y envio de usuario Forti.
    /// </summary>
    internal static class FortiHelper {

        public static MSRespuestaData GenerarForti(string usuarioForti, string celular, bool retornaError = false) {
            Coreservicio servicioActualizar = CoreServicioDal.GetServicio(11);
            Dictionary<string, object> parametros = new Dictionary<string, object> {
                { "Canal", Constante.CANAL_WEB },
                { "Usuario", usuarioForti },
                { "Movil", celular },
                { "Tipo", "A" }
            };

            MSRespuesta respuestaForti = ServicioHelper.EjecutaServicio<MSRespuesta>(servicioActualizar, parametros);
            if (!string.Equals(respuestaForti.Code, "0")) {
                if (retornaError) {
                    return respuestaForti.Data[0];
                } else {
                    throw new CoreExcepcion(EnumMensaje.ERROR, Constante.ERROR_VACIO, respuestaForti.Data[0].MensajeRespuesta);
                }
            }

            // Generar Código de Seguridad OTP

            Coreservicio servicioOtp = CoreServicioDal.GetServicio(12);
            MSRespuesta respuestaOtp = ServicioHelper.EjecutaServicio<MSRespuesta>(servicioOtp, parametros);
            if (!string.Equals(respuestaOtp.Code, "0")) {
                if (retornaError) {
                    return respuestaOtp.Data[0];
                } else {
                    throw new CoreExcepcion(EnumMensaje.ERROR, Constante.ERROR_VACIO, respuestaOtp.Data[0].MensajeRespuesta);
                }
            }

            return respuestaOtp.Data[0];
        }

        public static MSRespuestaData ValidarForti(string usuarioForti, string codigo) {
            Coreservicio servicioForti = CoreServicioDal.GetServicio(13);
            Dictionary<string, object> parametros = new Dictionary<string, object> {
                { "Canal", Constante.CANAL_WEB },
                { "Usuario", usuarioForti },
                { "Token", codigo }
            };

            MSRespuesta respuestaForti = ServicioHelper.EjecutaServicio<MSRespuesta>(servicioForti, parametros);
            if (!string.Equals(respuestaForti.Code, "0")) {
                throw new CoreExcepcion(EnumMensaje.ERROR, $"ERROR EN VALIDACION DE TOKEN: {codigo}", respuestaForti.Data[0].MensajeRespuesta);
            }

            return respuestaForti.Data[0];
        }

    }

}
