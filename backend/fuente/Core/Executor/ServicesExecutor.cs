﻿using Dal.Base;
using Microsoft.Extensions.Logging;
using NHibernate;
using System;
using System.Data;
using Util.Configuracion;
using Util.General;
using Util.Request;

namespace Core.Executor {

    public class ServicesExecutor {

        private readonly AppSettings settings;
        private readonly ILogger log;
        private readonly RequestBase request;

        public ServicesExecutor(AppSettings settings, ILogger logger, RequestBase request) {
            this.settings = settings;
            this.log = logger;
            this.request = request;
        }

        /// <summary>
        /// Ejecuta por modulo, componentes de negocio previo que obtienen tareas a ejecutar y luego ejecuta tareas.
        /// </summary>
        public void ProcesarNotificacion() {
            SettingsManager.SetConfiguration(settings, log);
            ITransaction tx = null;

            try {
                RequestMantener requestmantener = ConvertObject.SerializeDeserialize<RequestMantener>(request);
                requestmantener.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(IsolationLevel.ReadCommitted);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception) {
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }
        }

        /// <summary>
        /// Ejecuta rollback de la transaccion. 
        /// </summary>
        private static void Rollback(ITransaction tx) {
            try {
                if (tx != null) {
                    tx.Rollback();
                }
            } catch (Exception) {
                //NO hacer nada si da excepcion.
            }
        }

    }

}
