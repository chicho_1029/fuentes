﻿using Core.Service;
using Core.Service.Validar;
using Core.Util.Excepcion;
using Dal.Base;
using NHibernate;
using System;
using System.Data;
using Util.General;
using Util.Request;

namespace Core.Executor {

    /// <summary>
    /// Clase que procesa los eventos solicitados.
    /// </summary>
    public class HubExecutor : Validador {

        #region MANTENER

        /// <summary>
        /// Ejecutor de transacciones de mantenimiento.
        /// </summary>
        public static string EjecutarMantener(string data, string ip) {
            RequestMantener request = new RequestMantener();
            Response response = new Response();
            ITransaction tx = null;

            try {
                request = ConvertObject.Deserialize<RequestMantener>(data);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(IsolationLevel.ReadCommitted);

                ValidarLogin(request);

                MantenerService mantenerService = new MantenerService();
                response = mantenerService.ProcesarBusiness(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), null);
        }

        #endregion

        /// <summary>
        /// Ejecuta rollback de la transaccion. 
        /// </summary>
        private static void Rollback(ITransaction tx) {
            try {
                if (tx != null) {
                    tx.Rollback();
                }
            } catch (Exception) {
                //NO hacer nada si da excepcion.
            }
        }

    }

}
