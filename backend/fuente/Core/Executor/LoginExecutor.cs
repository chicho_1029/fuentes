﻿using Core.Service;
using Core.Util.Excepcion;
using CoreSecurity;
using Dal.Base;
using NHibernate;
using System;
using System.Data;
using Util.General;
using Util.Request;
using Util.Seguridad;

namespace Core.Executor {

    /// <summary>
    /// Clase que procesa el login del sistema.
    /// </summary>
    public static class LoginExecutor {

        /// <summary>
        /// Método que ejecuta el login
        /// </summary>
        public static string EjecutarLogin(string data, string ip, string autenticate) {
            RequestLogin request = new RequestLogin();
            Response response = new Response();
            ITransaction tx = null;

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestLogin>(datadesencriptada);
                request.Token = Token.GetToken();
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();
                request.Cclase = "LOGIN";

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(IsolationLevel.ReadCommitted);

                LoginService loginService = new LoginService();
                response = loginService.Procesar(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        /// <summary>
        /// Método que cierra el sistema.
        /// </summary>
        public static string EjecutarLogout(string data, string ip, string autenticate) {
            RequestLogin request = new RequestLogin();
            Response response = new Response();
            ITransaction tx = null;

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestLogin>(datadesencriptada);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();
                request.Cclase = "LOGOUT";

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(IsolationLevel.ReadCommitted);

                LogoutService logoutService = new LogoutService();
                response = logoutService.Procesar(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        /// <summary>
        /// Método Rollback que permite deshacer cambios en base de datos cuando se produce una excepcion.
        /// </summary>
        private static void Rollback(ITransaction tx) {
            try {
                if (tx != null) {
                    tx.Rollback();
                }
            } catch (Exception) {
                //NO hacer nada si da excepcion.
            }
        }

    }

}
