﻿using Business.Seguridad.Menu;
using Core.Service;
using Core.Service.Validar;
using Core.Util.Excepcion;
using CoreSecurity;
using Dal.Base;
using NHibernate;
using System;
using System.Data;
using Util.General;
using Util.Request;

namespace Core.Executor {

    /// <summary>
    /// Clase que procesa los eventos solicitados.
    /// </summary>
    public class CoreExecutor : Validador {

        #region MANTENER

        /// <summary>
        /// Ejecutor de transacciones de mantenimiento.
        /// </summary>
        public static string EjecutarMantener(string data, string ip, string autenticate) {
            RequestMantener request = new RequestMantener();
            Response response = new Response();
            ITransaction tx = null;

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestMantener>(datadesencriptada);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(IsolationLevel.ReadCommitted);

                ValidarLogin(request);

                MantenerService mantenerService = new MantenerService();
                response = mantenerService.ProcesarBusiness(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        #endregion

        #region QUERY

        /// <summary>
        /// Ejecutor de transacciones de consulta.
        /// </summary>
        public static string EjecutarQuery(string data, string ip, string autenticate) {
            RequestQuery request = new RequestQuery();
            Response response = new Response();

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestQuery>(datadesencriptada);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);

                ValidarLogin(request);

                QueryService queryService = new QueryService();
                response = queryService.Procesar(request);
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        #endregion

        #region MENU

        /// <summary>
        /// Ejecutor de transaccion de menu.
        /// </summary>
        public static string EjecutarMenu(string data, string ip, string autenticate) {
            RequestMenu request = new RequestMenu();
            Response response = new Response();
            ITransaction tx = null;

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestMenu>(datadesencriptada);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();
                request.Cclase = "MENU";

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(IsolationLevel.ReadCommitted);

                if (!request.SesionIniciada) {
                    ValidarRequestMenu(request);
                }

                ProcesarMenu menuService = new ProcesarMenu();
                response = menuService.EjecutarMenu(request);
                CompletarRadicacionSesion(request, response);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        #endregion

        /// <summary>
        /// Ejecuta rollback de la transaccion. 
        /// </summary>
        private static void Rollback(ITransaction tx) {
            try {
                if (tx != null) {
                    tx.Rollback();
                }
            } catch (Exception) {
                //NO hacer nada si da excepcion.
            }
        }

    }

}
