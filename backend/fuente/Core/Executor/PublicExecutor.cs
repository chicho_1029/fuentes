﻿using Core.Service;
using Core.Util.Excepcion;
using CoreSecurity;
using Dal.Base;
using Microsoft.Extensions.Logging;
using NHibernate;
using System;
using System.Threading;
using Util.Configuracion;
using Util.General;
using Util.Request;

namespace Core.Executor {

    public class PublicExecutor {

        #region MANTENER

        /// <summary>
        /// Ejecutor de transacciones de mantenimiento.
        /// </summary>
        public static string EjecutarMantenerEncryp(string data, string ip, string autenticate) {
            RequestMantener request = new RequestMantener();
            Response response = new Response();
            ITransaction tx = null;

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestMantener>(datadesencriptada);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

                MantenerService mantenerService = new MantenerService();
                response = mantenerService.ProcesarBusiness(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        /// <summary>
        /// Ejecutor de transacciones de mantenimiento.
        /// </summary>
        public static string EjecutarMantener(string data, string ip) {
            RequestMantener request = new RequestMantener();
            Response response = new Response();
            ITransaction tx = null;

            try {
                request = ConvertObject.Deserialize<RequestMantener>(data);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

                MantenerService mantenerService = new MantenerService();
                response = mantenerService.ProcesarBusiness(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                }

                tx.Commit();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
            } finally {
                SessionHelper.CloseSession();
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), null);
        }

        #endregion

        #region EjecutarMantenerCanales
        /// <summary>
        /// Ejecutor de transacciones de mantenimiento.
        /// </summary>
        public static string EjecutarMantenerCanales(string data, string ip, AppSettings settings, ILogger logger) {
            Response response = new Response();
            RequestMantener request = new RequestMantener();
            try {
                request = ConvertObject.Deserialize<RequestMantener>(data);
                CoreExecutorHilo coreExecutor = new CoreExecutorHilo(request, ip, settings, logger);
                Thread thread = new Thread(coreExecutor.EjecutarCanales);
                thread.Start();
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                throw;
            }

            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), null);

        }
        #endregion

        #region QUERY

        /// <summary>
        /// Ejecutor de transacciones de consulta.
        /// </summary>
        public static string EjecutarQueryEncryp(string data, string ip, string autenticate) {
            RequestQuery request = new RequestQuery();
            Response response = new Response();

            try {
                string datadesencriptada = SecurityLib.OpenSSLDecrypt(data, autenticate);
                request = ConvertObject.Deserialize<RequestQuery>(datadesencriptada);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);

                QueryService queryService = new QueryService();
                response = queryService.Procesar(request);
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
            } finally {
                SessionHelper.CloseSession();
            }
            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), autenticate);
        }

        /// <summary>
        /// Ejecutor de transacciones de consulta.
        /// </summary>
        public static string EjecutarQuery(string data, string ip) {
            RequestQuery request = new RequestQuery();
            Response response = new Response();

            try {
                request = ConvertObject.Deserialize<RequestQuery>(data);
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);

                QueryService queryService = new QueryService();
                response = queryService.Procesar(request);
            } catch (Exception ex) {
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
            } finally {
                SessionHelper.CloseSession();
            }
            return ResponseGenerator.GetResponse(ConvertObject.Serialize(response), string.Empty);
        }

        #endregion

        /// <summary>
        /// Ejecuta rollback de la transaccion. 
        /// </summary>
        private static void Rollback(ITransaction tx) {
            try {
                if (tx != null) {
                    tx.Rollback();
                }
            } catch (Exception) {
                //NO hacer nada si da excepcion.
            }
        }

    }

}
