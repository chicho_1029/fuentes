﻿using CoreSecurity;
using Util.Request;

namespace Core.Executor {

    /// <summary>
    /// Clase que instancia la respuesta. La información retorna encriptada
    /// </summary>
    internal static class ResponseGenerator {

        public static string GetResponse(string data, string autenticate) {
            string dataencriptada = null;

            try {
                if (string.IsNullOrEmpty(autenticate)) {
                    dataencriptada = SecurityLib.OpenSSLEncrypt(data);
                } else {
                    dataencriptada = SecurityLib.OpenSSLEncrypt(data, autenticate);
                }
            } catch {
                return ConvertObject.Serialize(dataencriptada);
            }

            return ConvertObject.Serialize(dataencriptada);
        }

    }

}