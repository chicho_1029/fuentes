﻿using Core.Service;
using Core.Util.Excepcion;
using Core.Util.Interfaces;
using Dal.Base;
using Microsoft.Extensions.Logging;
using NHibernate;
using System;
using System.Runtime.Remoting;
using Util.Configuracion;
using Util.Excepcion;
using Util.General;
using Util.Request;

namespace Core.Executor {
    public class CoreExecutorHilo {
        private RequestMantener request;
        private string ip;
        private AppSettings settings;
        private readonly ILogger logger;
        private bool error = false;

        public CoreExecutorHilo(RequestMantener request, string ip, AppSettings settings, ILogger logger) {
            this.request = request;
            this.ip = ip;
            this.settings = settings;
            this.logger = logger;
        }

        #region EjecutarMantener

        /// <summary>
        /// Ejecutor de transacciones de mantenimiento.
        /// </summary>
        public void EjecutarCanales() {
            Response response = new Response();
            SettingsManager.SetConfiguration(settings, logger);
            ITransaction tx = null;
            LogHelper logHelper = new LogHelper();
            error = false;
            try {
                request.Terminal = ip;
                request.Freal = Fecha.GetFechaActual();

                SessionHelper.SetSession(request);
                ISession session = SessionHelper.GetSession();
                tx = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

                MantenerService mantenerService = new MantenerService();
                response = mantenerService.ProcesarBusiness(request);

                if (!tx.IsActive) {
                    SessionHelper.SetSession(request);
                    session = SessionHelper.GetSession();
                    tx = session.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                }

                tx.Commit();

            } catch (Exception ex) {
                error = true;
                response = ExcepcionControlador.GetMensajeResponse(request, ex);
                Rollback(tx);
                EjecutarIntentos(request, settings, logger, ex);
            } finally {
                if (!error) {
                    if (request.Datos.ContainsKey("notificacion")) {
                        //NotificacionHelper.EnviarNotificacion(request.GetDatosObject("notificacion"));
                    }
                }
                SessionHelper.CloseSession();
            }
        }

        #endregion


        #region EjecutarIntentos

        private static void EjecutarIntentos(RequestMantener request, AppSettings settings, ILogger logger, Exception exception) {
            string componente = "Core.Util.EjecutaIntentos";
            IIntentos c = null;
            try {
                string assembly = componente.Substring(0, componente.IndexOf("."));
                ObjectHandle handle = Activator.CreateInstance(assembly, componente);
                object comp = handle.Unwrap();
                c = (IIntentos)comp;
                c.Ejecutar(request, settings, logger, exception);
            } catch (TypeLoadException e) {
                throw new CoreExcepcion("1024", "CLASE A EJECUTAR NO EXISTE ", e);
            } catch (InvalidCastException e) {
                throw new CoreExcepcion("1025", "PROCESO A EJECUTAR NO IMPLEMENTADO", e);
            }
        }

        #endregion

        /// <summary>
        /// Ejecuta rollback de la transaccion. 
        /// </summary>
        private static void Rollback(ITransaction tx) {
            try {
                if (tx != null) {
                    tx.Rollback();
                }
            } catch (Exception) {
                //NO hacer nada si da excepcion.
            }

        }
    }
}