﻿using Dal.Core;
using Modelo.Entities;
using System;
using System.Runtime.Remoting;
using Util.Componentes;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Core.Service {

    /// <summary>
    /// Clase servicio que procesa los mantenimientos.
    /// </summary>
    public class MantenerService {

        public Response ProcesarBusiness(RequestMantener request) {

            if (string.IsNullOrEmpty(request.Cclase)) {
                throw new CoreExcepcion("1032", $"PROCESO NO AUTORIZADO: {request.Cclase}");
            }

            Coreclase clase = CoreClaseDal.GetClase(request.Cclase);
            if (clase == null) {
                throw new CoreExcepcion("1031", $"CLASE INCORRECTA: {request.Cclase}");
            }
            if (!clase.Activado) {
                throw new CoreExcepcion("1032", $"PROCESO NO ACTIVADO {clase.Cclase}");
            }

            if (clase.Tipo == EnumTipoClase.MANTENER_NEGOCIO) {
                ValidarTransaccion(request, string.Empty, request.Mantener);
            }

            string cnegocio = clase.Clasenegocio;
            string assembly = cnegocio.Substring(0, cnegocio.IndexOf("."));
            ObjectHandle handle = Activator.CreateInstance(assembly, cnegocio);
            ClaseBusinessMantener c = (ClaseBusinessMantener)handle.Unwrap();
            c.Ejecutar(request);

            return request.Response;
        }

        public static Coreclasetransaccion ValidarTransaccion(RequestMantener request, string centidad, Mantener mantener, bool clasetransaccion = true) {
            Coreclasetransaccion ctransaccion = null;
            if (clasetransaccion) {
                ctransaccion = CoreClaseTransaccionDal.GetClaseTransaccionMantener(request.Cmodulo, request.Ctransaccion, request.Cclase, centidad, mantener.Permiso);
                if (ctransaccion == null || !ctransaccion.Activado || ctransaccion.Permiso == null) {
                    throw new CoreExcepcion("1032", $"TRANSACCION NO AUTORIZADO: {request.Cclase} | {centidad} | {mantener.Permiso}");
                }
            }

            if ((!string.IsNullOrEmpty(mantener.Insercion) && mantener.Operacion != EnumOperacion.INSERT) ||
                (!string.IsNullOrEmpty(mantener.Actualizacion) && mantener.Operacion != EnumOperacion.UPDATE) ||
                (!string.IsNullOrEmpty(mantener.Eliminacion) && mantener.Operacion != EnumOperacion.DELETE)) {
                throw new CoreExcepcion("1032", $"OPERACION NO AUTORIZADO: {mantener.Operacion} | {mantener.Insercion} | {mantener.Actualizacion} | {mantener.Eliminacion}");
            }

            Coreusuariosesiontransaccion sesiontransaccion = CoreUsuarioSesionTransaccionDal.GetUsuarioTransaccion(request.Cusuario, request.Token, mantener.OperacionTransaccion);
            if (sesiontransaccion != null) {
                throw new CoreExcepcion("1032", $"SESSION NO AUTORIZADO: {request.Cusuario} | {request.Token} | {mantener.OperacionTransaccion}");
            }
            CoreUsuarioSesionTransaccionDal.CrearUsuarioTransaccion(request.Cusuario, request.Token, mantener.OperacionTransaccion);

            return ctransaccion;
        }

    }

}

