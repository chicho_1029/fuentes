﻿using Business.Seguridad.Sesion;
using Util.Request;

namespace Core.Service {

    /// <summary>
    /// Clase servicio que procesa el logout.
    /// </summary>
    public class LogoutService {

        public Response Procesar(RequestLogin request) {
            ProcesarLogout procesarLogout = new ProcesarLogout();
            return procesarLogout.Ejecutar(request);
        }

    }

}
