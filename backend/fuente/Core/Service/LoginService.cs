﻿using Business.Seguridad.Sesion;
using Business.Util.General;
using Util.Excepcion;
using Util.Request;

namespace Core.Service {

    /// <summary>
    /// Clase servicio que procesa el login.
    /// </summary>
    public class LoginService {

        public Response Procesar(RequestLogin request) {
            ValidarRequest(request);

            ProcesarLogin procesarLogin = new ProcesarLogin();
            return procesarLogin.Ejecutar(request);
        }

        private static void ValidarRequest(RequestLogin request) {
            if (string.IsNullOrEmpty(request.Cusuario)) {
                throw new CoreExcepcion("1032", $"USUARIO NO AUTORIZADO: {request.Cusuario}");
            }

            if (string.IsNullOrEmpty(request.Password)) {
                throw new CoreExcepcion("1032", $"CLAVE NO AUTORIZADO: {request.Password}");
            }

            if (string.IsNullOrEmpty(request.Ccanal)) {
                throw new CoreExcepcion("1032", $"CANAL NO AUTORIZADO: {request.Ccanal}");
            } else {
                if (!string.Equals(request.Ccanal, Constante.CANAL_OFICINA)) {
                    throw new CoreExcepcion("1032", $"CANAL NO CORRESPONDE: {request.Ccanal}");
                }
            }
        }

    }

}
