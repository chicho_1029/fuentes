﻿using Core.Util.Interfaces;
using Core.Util.Transaccion;
using Dal.Core;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Runtime.Remoting;
using Util.Excepcion;
using Util.General;
using Util.Request;


namespace Core.Service.Validar {

    /// <summary>
    /// Clase general de validacion de información.
    /// </summary>
    public class Validador {

        public static void ValidarRequestMenu(RequestBase request) {
            Radicacion radicacion = GenerarRadicacionMenu(request.Cusuario, request.Token);

            if (!string.Equals(request.Ccanal, radicacion.Ccanal)) {
                throw new CoreExcepcion("1032", $"CANAL NO CORRESPONDE: {request.Ccanal}");
            }
            if (!string.Equals(request.Terminal, radicacion.Terminal)) {
                throw new CoreExcepcion("1032", $"TERMINAL NO CORRESPONDE: {request.Terminal}");
            }
            if (!string.Equals(request.Terminallocal, radicacion.TerminalLocal)) {
                throw new CoreExcepcion("1032", $"LOCAL NO CORRESPONDE: {request.Terminallocal}");
            }
            if (!string.Equals(request.Navegador, radicacion.Navegador)) {
                throw new CoreExcepcion("1032", $"NAVEGADOR NO CORRESPONDE: {request.Navegador}");
            }

            ValidarRoles(request.Cmodulologin, radicacion.Lmodulos);
        }

        private static Radicacion GenerarRadicacionMenu(string cusuario, string token) {
            Coreusuariosesionhistoria historia = CoreUsuarioSesionHistoriaDal.GetUsuarioSesionHistoriaIngreso(cusuario, token);
            if (historia == null) {
                throw new CoreExcepcion("1021", $"SESION HISTORIA NO EXISTE: {cusuario} | {token}");
            }

            Coreusuario usuario = CoreUsuarioDal.GetUsuario(cusuario);
            if (usuario == null) {
                throw new CoreExcepcion("1021", $"USUARIO NO EXISTE: {cusuario}");
            }

            Radicacion radicacion = ConvertObject.SerializeDeserialize<Radicacion>(historia);
            radicacion.Ccanal = usuario.Ccanal;
            radicacion.Lmodulos = ConvertObject.Deserialize<List<RadicacionModulo>>(historia.Radicacion);

            return radicacion;
        }

        private static void ValidarRoles(short cmodulologin, List<RadicacionModulo> Lmodulos) {
            if (!Lmodulos.Exists(x => x.Cmodulologin == cmodulologin)) {
                throw new CoreExcepcion("1032", $"MÓDULO NO AUTORIZADO: {cmodulologin}");
            }
        }

        public static void CompletarRadicacionSesion(RequestMenu request, Response response) {
            if (request.SesionIniciada) {
                ValidarLogin(request, false);
            } else {
                RadicacionSesion radicacion = GenerarRadicacionSesion(request.Cusuario, request.Ccanal);
                CoreUsuarioSesionDal.CrearSession(request, radicacion);

                response.Add("radicacion", radicacion);
            }
        }

        private static RadicacionSesion GenerarRadicacionSesion(string cusuario, string ccanal) {
            Coreusuario usuario = CoreUsuarioDal.GetUsuario(cusuario);
            Corecanal canal = CoreCanalDal.GetCanal(ccanal);

            RadicacionSesion radicacion = new RadicacionSesion();
            //radicacion.CorreoUsuario = usuario.Correo;
            radicacion.FechaUltimoIngreso = usuario.Fultimoingreso;
            //radicacion.IdentificacionUsuario = usuario.Identificacion;
            //radicacion.NombreOficina = usuario.Nombreoficina;
            //radicacion.NombreUsuario = usuario.Nombre;
            radicacion.FechaSistema = Fecha.GetFechaActualEntero();
            radicacion.TiempoSesion = Convert.ToInt16(canal.Tiemposesion);
            return radicacion;
        }

        public static void ValidarLogin(RequestBase request, bool validarTransaccion = true) {
            CompletarRequest(request);

            if (validarTransaccion) {
                ValidarTransaccion.Validar(request.Cmodulologin, request.Cmodulo, request.Ctransaccion, request.Cusuario, request.Token);
            }

            ValidarUsuario(request.Cusuario, request.Ccanal, request.Token);
        }

        private static void CompletarRequest(RequestBase request) {
            Radicacion radicacion = GenerarRadicacion(request.Cusuario, request.Token);
            ValidarRequest(request, radicacion);

            request.Fsistema = radicacion.Fsistema;
            request.Navegador = radicacion.Navegador;
            request.Terminal = radicacion.Terminal;
            request.Terminallocal = radicacion.TerminalLocal;
        }

        private static Radicacion GenerarRadicacion(string cusuario, string token) {
            Coreusuariosesionhistoria historia = CoreUsuarioSesionHistoriaDal.GetUsuarioSesionHistoriaIngreso(cusuario, token);
            if (historia == null) {
                throw new CoreExcepcion("1021", $"SESION HISTORIA NO EXISTE: {cusuario} | {token}");
            }

            Coreusuariosesion sesion = CoreUsuarioSesionDal.GetSesionPorUsuarioToken(cusuario, token);
            if (sesion == null) {
                throw new CoreExcepcion("1021", $"SESION NO EXISTE: {cusuario} | {token}");
            }

            Coreusuario usuario = CoreUsuarioDal.GetUsuario(cusuario);
            if (usuario == null) {
                throw new CoreExcepcion("1021", $"USUARIO NO EXISTE: {cusuario}");
            }

            Radicacion radicacion = ConvertObject.SerializeDeserialize<Radicacion>(sesion);
            radicacion.Ccanal = usuario.Ccanal;
            radicacion.Lmodulos = ConvertObject.Deserialize<List<RadicacionModulo>>(historia.Radicacion);

            return radicacion;
        }

        private static void ValidarRequest(RequestBase request, Radicacion radicacion) {

            if (request.Cmodulologin != radicacion.Cmodulologin) {
                throw new CoreExcepcion("1032", $"ROL NO CORRESPONDE: {request.Cmodulologin}");
            }
            if (!string.Equals(request.Ccanal, radicacion.Ccanal)) {
                throw new CoreExcepcion("1032", $"CANAL NO CORRESPONDE: {request.Ccanal}");
            }
            //if (!string.Equals(request.Terminal, radicacion.Terminal)) {
            //    throw new CoreExcepcion("1032", $"TERMINAL NO CORRESPONDE: {request.Terminal}");
            //}
            if (!string.Equals(request.Terminallocal, radicacion.TerminalLocal)) {
                throw new CoreExcepcion("1032", $"LOCAL NO CORRESPONDE: {request.Terminallocal}");
            }
            if (!string.Equals(request.Navegador, radicacion.Navegador)) {
                throw new CoreExcepcion("1032", $"NAVEGADOR NO CORRESPONDE: {request.Navegador}");
            }

            ValidarRoles(request.Cmodulologin, radicacion.Lmodulos);
        }

        private static void ValidarUsuario(string cusuario, string ccanal, string token) {
            string componente = "Core.Util.Usuario";
            ILogin c = null;
            try {
                string assembly = componente.Substring(0, componente.IndexOf("."));
                ObjectHandle handle = Activator.CreateInstance(assembly, componente);
                object comp = handle.Unwrap();
                c = (ILogin)comp;
                c.Validarlogin(cusuario, ccanal, token);
            } catch (TypeLoadException e) {
                throw new CoreExcepcion("1024", $"CLASE A EJECUTAR NO EXISTE: {componente}", e);
            } catch (InvalidCastException e) {
                throw new CoreExcepcion("1025", $"PROCESO A EJECUTAR NO IMPLEMENTADO: {componente}", e);
            }
        }

    }

}
