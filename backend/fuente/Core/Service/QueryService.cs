﻿using Dal.Core;
using Modelo.Entities;
using System;
using System.Runtime.Remoting;
using Util.Componentes;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Core.Service {

    /// <summary>
    /// Clase servicio que procesa las consultas.
    /// </summary>
    public class QueryService {

        public Response Procesar(RequestQuery request) {

            if (request.Cclase == null) {
                throw new CoreExcepcion("1032", $"PROCESO NO AUTORIZADO: {request.Cclase}");
            }

            Coreclase clase = CoreClaseDal.GetClase(request.Cclase);
            if (clase == null) {
                throw new CoreExcepcion("1031", $"CLASE INCORRECTA: {request.Cclase}");
            }
            if (!clase.Activado) {
                throw new CoreExcepcion("1032", $"PROCESO NO ACTIVADO {clase.Cclase}");
            }

            if (clase.Tipo == EnumTipoClase.CONSULTA_NEGOCIO) {
                ValidarTransaccion(request, string.Empty, null);
            }

            string cnegocio = clase.Clasenegocio;
            string assembly = cnegocio.Substring(0, cnegocio.IndexOf("."));
            ObjectHandle handle = Activator.CreateInstance(assembly, cnegocio);
            ClaseBusinessQuery c = (ClaseBusinessQuery)handle.Unwrap();
            c.Ejecutar(request);

            return request.Response;
        }

        public static bool ValidarTransaccion(RequestQuery request, string centidad, string metodo) {
            Coreclasetransaccion transaccion = CoreClaseTransaccionDal.GetClaseTransaccionConsulta(request.Cmodulo, request.Ctransaccion, request.Cclase, centidad, metodo);
            if (transaccion == null || !transaccion.Activado) {
                throw new CoreExcepcion("1032", $"TRANSACCION NO AUTORIZADO: {request.Cclase} | {centidad} | {metodo}");
            }
            return transaccion.Parametro;
        }

    }

}

