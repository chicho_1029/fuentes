﻿using Core.Service;
using CoreSecurity;
using Dal.Base;
using Dal.Core;
using Modelo.Entities;
using Newtonsoft.Json;
using System;
using System.Reflection;
using Util.Componentes;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Core.General {

    /// <summary>
    /// Clase que procesa el mantenimiento de cruds.
    /// </summary>
    internal class ProcesarMantenimiento : ClaseBusinessMantener {

        public override Response Ejecutar(RequestMantener request) {
            Mantener mantener = request.Mantener;
            if (string.IsNullOrEmpty(mantener.Operacion)) {
                throw new CoreExcepcion("1032", $"OPERACION NO AUTORIZADO: {mantener.Operacion}");
            }

            Coreentidad entidad = CoreEntidadDal.GetEntidad(mantener.CodigoEntidad);
            if (entidad == null) {
                throw new CoreExcepcion("1032", $"ENTIDAD NO AUTORIZADO: {mantener.CodigoEntidad}");
            }

            Coreclasetransaccion clasetransaccion = MantenerService.ValidarTransaccion(request, entidad.Centidad, mantener);

            string assembly = "Dal";
            string clase = string.Join(string.Empty, assembly, ".", !string.IsNullOrEmpty(entidad.Esquema) ? entidad.Esquema + "." : "Core.", entidad.Nombre, assembly, ",", assembly);
            string nentidad = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(entidad.Nombre.ToLower());

            Type t = Type.GetType(clase);
            Type e = Type.GetType("Modelo.Entities." + (!string.IsNullOrEmpty(entidad.Esquema) ? entidad.Esquema + "." : string.Empty) + nentidad + ", Modelo");
            var registro = JsonConvert.DeserializeObject(Convert.ToString(mantener.Registro), e);

            SetCamposPassword(mantener.Operacion, registro);

            switch (mantener.Operacion) {
                case EnumOperacion.INSERT:
                    SessionCore.Save(registro);
                    break;
                case EnumOperacion.UPDATE:
                    MethodInfo m = t.GetMethod(clasetransaccion.Metodo);
                    object registromodificado = m.Invoke(t, new object[] { registro });
                    SessionCore.Update(registromodificado);
                    break;
                case EnumOperacion.DELETE:
                    SessionCore.Delete(registro);
                    break;
            }

            return request.Response;
        }

        private void SetCamposPassword(string operacion, object obj) {
            string PASSWORD = EnumCampos.PASSWORD;

            // Verifica si entidad tiene campo password
            if (operacion.Equals(EnumOperacion.DELETE)) return;
            var p = obj.GetType().GetProperty(PASSWORD);
            if (p == null) return;

            obj.GetType().GetProperty(PASSWORD).SetValue(obj, SecurityLib.OpenSSLEncrypt(Convert.ToString(obj.GetType().GetProperty(PASSWORD).GetValue(obj))));
        }
    }

}
