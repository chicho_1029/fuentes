﻿using Core.Service;
using Core.Util.Transaccion;
using Dal.Base;
using Dal.Core;
using Modelo.Entities;
using System;
using Util.Componentes;
using Util.Enumeracion;
using Util.Excepcion;
using Util.Request;

namespace Core.General {

    /// <summary>
    /// Clase que procesa el mantenimiento de favoritos.
    /// </summary>
    internal class ProcesarFavorito : ClaseBusinessMantener {

        public override Response Ejecutar(RequestMantener request) {
            Mantener mantener = request.Mantener;
            if (string.IsNullOrEmpty(mantener.Operacion)) {
                throw new CoreExcepcion("1032", $"OPERACION NO AUTORIZADO: {mantener.Operacion}");
            }

            Coreentidad entidad = CoreEntidadDal.GetEntidad(mantener.CodigoEntidad);
            if (entidad == null) {
                throw new CoreExcepcion("1032", $"ENTIDAD NO AUTORIZADO: {mantener.CodigoEntidad}");
            }

            MantenerService.ValidarTransaccion(request, entidad.Centidad, mantener, false);

            Coremenufavorito registro = ConvertObject.Deserialize<Coremenufavorito>(Convert.ToString(mantener.Registro));
            ValidarTransaccion.Validar(2, registro.Cmodulo, registro.Ctransaccion, request.Cusuario, request.Token);

            switch (mantener.Operacion) {
                case EnumOperacion.INSERT:
                    SessionCore.Save(registro);
                    break;
                case EnumOperacion.DELETE:
                    SessionCore.Delete(registro);
                    break;
                default:
                    throw new CoreExcepcion("1032", $"OPERACION MANTENER NO AUTORIZADO: {mantener.Operacion}");
            }

            return request.Response;
        }

    }

}
