﻿using Business.Util.General;
using Core.Service;
using Dal.Core;
using Modelo.Entities;
using NHibernate;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using Util.Componentes;
using Util.Excepcion;
using Util.Request;

namespace Core.General {

    /// <summary>
    /// Clase que procesa la consulta de entidades especificas.
    /// </summary>
    internal class ProcesarConsultaSeguridad : ClaseBusinessQuery {

        public override Response Ejecutar(RequestQuery request) {

            if (request.Queries != null) {
                foreach (Query q in request.Queries) {
                    Coreentidad entidad = CoreEntidadDal.GetEntidad(q.CodigoEntidad);
                    if (entidad == null || !entidad.Permiteseguridad) {
                        throw new CoreExcepcion("1032", $"ENTIDAD NO AUTORIZADO: {q.CodigoEntidad}");
                    }
                    if (string.IsNullOrEmpty(q.Metodo)) {
                        throw new CoreExcepcion("1032", $"METODO NO AUTORIZADO: {q.Metodo}");
                    }

                    QueryService.ValidarTransaccion(request, entidad.Centidad, q.Metodo);

                    string assembly = "Dal";
                    string clase = string.Join(string.Empty, assembly, ".", !string.IsNullOrEmpty(entidad.Esquema) ? entidad.Esquema + "." : "Core.", entidad.Nombre, assembly, ",", assembly);

                    Type t = Type.GetType(clase);
                    MethodInfo m = t.GetMethod(q.Metodo);
                    IQuery i = (IQuery)m.Invoke(t, new object[] { });

                    foreach (string key in q.Parametros.Keys) {
                        string value = Regex.Replace(q.Parametros[key].ToString(), Constante.CARACTERES_RESERVADOS, string.Empty);
                        i.SetParameter(key, value);
                    }

                    request.AddResponse(q.Objeto, i.SetResultTransformer(NHibernate.Transform.Transformers.AliasToEntityMap).List<object>());
                }
            }

            return request.Response;
        }

    }

}
