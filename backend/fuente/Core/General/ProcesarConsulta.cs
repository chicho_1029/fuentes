﻿using Business.Util.General;
using Core.Service;
using Dal.Core;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using Util.Componentes;
using Util.Excepcion;
using Util.Request;

namespace Core.General {

    /// <summary>
    /// Clase que procesa la consulta general de entidades.
    /// </summary>
    internal class ProcesarConsulta : ClaseBusinessQuery {

        public override Response Ejecutar(RequestQuery request) {

            if (request.Queries != null) {
                foreach (Query q in request.Queries) {
                    Coreentidad entidad = CoreEntidadDal.GetEntidad(q.CodigoEntidad);
                    if (entidad == null || !entidad.Permiteconsulta) {
                        throw new CoreExcepcion("1032", $"ENTIDAD NO AUTORIZADO: {q.CodigoEntidad}");
                    }

                    bool parametro = QueryService.ValidarTransaccion(request, entidad.Centidad, q.Metodo);

                    string assembly = "Dal";
                    string clase = string.Join(string.Empty, assembly, ".", !string.IsNullOrEmpty(entidad.Esquema) ? entidad.Esquema + "." : "Core.", entidad.Nombre, assembly, ",", assembly);

                    Type t = Type.GetType(clase);
                    MethodInfo m = t.GetMethod(q.Metodo);
                    if (m == null)
                        throw new CoreExcepcion("1002", $"METODO NO EXISTE: {q.Metodo}");

                    List<object> parametros = new List<object>();
                    int numparams = 0;
                    foreach (ParameterInfo p in m.GetParameters()) {
                        string v = Regex.Replace(Convert.ToString(q.Parametros[p.Name]), Constante.CARACTERES_RESERVADOS, string.Empty);
                        parametros.Add(Convert.ChangeType(v, p.ParameterType));
                        numparams += 1;
                    }

                    if (m.ReturnType.FullName.Contains("List") || m.ReturnType.FullName.Contains("Enumerable")) {
                        var result = parametro ? (IEnumerable<object>)m.Invoke(t, parametros.ToArray()) : (IEnumerable<object>)m.Invoke(t, null);
                        request.AddResponse(q.Objeto, result);
                    } else {
                        var result = parametro ? m.Invoke(t, parametros.ToArray()) : m.Invoke(t, null);
                        request.AddResponse(q.Objeto, result);
                    }
                }
            }

            return request.Response;
        }

    }

}
