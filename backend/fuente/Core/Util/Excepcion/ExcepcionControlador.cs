﻿using Dal.Base;
using Dal.Core;
using Microsoft.Extensions.Logging;
using Modelo.Entities;
using System;
using Util.Configuracion;
using Util.Excepcion;
using Util.Request;

namespace Core.Util.Excepcion {
    /// <summary>
    /// Clase que controla las excepciones de la aplicación. Se consideran las excepciones de la BDD.
    /// </summary>
    public static class ExcepcionControlador {

        private static readonly ILogger log = SettingsManager.GetLogger();
        private static readonly LogHelper logHelper = new LogHelper();
        private static readonly string msgDefault = "Información no disponible";

        public static Response GetMensajeResponse(RequestBase request, Exception exception) {
            Response resp = EjecutarMensajeResponse(exception, request.Cusuario, request.Token);
            logHelper.GrabarLog(string.Join(".", exception.TargetSite.DeclaringType.FullName, exception.TargetSite.Name), resp, exception, true);
            return resp;
        }

        public static Response EjecutarMensajeResponse(Exception exception, string cusuario, string token) {
            Response response = new Response();
            CoreExcepcion e = exception as CoreExcepcion;

            if (e == null)
                log.LogError($"TOKEN: {token}, USUARIO: {cusuario}, EXCEPCION: {exception}");

            try {
                if (e != null) {
                    GetMensajeCore(e, response);
                } else {
                    response.SetCod("-1");
                    response.SetMsgusu(msgDefault);
                }
            } catch (Exception) {
                response.SetCod("-1");
                response.SetMsgusu(msgDefault);
            }
            return response;
        }

        private static void GetMensajeCore(CoreExcepcion e, Response response) {
            response.SetCod(e.Codigo);
            string msg;

            try {
                Coremensaje r = new Coremensaje();
                r = CoreMensajeDal.GetMensaje(e.Codigo);
                msg = r.Mensaje;
            } catch (Exception) {
                response.SetMsgusu(msgDefault);
                return;
            }
            try {
                if (e.Parametros != null) {
                    msg = string.Format(msg, e.Parametros);
                }
            } catch (Exception) {
                // no tomar accion
            }
            response.SetMsgusu(msg);
        }

    }

}
