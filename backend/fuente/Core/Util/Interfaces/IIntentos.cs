﻿using Microsoft.Extensions.Logging;
using System;
using Util.Configuracion;
using Util.Request;

namespace Core.Util.Interfaces {

    public interface IIntentos {
        void Ejecutar(RequestMantener request, AppSettings settings, ILogger logger, Exception exception);
    }

}
