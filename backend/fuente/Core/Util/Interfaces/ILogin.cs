﻿
namespace Core.Util.Interfaces {

    public interface ILogin {
        /// <summary>
        /// Metodo que valida que un usuario tenga una session activa
        /// </summary>
        void Validarlogin(string cusuario, string ccanal, string token);
    }
}
