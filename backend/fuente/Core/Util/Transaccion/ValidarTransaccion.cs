﻿using Dal.Core;
using System;
using System.Collections.Generic;
using Util.Excepcion;

namespace Core.Util.Transaccion {

    /// <summary>
    /// Clase utilitaria que permite validar las transacciones permitidas por rol.
    /// </summary>
    public class ValidarTransaccion {

        public static void Validar(short cmodulologin, short cmodulo, int ctransaccion, string cusuario, string token) {

            List<short> lroles = CoreRolDal.GetRolesLogeado(cmodulologin, cusuario, token);

            IList<object> datos = CoreTransaccionDal.GetRutas(lroles);
            bool val = false;
            foreach (object[] obj in datos) {
                if (obj[0] == null || obj[1] == null) {
                    continue;
                }
                short modulo = Convert.ToInt16(obj[0]);
                int transaccion = Convert.ToInt32(obj[1]);

                if (cmodulo.Equals(modulo) && ctransaccion.Equals(transaccion)) {
                    val = true;
                    break;
                }
            }

            if (!val) {
                throw new CoreExcepcion("1028", $"TRANSACCIÓN NO PERMITIDA: {cmodulologin} | {cmodulo} | {ctransaccion}");
            }
        }
    }
}
