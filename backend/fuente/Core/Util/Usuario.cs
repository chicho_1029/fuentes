﻿using Business.Util.General;
using Core.Util.Interfaces;
using Dal.Base;
using Dal.Core;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using Util.Excepcion;
using Util.General;

namespace Core.Util {

    /// <summary>
    /// Clase utilitaria que permite crear y validar usuarios. Genera el registro de sesiones por usuario.
    /// </summary>
    class Usuario : ILogin {

        public void Validarlogin(string cusuario, string ccanal, string token) {
            Valida(cusuario, ccanal, token);
            Procesar(cusuario, token);
        }

        private void Procesar(string cusuario, string token) {
            EliminarUsuarios(cusuario);
            DateTime fultimaaccion;

            Coreusuariosesion sesionactiva = CoreUsuarioSesionDal.GetSesionPorUsuario(cusuario);
            Coreusuariosesion sesion = CoreUsuarioSesionDal.GetSesionPorUsuarioToken(cusuario, token);

            if (sesion == null) {
                if (sesionactiva == null) {
                    throw new CoreExcepcion("1029", " EL USUARIO {0} NO TIENE UNA SESIÓN ACTIVA", cusuario);
                } else {
                    throw new CoreExcepcion("1014", "EL USUARIO {0} YA TIENE UNA SESIÓN ACTIVA", sesionactiva.Cusuario);
                }
            }
            short tiemposession = sesion.Tiemposesion ?? 0;
            fultimaaccion = sesion.Fultimaaccion ?? Fecha.GetFechaActual();

            sesion.Tiemposesion = tiemposession;
            sesion.Fultimaaccion = Fecha.GetFechaActual();

            ThreadUpdate.Update(sesion);

            fultimaaccion = fultimaaccion.AddMinutes(Convert.ToDouble(tiemposession.ToString()));
            if (fultimaaccion.CompareTo(Fecha.GetFechaActual()) < 0) {
                throw new CoreExcepcion("1021", "SESIÓN CADUCADA");
            }
        }

        private void EliminarUsuarios(string cusuario) {
            DateTime fultimaaccion;
            IList<Coreusuariosesion> lsesiones = CoreUsuarioSesionDal.GetSesiones();

            foreach (Coreusuariosesion obj in lsesiones) {
                if (obj == null) return;

                if (obj.Cusuario.Equals(cusuario)) {
                    continue;
                }

                fultimaaccion = obj.Fultimaaccion ?? Fecha.GetFechaActual();
                fultimaaccion = fultimaaccion.AddMinutes(Convert.ToDouble(obj.Tiemposesion.ToString()) + 30);
                if (fultimaaccion.CompareTo(Fecha.GetFechaActual()) < 0) {
                    ThreadDelete.Delete(obj);
                }
            }

            CoreFortiDal.EliminarForti();
        }

        private void Valida(string cusuario, string ccanal, string token) {
            if (string.IsNullOrEmpty(cusuario)) {
                throw new CoreExcepcion("1032", $"USUARIO NO CORRESPONDE: {cusuario}");
            }

            if (string.IsNullOrEmpty(token)) {
                throw new CoreExcepcion("1032", $"TOKEN NO CORRESPONDE: {token}");
            }

            if (string.IsNullOrEmpty(ccanal)) {
                throw new CoreExcepcion("1032", $"CANAL NO CORRESPONDE: {ccanal}");
            } else {
                if (!ccanal.Equals(Constante.CANAL_OFICINA)) {
                    throw new CoreExcepcion("1032", $"CANAL NO CORRESPONDE: {ccanal}");
                }
            }
        }
    }
}
