﻿namespace WebPUBLIC.Models {

    public class FileModel {

        public virtual string Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Extension { get; set; }
        public virtual byte[] Archivo { get; set; }

    }

}
