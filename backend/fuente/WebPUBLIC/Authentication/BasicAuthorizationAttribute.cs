﻿using Microsoft.AspNetCore.Authorization;
namespace WebPUBLIC.Authentication {
    public class BasicAuthorizationAttribute : AuthorizeAttribute {
        public BasicAuthorizationAttribute() {
            Policy = "BasicAuthentication";
        }
    }
}