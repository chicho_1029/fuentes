﻿using Core.Util.Excepcion;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Util.Configuracion;
using Util.Request;
using Util.Seguridad;
using WebPUBLIC.Models;

namespace WebPUBLIC.Functions {

    internal class PublicFunction {

        private readonly string urlApi;
        private readonly ILogger log;
        private readonly AuthorizationBasic authorizationbasic;

        public PublicFunction(ILogger log, string urlApi, AuthorizationBasic authorizationbasic) {
            this.log = log;
            this.urlApi = urlApi;
            this.authorizationbasic = authorizationbasic;
        }

        internal string EjecutarServicio(HttpContext context, HttpRequest request, string datos, string metodo, bool post = true, bool captcha = false, string keycaptcha = "") {
            string response = null;
            string URL = string.Join("/", urlApi, "Api", metodo);

            try {
                string datastr = ConvertObject.Serialize(datos);
                var clientIP = ApiHelper.GetIp(context, request);
                string authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(string.Join(":", authorizationbasic.UsrWebAPI, authorizationbasic.PwdWebAPI)));

                HttpWebRequest httpRequest = WebRequest.Create(URL) as HttpWebRequest;

                httpRequest.Headers.Add("clientIP", clientIP);
                httpRequest.Headers["Authorization"] = "Basic " + authInfo;
                httpRequest.Method = "GET";
                httpRequest.Headers["Autenticate"] = ApiHelper.GetAutenticate(request);

                if (post) {
                    byte[] bufferData = Encoding.UTF8.GetBytes(datastr);

                    httpRequest.Method = "POST";
                    httpRequest.KeepAlive = false;
                    httpRequest.UseDefaultCredentials = true;
                    httpRequest.ContentType = "text/json";
                    httpRequest.ContentLength = bufferData.Length;
                    httpRequest.Timeout = 300000;

                    using (Stream streamRequest = httpRequest.GetRequestStream()) {
                        streamRequest.Write(bufferData, 0, bufferData.Length);
                        streamRequest.Flush();
                        streamRequest.Close();
                    }
                }

                if (captcha) {
                    Captcha.ValidateCaptcha(request.Headers["Content-Captcha"].ToString(), keycaptcha);
                }

                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                using (Stream streamResponse = httpResponse.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(streamResponse)) {
                    response = streamReader.ReadToEnd();

                    streamReader.Close();
                    streamResponse.Close();
                    httpResponse.Close();
                }
            } catch (Exception ex) {
                log.LogError($"URL - {URL} - EXCEPTION: {ex}");

                throw new Exception(ResponseHelper.GetResponseError(request));
            }
            return response;
        }

        internal string CrearArchivo(string url, string path, string data) {
            try {
                FileModel file = ConvertObject.Deserialize<FileModel>(data);

                bool existe = true;
                string carpeta = Path.Combine(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString());
                string nombre = file.Id + Regex.Replace(string.Join(string.Empty, Guid.NewGuid().ToString()), @"[^0-9]+", string.Empty) + "." + file.Extension;
                string ubicacion = Path.Combine(path, carpeta);

                Directory.CreateDirectory(ubicacion);

                string ubicacionArchivo = string.Empty;
                while (existe) {
                    ubicacionArchivo = Path.Combine(ubicacion, nombre);
                    if (!File.Exists(ubicacionArchivo)) {
                        existe = false;
                    }
                }

                File.WriteAllBytes(ubicacionArchivo, file.Archivo);

                return new Uri(url + Path.Combine(carpeta, nombre)).AbsoluteUri; ;
            } catch {
                return string.Empty;
            }
        }

    }

}
