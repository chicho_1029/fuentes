﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using Util.Configuracion;
using Util.Request;
using WebPUBLIC.Authentication;
using WebPUBLIC.Functions;

namespace WebPUBLIC.Controllers {

    [Route("ApiPublic")]
    [ApiController]
    public class PublicController : ControllerBase {

        private readonly ILogger log;
        private readonly PublicFunction function;
        private readonly Public config;

        public PublicController(IOptions<AppSettings> settings, ILogger<PublicController> logger) {
            function = new PublicFunction(logger, settings.Value.UrlApi, settings.Value.AuthorizationBasic);
            log = logger;
            config = settings.Value.Public;
        }

        [Route("facebookcheck")]
        [HttpGet]
        public string Webhook(
                        [FromQuery(Name = "hub.mode")] string mode,
                        [FromQuery(Name = "hub.challenge")] string challenge,
                        [FromQuery(Name = "hub.verify_token")] string verify_token) {
            try {
                //ModelFacebook modelFace = new ModelFacebook();
                //modelFace.mode = mode;
                //modelFace.challenge = challenge;
                //modelFace.verify_token = verify_token;

                //string dataRequest = ConvertObject.Serialize(modelFace);
                //return function.EjecutarServicio(HttpContext, Request, dataRequest, "FacebookInternal");
                return "";
            } catch {
                return "";
            }
        }

        [Route("facebookcheck")]
        [HttpPost]
        public async Task<IActionResult> RecibeMensajesFacebook() {
            DateTime date = DateTime.Now;
            string dataRequest = string.Empty;
            string dataResponse = string.Empty;

            try {
                dataRequest = await RequestHelper.GetRequestAsync(Request);
                string response = function.EjecutarServicio(HttpContext, Request, dataRequest, "FacebookCheck");
                dataResponse = ResponseHelper.GetResponseService(response);

                return Ok(dataResponse);
            } catch (Exception ex) {
                dataResponse = ex.Message;
                return BadRequest(ResponseHelper.GetResponseError(Request));
            } finally {
                log.LogInformation($"FACEBOOK - {date} | {DateTime.Now} - REQUEST: {dataRequest}, RESPONSE: {dataResponse}");
            }
        }

        [Route("instagramcheck")]
        [HttpGet]
        public string Instagramcheck(
                              [FromQuery(Name = "hub.mode")] string mode,
                              [FromQuery(Name = "hub.challenge")] string challenge,
                              [FromQuery(Name = "hub.verify_token")] string verify_token) {
            try {
                //ModelInstagram modelInstagram = new ModelInstagram();
                //modelInstagram.mode = mode;
                //modelInstagram.challenge = challenge;
                //modelInstagram.verify_token = verify_token;

                //string dataRequest = ConvertObject.Serialize(modelInstagram);
                //return function.EjecutarServicio(HttpContext, Request, dataRequest, "InstagramInternal");
                return "";
            } catch {
                return "";
            }
        }

        [Route("instagramcheck")]
        [HttpPost]
        public async Task<IActionResult> Instagramcheck() {
            DateTime date = DateTime.Now;
            string dataRequest = string.Empty;
            string dataResponse = string.Empty;

            try {
                dataRequest = await RequestHelper.GetRequestAsync(Request);
                string response = function.EjecutarServicio(HttpContext, Request, dataRequest, "InstagramCheck");
                dataResponse = ResponseHelper.GetResponseService(response);

                return Ok(dataResponse);
            } catch (Exception ex) {
                dataResponse = ex.Message;
                return BadRequest(ResponseHelper.GetResponseError(Request));
            } finally {
                log.LogInformation($"INSTAGRAM - {date} | {DateTime.Now} - REQUEST: {dataRequest}, RESPONSE: {dataResponse}");
            }
        }

        [Route("whatsapp")]
        [HttpPost]
        public async Task<IActionResult> WebkookWhatsapp() {
            DateTime date = DateTime.Now;
            string dataRequest = string.Empty;
            string dataResponse = string.Empty;
            string response = string.Empty;

            try {
                dataRequest = await RequestHelper.GetRequestAsync(Request);
                response = function.EjecutarServicio(HttpContext, Request, dataRequest, "WhatsappCheck");
                dataResponse = ResponseHelper.GetResponseService(response);

                return Ok(new { Ok = "Mensaje enviado" });
            } catch (Exception ex) {
                dataResponse = ex.Message;
                return BadRequest(new { Error = "Error al recibir mensaje" });
            } finally {
                log.LogInformation($"WHATSAPP - {date} | {DateTime.Now} - REQUEST: {dataRequest}, SERVICE: {response}, RESPONSE: {dataResponse}");
            }
        }

        [Route("file")]
        [HttpPost]
        public async Task<IActionResult> FileCheck() {
            DateTime date = DateTime.Now;

            try {
                string dataRequest = await RequestHelper.GetRequestAsync(Request);
                string response = function.CrearArchivo(config.FilesUrl, config.FilesPath, dataRequest);
                return Ok(new { Link = response });
            } catch (Exception ex) {
                log.LogError($"FILE - {date} | {DateTime.Now} - EXCEPTION: {ex}");

                return Ok(new { Link = string.Empty });
            }
        }

        [Route("calificar"), BasicAuthorization]
        [HttpPost]
        public IActionResult Calificar([FromBody] string data) {
            DateTime date = DateTime.Now;

            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.EjecutarServicio(HttpContext, Request, dataRequest, "MantenerPublic", true, true, config.CaptchaKey);
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch (Exception ex) {
                log.LogError($"CALIFICAR - {date} | {DateTime.Now} - EXCEPTION: {ex}");

                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("consultacalificar"), BasicAuthorization]
        [HttpPost]
        public IActionResult ConsultaCalificar([FromBody] string data) {
            DateTime date = DateTime.Now;

            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.EjecutarServicio(HttpContext, Request, dataRequest, "ConsultarPublic", true, true, config.CaptchaKey);
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch (Exception ex) {
                log.LogError($"CONSULTACALIFICAR - {date} | {DateTime.Now} - EXCEPTION: {ex}");

                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

    }
}
