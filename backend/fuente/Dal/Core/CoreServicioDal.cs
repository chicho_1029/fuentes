﻿using Dal.Base;
using Modelo.Entities;

namespace Dal.Core {

    public static class CoreServicioDal {

        #region METODOS

        public static Coreservicio GetServicio(short cservicio) {
            return SessionCore.GetSession().Get<Coreservicio>(cservicio);
        }

        #endregion

    }

}
