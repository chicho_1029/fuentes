﻿using Dal.Base;
using Modelo.Entities;
using NHibernate;

namespace Dal.Core {

    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreInstancia.
    /// </summary>
    public static class CoreInstanciaDal {

        #region METODOS

        public static Coreinstancia GetInstancia(int cinstancia) {
            return SessionCore.GetSession().Get<Coreinstancia>(cinstancia);
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreinstancia ActualizarInstancia(Coreinstancia obj) {
            Coreinstancia instancia = GetInstancia(obj.Cinstancia);
            instancia.Nombre = obj.Nombre;
            instancia.Servidor = obj.Servidor;
            instancia.Base = obj.Base;
            instancia.Usuario = obj.Usuario;
            instancia.Password = obj.Password;
            return instancia;
        }

        #endregion

        #region CONSULTAS

        public static IQuery GetConsultaInstancias() {
            string cmd = "Select Cinstancia as Cinstancia, Nombre as Nombre From Coreinstancia ";
            return SessionCore.GetSession().CreateQuery(cmd);
        }

        #endregion
    }

}
