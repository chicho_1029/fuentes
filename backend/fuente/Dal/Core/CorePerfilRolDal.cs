﻿using Dal.Base;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dal.Core {
    public static class CorePerfilRolDal {
        public static IList<Coreperfilrol> GetRolesToPerfiles(int perfil) {
            return SessionCore.GetSession().Query<Coreperfilrol>()
                                           .Where(x => x.Perfil == perfil).ToList();
        }
    }
}
