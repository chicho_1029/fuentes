﻿using Dal.Base;
using Modelo.Entities;
using System.Collections.Generic;
using System.Linq;
using Util.General;
using Util.Request;

namespace Dal.Core {

    public static class CoreUsuarioSesionDal {

        #region METODOS

        public static Coreusuariosesion GetSesionPorUsuario(string cusuario) {
            return SessionCore.GetSession().Get<Coreusuariosesion>(cusuario);
        }

        public static Coreusuariosesion GetSesionPorUsuarioToken(string cusuario, string token) {
            return SessionCore.GetSession().Query<Coreusuariosesion>()
                                           .FirstOrDefault(x => x.Cusuario == cusuario && x.Token == token);
        }

        public static IList<Coreusuariosesion> GetSesiones() {
            return SessionCore.GetSession().Query<Coreusuariosesion>().ToList();
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreusuariosesion CrearSession(RequestBase request, RadicacionSesion radicacion) {
            Coreusuariosesion usuarioSesion = GetSesionPorUsuario(request.Cusuario);
            if (usuarioSesion != null) {
                EliminarSesion(usuarioSesion);
            }

            Coreusuariosesion sesion = new Coreusuariosesion {
                Cusuario = request.Cusuario,
                Cmodulologin = request.Cmodulologin,
                Token = request.Token,
                Fsistema = radicacion.FechaSistema,
                Terminal = request.Terminal,
                Terminallocal = request.Terminallocal,
                Navegador = request.Navegador,
                Finicio = Fecha.GetFechaActual(),
                Tiemposesion = radicacion.TiempoSesion,
                Fultimaaccion = Fecha.GetFechaActual()
            };

            // Actualiza usuario
            Coreusuario usuario = CoreUsuarioDal.GetUsuario(request.Cusuario);
            usuario.Fultimoingreso = Fecha.GetFechaActual();
            CoreUsuarioDal.ActualizarUsuario(usuario);

            // Actualiza historia
            CoreUsuarioSesionHistoriaDal.ActualizarSesionHistoriaIngreso(sesion);

            return SessionCore.SaveChanges(sesion);
        }

        public static void EliminarSesion(Coreusuariosesion obj) {
            IList<Coreusuariosesiontransaccion> ltransacciones = CoreUsuarioSesionTransaccionDal.GetUsuariosTransaccion(obj.Cusuario);
            foreach (Coreusuariosesiontransaccion transaccion in ltransacciones) {
                ThreadDelete.Delete(transaccion);
            }

            SessionCore.DeleteChanges(obj);
        }

        #endregion

    }

}
