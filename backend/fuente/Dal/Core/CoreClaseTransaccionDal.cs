﻿using Dal.Base;
using Modelo.Entities;
using System.Linq;

namespace Dal.Core {

    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreClaseTransaccion.
    /// </summary>
    public static class CoreClaseTransaccionDal {

        public static Coreclasetransaccion GetClaseTransaccionConsulta(short cmodulo, int ctransaccion, string cclase, string centidad, string metodo) {
            return SessionCore.GetSession().Query<Coreclasetransaccion>()
                                           .FirstOrDefault(x => x.Cmodulo == cmodulo &&
                                                                x.Ctransaccion == ctransaccion &&
                                                                x.Cclase == cclase &&
                                                                x.Centidad == centidad &&
                                                                x.Metodo == metodo &&
                                                                x.Consulta);
        }

        public static Coreclasetransaccion GetClaseTransaccionMantener(short cmodulo, int ctransaccion, string cclase, string centidad, string permiso) {
            return SessionCore.GetSession().Query<Coreclasetransaccion>()
                                           .FirstOrDefault(x => x.Cmodulo == cmodulo &&
                                                                x.Ctransaccion == ctransaccion &&
                                                                x.Cclase == cclase &&
                                                                x.Centidad == centidad &&
                                                                x.Permiso == permiso &&
                                                                !x.Consulta);
        }
    }

}
