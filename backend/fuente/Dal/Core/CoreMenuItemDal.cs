﻿using Dal.Base;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dal.Core {
    public static class CoreMenuItemDal {
        #region METODOS
        public static IList<Coremenuitem> GetMenuItemPorRol(short crol) {
            return SessionCore.GetSession().Query<Coremenuitem>()
                                           .Where(x => x.Crol == crol)
                                           .OrderBy(x => x.Cmenuitem).ToList();
        }
        #endregion
    }
}
