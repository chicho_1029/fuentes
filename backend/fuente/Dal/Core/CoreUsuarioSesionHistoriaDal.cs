﻿using Dal.Base;
using Modelo.Entities;
using Newtonsoft.Json;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using Util.General;
using Util.Request;

namespace Dal.Core {

    public static class CoreUsuarioSesionHistoriaDal {

        #region METODOS

        public static Coreusuariosesionhistoria GetUsuarioSesionHistoriaIngreso(string cusuario, string token) {
            return SessionCore.GetSession().Query<Coreusuariosesionhistoria>()
                                           .FirstOrDefault(x => x.Cusuario == cusuario && x.Token == token &&
                                                                x.Ingreso == true && x.Fsalida == null);
        }

        public static IList<Coreusuariosesionhistoria> GetUsuarioSesionesIngreso(string cusuario, DateTime finicio, DateTime ffin) {
            return SessionCore.GetSession().Query<Coreusuariosesionhistoria>()
                                           .Where(x => x.Cusuario == cusuario && x.Finicio != null &&
                                                       x.Finicio >= finicio && x.Finicio <= ffin).ToList();
        }

        private static string HQL_ULTIMOINGRESO = "select max(Fcreacion) from Coreusuariosessionhistoria t where t.Cusuario = :cusuario " +
                                          "And Tipo = true and Token != :token ";

        public static DateTime GetUltimoIngreso(string cusuario, string token) {
            DateTime fultima;
            DateTime date = new DateTime(0001, 1, 1, 0, 0, 0);
            IQuery qry = SessionCore.GetSession().CreateQuery(CoreUsuarioSesionHistoriaDal.HQL_ULTIMOINGRESO);
            qry.SetParameter("cusuario", cusuario);
            qry.SetParameter("token", token);
            fultima = qry.UniqueResult<DateTime>();
            return fultima == date ? DateTime.Now : fultima;
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreusuariosesionhistoria CrearSesionHistoriaIngreso(RequestLogin request, object radicacion) {
            Coreusuariosesionhistoria historia = ConvertObject.SerializeDeserialize<Coreusuariosesionhistoria>(request, true);
            historia.Ingreso = true;
            historia.Fcreacion = Fecha.GetFechaActual();
            historia.Radicacion = JsonConvert.SerializeObject(radicacion);

            return SessionCore.SaveChanges(historia);
        }

        public static Coreusuariosesionhistoria ActualizarSesionHistoriaIngreso(Coreusuariosesion sesion) {
            Coreusuariosesionhistoria historia = GetUsuarioSesionHistoriaIngreso(sesion.Cusuario, sesion.Token);
            historia.Cmodulologin = sesion.Cmodulologin;
            historia.Finicio = Fecha.GetFechaActual();

            return SessionCore.UpdateChanges(historia);
        }

        public static Coreusuariosesionhistoria ActualizarSesionHistoriaSalida(Coreusuariosesion sesion) {
            Coreusuariosesionhistoria historia = GetUsuarioSesionHistoriaIngreso(sesion.Cusuario, sesion.Token);
            historia.Fsalida = Fecha.GetFechaActual();

            return SessionCore.UpdateChanges(historia);
        }

        #endregion

    }

}
