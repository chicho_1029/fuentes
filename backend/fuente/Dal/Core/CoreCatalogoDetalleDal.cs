﻿using Dal.Base;
using Modelo.Dto.General;
using Modelo.Entities;
using NHibernate.Transform;
using System.Collections.Generic;
using System.Linq;

namespace Dal.Core {

    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreCatalogoDetalle.
    /// </summary>
    public static class CoreCatalogoDetalleDal {

        private static CatalogoDto catalogoDto = null;

        #region METODOS

        public static Corecatalogodetalle GetDetalle(int ccatalogo, string cdetalle) {
            return SessionCore.GetSession().Query<Corecatalogodetalle>()
                                           .FirstOrDefault(x => x.Ccatalogo == ccatalogo && x.Cdetalle == cdetalle);
        }

        public static IList<Corecatalogodetalle> GetDetallesPorCatalogo(int ccatalogo) {
            return SessionCore.GetSession().Query<Corecatalogodetalle>()
                                           .Where(x => x.Ccatalogo == ccatalogo && x.Activado)
                                           .OrderBy(x => x.Cdetalle).ToList();
        }

        public static IList<Corecatalogodetalle> GetDetallesActivosPorCatalogo(int ccatalogo) {
            return SessionCore.GetSession().Query<Corecatalogodetalle>()
                                           .Where(x => x.Ccatalogo == ccatalogo && x.Activado).ToList();
        }

        #endregion

        #region MANTENIMIENTO

        public static Corecatalogodetalle ActualizarCatalogoDetalle(Corecatalogodetalle obj) {
            Corecatalogodetalle detalle = GetDetalle(obj.Ccatalogo, obj.Cdetalle);
            detalle.Nombre = obj.Nombre;
            detalle.Calterno1 = obj.Calterno1;
            detalle.Calterno2 = obj.Calterno2;
            detalle.Activado = obj.Activado;
            return detalle;
        }

        #endregion

        #region CATALOGOS

        public static IList<CatalogoDto> GetCatalogoDetalles(int ccatalogo) {
            return SessionCore.GetSession().QueryOver<Corecatalogodetalle>()
                                           .Where(x => x.Ccatalogo == ccatalogo && x.Activado == true)
                                           .SelectList(list => list
                                               .Select(x => x.Cdetalle).WithAlias(() => catalogoDto.Value)
                                               .Select(x => x.Nombre).WithAlias(() => catalogoDto.Label))
                                           .TransformUsing(Transformers.AliasToBean<CatalogoDto>()).List<CatalogoDto>();
        }

        public static IList<CatalogoDto> GetCatalogoDetallesOrdenNombre(int ccatalogo) {
            return SessionCore.GetSession().QueryOver<Corecatalogodetalle>()
                                           .Where(x => x.Ccatalogo == ccatalogo && x.Activado == true)
                                           .OrderBy(x => x.Nombre).Asc
                                           .SelectList(list => list
                                               .Select(x => x.Cdetalle).WithAlias(() => catalogoDto.Value)
                                               .Select(x => x.Nombre).WithAlias(() => catalogoDto.Label))
                                           .TransformUsing(Transformers.AliasToBean<CatalogoDto>()).List<CatalogoDto>();
        }

        #endregion
    }

}
