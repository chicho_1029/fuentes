﻿using Dal.Base;
using Modelo.Entities;

namespace Dal.Core {

    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreClase.
    /// </summary>
    public static class CoreClaseDal {

        #region METODOS

        public static Coreclase GetClase(string cclase) {
            return SessionCore.GetSession().Get<Coreclase>(cclase);
        }

        #endregion

    }

}
