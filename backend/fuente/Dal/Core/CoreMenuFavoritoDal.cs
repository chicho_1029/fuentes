﻿using Dal.Base;
using Modelo.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Dal.Core {
    public static class CoreMenuFavoritoDal {

        #region METODOS

        public static IList<Coremenufavorito> GetUsuarioRolFavorito(string cusuario, short crol) {
            return SessionCore.GetSession().Query<Coremenufavorito>()
                                           .Where(x => x.Cusuario == cusuario && x.Crol == crol).ToList();
        }

        #endregion

    }
}
