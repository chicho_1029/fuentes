﻿using Dal.Base;
using Modelo.Dto.General;
using Modelo.Entities;
using NHibernate.Transform;
using System.Collections.Generic;
using System.Linq;

namespace Dal.Core {

    public static class CoreModuloDal {

        public static Coremodulo GetModulo(short cmodulo) {
            return SessionCore.GetSession().Query<Coremodulo>()
                                           .FirstOrDefault(x => x.Cmodulo == cmodulo);
        }

        #region CATALOGOS

        public static IList<CatalogoDto> GetCatalogoModulos() {
            CatalogoDto catalogoDto = null;

            return SessionCore.GetSession().QueryOver<Coremodulo>()
                                           .Where(x => x.Activado == true)
                                           .SelectList(list => list
                                               .Select(x => x.Cmodulo).WithAlias(() => catalogoDto.Value)
                                               .Select(x => x.Nombre).WithAlias(() => catalogoDto.Label))
                                           .TransformUsing(Transformers.AliasToBean<CatalogoDto>()).List<CatalogoDto>();
        }

        #endregion

    }

}
