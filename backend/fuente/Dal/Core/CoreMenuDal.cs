﻿using Dal.Base;
using Modelo.Dto.General;
using Modelo.Entities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using Util.Request;

namespace Dal.Core {

    /// <summary>
    /// Clase dal que gestiona la endidad menu.
    /// </summary>
    public static class CoreMenuDal {

        private static readonly Coremenu alias = null;
        private static readonly Coremenuitem aliasItem = null;

        #region METODOS

        //public static Coremenu GetMenu(short crol, int cmenu) {
        //    return SessionCore.GetSession().Query<Coremenu>()
        //                                   .FirstOrDefault(x => x.Crol == crol && x.Cmenu == cmenu);
        //}

        //public static IList<Coremenu> GetMenuPorRol(short crol) {
        //    return SessionCore.GetSession().Query<Coremenu>()
        //                                   .Where(x => x.Crol == crol)
        //                                   .OrderBy(x => x.Crol).ToList();
        //}

        public static IList<CoremenuDto> GetMenuActivoPorRol(IList<RadicacionRol> lroles) {
            CoremenuDto coremenuDto = null;
            List<short> Introles = new List<short>();
            foreach (RadicacionRol radicacionRol in lroles) {
                Introles.Add(radicacionRol.Crol);
            }

            var query = SessionCore.GetSession().QueryOver(() => alias)
                                        .JoinEntityAlias(() => aliasItem, () => aliasItem.Cmenu == alias.Cmenu)
                                        .Where(x => x.Activado == true && x.Mostrar == true && aliasItem.Izquierda == false)
                                        .WhereRestrictionOn(() => aliasItem.Crol).IsIn(Introles)
                                        .OrderBy(() => aliasItem.Orden).Desc;

            query.SelectList(list => list
                .Select(() => alias.Cmenu).WithAlias(() => coremenuDto.Cmenu)
                .Select(() => aliasItem.Cmenupadre).WithAlias(() => coremenuDto.Cmenupadre)
                .Select(() => alias.Cmodulo).WithAlias(() => coremenuDto.Cmodulo)
                .Select(() => alias.Ctransaccion).WithAlias(() => coremenuDto.Ctransaccion)
                .Select(() => alias.Nombre).WithAlias(() => coremenuDto.Nombre)
                .Select(() => aliasItem.Icono).WithAlias(() => coremenuDto.Icono)
                .Select(() => aliasItem.Orden).WithAlias(() => coremenuDto.Orden)
                .Select(() => alias.Mostrar).WithAlias(() => coremenuDto.Mostrar)
                .Select(() => alias.Activado).WithAlias(() => coremenuDto.Activado)
                .Select(() => aliasItem.Izquierda).WithAlias(() => coremenuDto.Izquierda));

            return query.TransformUsing(Transformers.AliasToBean<CoremenuDto>()).List<CoremenuDto>();
        }

        public static IList<CoremenuDto> GetMenuPadrePorRol(List<short> lroles) {
            CoremenuDto coremenuDto = null;
            var query = SessionCore.GetSession().QueryOver(() => alias)
                                        .JoinEntityAlias(() => aliasItem, () => aliasItem.Cmenu == alias.Cmenu)
                                        .Where(x => x.Activado == true && x.Mostrar == true && aliasItem.Izquierda == false && aliasItem.Cmenupadre == null)
                                        .WhereRestrictionOn(() => aliasItem.Crol).IsIn(lroles)
                                        .OrderBy(() => aliasItem.Orden).Desc;
            query.SelectList(list => list
                .Select(() => alias.Cmenu).WithAlias(() => coremenuDto.Cmenu)
                .Select(() => aliasItem.Cmenupadre).WithAlias(() => coremenuDto.Cmenupadre)
                .Select(() => alias.Cmodulo).WithAlias(() => coremenuDto.Cmodulo)
                .Select(() => alias.Ctransaccion).WithAlias(() => coremenuDto.Ctransaccion)
                .Select(() => alias.Nombre).WithAlias(() => coremenuDto.Nombre)
                .Select(() => aliasItem.Icono).WithAlias(() => coremenuDto.Icono)
                .Select(() => aliasItem.Orden).WithAlias(() => coremenuDto.Orden)
                .Select(() => alias.Mostrar).WithAlias(() => coremenuDto.Mostrar)
                .Select(() => alias.Activado).WithAlias(() => coremenuDto.Activado)
                .Select(() => aliasItem.Izquierda).WithAlias(() => coremenuDto.Izquierda));

            return query.TransformUsing(Transformers.AliasToBean<CoremenuDto>()).List<CoremenuDto>();

        }


        public static IList<CoremenuDto> GetMenuPadreSidebar(List<short> lroles, int cmenusidebar) {
            CoremenuDto coremenuDto = null;
            var query = SessionCore.GetSession().QueryOver(() => alias)
                                        .JoinEntityAlias(() => aliasItem, () => aliasItem.Cmenu == alias.Cmenu)
                                        .Where(x => x.Activado == true && x.Mostrar == true && aliasItem.Izquierda == false && aliasItem.Cmenupadre == cmenusidebar)
                                        .WhereRestrictionOn(() => aliasItem.Crol).IsIn(lroles)
                                        .OrderBy(() => aliasItem.Orden).Desc;
            query.SelectList(list => list
                .Select(() => alias.Cmenu).WithAlias(() => coremenuDto.Cmenu)
                .Select(() => aliasItem.Cmenupadre).WithAlias(() => coremenuDto.Cmenupadre)
                .Select(() => alias.Cmodulo).WithAlias(() => coremenuDto.Cmodulo)
                .Select(() => alias.Ctransaccion).WithAlias(() => coremenuDto.Ctransaccion)
                .Select(() => alias.Nombre).WithAlias(() => coremenuDto.Nombre)
                .Select(() => aliasItem.Icono).WithAlias(() => coremenuDto.Icono)
                .Select(() => aliasItem.Orden).WithAlias(() => coremenuDto.Orden)
                .Select(() => alias.Mostrar).WithAlias(() => coremenuDto.Mostrar)
                .Select(() => alias.Activado).WithAlias(() => coremenuDto.Activado)
                .Select(() => aliasItem.Izquierda).WithAlias(() => coremenuDto.Izquierda));

            return query.TransformUsing(Transformers.AliasToBean<CoremenuDto>()).List<CoremenuDto>();

        }


        public static IList<CoremenuDto> GetMenuPorRolPadre(int cmenupadre) {
            CoremenuDto coremenuDto = null;

            var query = SessionCore.GetSession().QueryOver(() => alias)
                                        .JoinEntityAlias(() => aliasItem, () => aliasItem.Cmenu == alias.Cmenu)
                                        .Where(x => aliasItem.Cmenupadre == cmenupadre && x.Activado == true && x.Mostrar == true && aliasItem.Izquierda == false)
                                        .OrderBy(() => aliasItem.Orden).Desc;
            query.SelectList(list => list
                      .Select(() => alias.Cmenu).WithAlias(() => coremenuDto.Cmenu)
                      .Select(() => aliasItem.Cmenupadre).WithAlias(() => coremenuDto.Cmenupadre)
                      .Select(() => alias.Cmodulo).WithAlias(() => coremenuDto.Cmodulo)
                      .Select(() => alias.Ctransaccion).WithAlias(() => coremenuDto.Ctransaccion)
                      .Select(() => alias.Nombre).WithAlias(() => coremenuDto.Nombre)
                      .Select(() => aliasItem.Icono).WithAlias(() => coremenuDto.Icono)
                      .Select(() => aliasItem.Orden).WithAlias(() => coremenuDto.Orden)
                      .Select(() => alias.Mostrar).WithAlias(() => coremenuDto.Mostrar)
                      .Select(() => alias.Activado).WithAlias(() => coremenuDto.Activado)
                      .Select(() => aliasItem.Izquierda).WithAlias(() => coremenuDto.Izquierda));

            return query.TransformUsing(Transformers.AliasToBean<CoremenuDto>()).List<CoremenuDto>();
        }

        public static int GetMenuMaximoPorRol(short crol) {
            var max = SessionCore.GetSession().QueryOver(() => alias)
                                        .JoinEntityAlias(() => aliasItem, () => aliasItem.Cmenu == alias.Cmenu)
                                        .Where(() => aliasItem.Crol == crol)
                                        .Select(Projections.Max<Coremenu>(x => x.Cmenu))
                                        .SingleOrDefault();
            return max == null ? 1 : Convert.ToInt32(max);
        }

        public static IList<SidebarDto> GetSidebar(List<short> lroles) {
            int numnotifiacion = 0;
            SidebarDto sidebarDto = null;
            var query = SessionCore.GetSession().QueryOver(() => alias)
                                     .JoinEntityAlias(() => aliasItem, () => aliasItem.Cmenu == alias.Cmenu)
                                     .Where(x => x.Activado == true && x.Mostrar == true && aliasItem.Izquierda == true)
                                     .WhereRestrictionOn(() => aliasItem.Crol).IsIn(lroles)
                                     .OrderBy(() => aliasItem.Orden).Asc;
            query.SelectList(list => list
                      .Select(() => alias.Cmenu).WithAlias(() => sidebarDto.Cmenu)
                      .Select(() => alias.Nombre).WithAlias(() => sidebarDto.Nombre)
                      .Select(() => aliasItem.Orden).WithAlias(() => sidebarDto.Orden)
                      .Select(() => aliasItem.Icono).WithAlias(() => sidebarDto.Icono)
                      .Select(() => alias.Cmodulo).WithAlias(() => sidebarDto.Cmodulo)
                      .Select(() => alias.Ctransaccion).WithAlias(() => sidebarDto.Ctransaccion)
                      .Select(() => aliasItem.Sidebaractive).WithAlias(() => sidebarDto.Sidebaractive)
                      .Select(() => aliasItem.Notificacion).WithAlias(() => sidebarDto.Notificacion)
                      .Select(() => aliasItem.Cargarpantalla).WithAlias(() => sidebarDto.Cargarpantalla)
                      .Select(Projections.Conditional(
                              Restrictions.IsNull(Projections.Property(() => alias.Ctransaccion)),
                                                  Projections.Constant(true),
                                                  Projections.Constant(false))).WithAlias(() => sidebarDto.Essubmenu)
                      .Select(() => numnotifiacion).WithAlias(() => sidebarDto.Numnotifiacion)
                      .Select(() => true).WithAlias(() => sidebarDto.Mostrar));

            return query.TransformUsing(Transformers.AliasToBean<SidebarDto>()).List<SidebarDto>();
        }

        #endregion

        #region MANTENIMIENTO

        public static Coremenu CrearMenu(Coremenu obj) {
            return SessionCore.Save(obj);
        }

        public static Coremenu ActualizarMenu(Coremenu obj) {
            return SessionCore.Update(obj);
        }

        public static void EliminarMenu(Coremenu obj) {
            SessionCore.Delete(obj);
        }

        #endregion

    }
}
