﻿using Dal.Base;
using Modelo.Entities;

namespace Dal.Core {

    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreEntidad.
    /// </summary>
    public static class CoreEntidadDal {

        #region METODOS

        public static Coreentidad GetEntidad(string centidad) {
            return SessionCore.GetSession().Get<Coreentidad>(centidad);
        }

        #endregion

    }

}
