﻿using Dal.Base;
using Modelo.Entities;
using System.Collections.Generic;
using System.Linq;
using Util.General;
using Util.Request;

namespace Dal.Core {

    public static class CoreFortiDal {

        #region METODOS

        public static Coreforti GetFortiPorIdentificador(RequestBase request, string identificador, string identificacion) {
            return SessionCore.GetSession().Query<Coreforti>()
                                           .FirstOrDefault(x => x.Cmodulo == request.Cmodulo &&
                                                                x.Ctransaccion == request.Ctransaccion &&
                                                                x.Identificador == identificador &&
                                                                x.Identificacion == identificacion);
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreforti AsignarForti(RequestBase request, string identificador, string identificacion, string nombre, string celular, string correo) {
            Coreforti obj = SessionCore.GetSession().Query<Coreforti>().FirstOrDefault(x => x.Cmodulo == request.Cmodulo &&
                                                                                            x.Ctransaccion == request.Ctransaccion &&
                                                                                            x.Identificador == identificador);
            if (obj == null) {
                obj = SessionCore.GetSession().Query<Coreforti>().FirstOrDefault(x => x.Identificador == null);
            }
            obj.Cmodulo = request.Cmodulo;
            obj.Ctransaccion = request.Ctransaccion;
            obj.Cusuario = request.Cusuario;
            obj.Token = request.Token;
            obj.Identificador = identificador;
            obj.Identificacion = identificacion;
            obj.Nombre = nombre;
            obj.Celular = celular;
            obj.Correo = correo;
            obj.Fcaducidad = Fecha.GetFechaCaducidad(10);

            ThreadUpdate.Update(obj);
            return obj;
        }

        public static void EliminarForti() {
            IList<Coreforti> lForti = SessionCore.GetSession().Query<Coreforti>().Where(x => x.Fcaducidad < Fecha.GetFechaActual()).ToList();
            foreach (Coreforti forti in lForti) {
                forti.Cmodulo = null;
                forti.Ctransaccion = null;
                forti.Cusuario = null;
                forti.Token = null;
                forti.Identificador = null;
                forti.Identificacion = null;
                forti.Nombre = null;
                forti.Celular = null;
                forti.Correo = null;
                forti.Fcaducidad = null;

                ThreadUpdate.Update(forti);
            }
        }

        #endregion
    }
}
