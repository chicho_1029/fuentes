﻿using Dal.Base;
using Modelo.Entities;

namespace Dal.Core {

    public static class CoreMensajeDal {

        #region METODOS

        public static Coremensaje GetMensaje(string cmensaje) {
            return SessionCore.GetSession().Get<Coremensaje>(cmensaje);
        }

        #endregion
    }
}
