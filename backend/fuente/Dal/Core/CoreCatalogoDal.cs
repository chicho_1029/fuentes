﻿using Dal.Base;
using Modelo.Dto.General;
using Modelo.Entities;
using NHibernate.Transform;
using System.Collections.Generic;

namespace Dal.Core {

    public static class CoreCatalogoDal {

        #region METODOS

        public static Corecatalogo GetCatalogo(int ccatalogo) {
            return SessionCore.GetSession().Get<Corecatalogo>(ccatalogo);
        }

        #endregion

        #region MANTENIMIENTO

        public static Corecatalogo ActualizarCatalogo(Corecatalogo obj) {
            Corecatalogo catalogo = GetCatalogo(obj.Ccatalogo);
            catalogo.Nombre = obj.Nombre;
            catalogo.Activado = obj.Activado;
            return catalogo;
        }

        #endregion

        #region CATALOGOS

        public static IList<CatalogoDto> GetCatalogoCatalogos(short cmodulo) {
            CatalogoDto catalogoDto = null;

            return SessionCore.GetSession().QueryOver<Corecatalogo>()
                                           .Where(x => x.Cmodulo == cmodulo && x.Activado == true)
                                           .SelectList(list => list
                                               .Select(x => x.Ccatalogo).WithAlias(() => catalogoDto.Value)
                                               .Select(x => x.Nombre).WithAlias(() => catalogoDto.Label))
                                           .TransformUsing(Transformers.AliasToBean<CatalogoDto>()).List<CatalogoDto>();
        }

        #endregion

    }

}
