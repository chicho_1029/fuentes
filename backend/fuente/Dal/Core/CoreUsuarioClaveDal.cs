﻿using Dal.Base;
using Modelo.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;
using Util.General;
using Util.Seguridad;

namespace Dal.Core {
    public static class CoreUsuarioClaveDal {

        #region METODOS

        public static Coreusuarioclave GetClave(string cusuario) {
            string querycmd = "Select t From Coreusuarioclave t, Coreusuario u where t.Cusuario=u.Cusuario and t.Cclave=u.Cclave and u.Cusuario=:pCusuario";
            IQuery queryhb = SessionCore.GetSession().CreateQuery(querycmd);
            queryhb.SetParameter("pCusuario", cusuario);
            return queryhb.UniqueResult<Coreusuarioclave>();
        }

        private static string HQL = "Select Max(t.Cclave) from Coreusuarioclave t where t.Cusuario = :pCusuario";

        public static int GetMaxClave(string cusuario) {
            int max = 0;
            IQuery queryhb = SessionCore.GetSession().CreateQuery(CoreUsuarioClaveDal.HQL);
            queryhb.SetParameter("pCusuario", cusuario);
            max = queryhb.UniqueResult<int>();
            return max;
        }

        private static string HQL_ULTIMOS_N = "SELECT t.Fcreacion,t.Password FROM Coreusuarioclave t "
                                            + "where t.Cusuario = :pCusuario AND t.Temporal = 0 "
                                            + "Order By t.Cclave Desc ";

        /// <summary>
        /// Enterga una lista de registros asociados a un usuario.
        /// </summary>
        public static IList<object[]> GetPasswords(string cusuario, int numeroregistros) {
            IList<object[]> ldata = null;
            IQuery queryhb = SessionCore.GetSession().CreateQuery(HQL_ULTIMOS_N);
            queryhb.SetParameter("pCusuario", cusuario);
            queryhb.SetMaxResults(numeroregistros);
            ldata = queryhb.List<object[]>();
            return ldata;
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreusuarioclave CrearClave(string cusuario, string password) {
            Coreusuarioclave obj = new Coreusuarioclave();
            obj.Cclave = CoreUsuarioClaveDal.GetMaxClave(cusuario) + 1;
            obj.Cusuario = cusuario;
            obj.Password = Password.Encriptar(password);
            obj.Fcreacion = Fecha.GetFechaActual();
            SessionCore.GetSession().Save(obj);
            return obj;
        }

        #endregion
    }
}
