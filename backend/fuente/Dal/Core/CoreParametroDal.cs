﻿using Dal.Base;
using Modelo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dal.Core {

    public static class CoreParametroDal {

        #region METODOS

        public static IList<Coreparametro> GetParametros() {
            return SessionCore.GetSession().Query<Coreparametro>().ToList();
        }

        public static Coreparametro GetParametro(string cparametro) {
            return SessionCore.GetSession().Get<Coreparametro>(cparametro);
        }

        public static short GetValueShort(string cparametro) {
            return Convert.ToInt16(GetParametro(cparametro).Valor);
        }

        public static int GetValueInteger(string cparametro) {
            return Convert.ToInt32(GetParametro(cparametro).Valor);
        }

        public static long GetValueLong(string cparametro) {
            return Convert.ToInt64(GetParametro(cparametro).Valor);
        }

        public static string GetValueString(string cparametro) {
            return Convert.ToString(GetParametro(cparametro).Valor);
        }

        public static decimal GetValueDecimal(string cparametro) {
            return Convert.ToDecimal(GetParametro(cparametro).Valor);
        }

        public static bool GetValueBoolean(string cparametro) {
            return Convert.ToBoolean(GetParametro(cparametro).Valor);
        }

        public static IList<Coreparametro> GetParametrosLista(List<string> parametros) {
            return SessionCore.GetSession().Query<Coreparametro>()
                                           .Where(x => parametros.Contains(x.Cparametro)).ToList();
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreparametro ActualizarParametro(Coreparametro obj) {
            Coreparametro parametro = GetParametro(obj.Cparametro);
            parametro.Nombre = obj.Nombre;
            parametro.Valor = obj.Valor;
            return parametro;
        }

        #endregion
    }

}
