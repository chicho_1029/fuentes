﻿using Dal.Base;
using Modelo.Dto;
using Modelo.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Text;
using Util.Request;

namespace Dal.Core {
    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreEnte.
    /// </summary>
    public static class CoreEnteDal {

        #region METODOS

        public static Coreente GetEnte(long cente) {
            return SessionCore.GetSession().QueryOver<Coreente>().Where(x => x.Cente == cente).SingleOrDefault();
        }

        public static Coreente GetEntePorIdentificacion(string identificacion) {
            return SessionCore.GetSession().QueryOver<Coreente>().Where(x => x.Identificacion == identificacion).SingleOrDefault();
        }

        public static Coreente GetEntePorCorreo(long cente, string correo) {
            return SessionCore.GetSession().QueryOver<Coreente>().Where(x => x.Cente != cente && x.Email == correo).SingleOrDefault();
        }

        public static Coreente GetEntePorCelular(long cente, string celular) {
            return SessionCore.GetSession().QueryOver<Coreente>().Where(x => x.Cente != cente && x.Celular == celular).SingleOrDefault();
        }

        public static IList<CoreEnteDto> GetEntesHelper(string identificacion, string nombre) {
            string cmd = "Select t From Coreente t Where Identificacion like :pIdentificacion And Nombre like :pNombre";
            IQuery query = SessionCore.GetSession().CreateQuery(cmd);
            query.SetParameter("pIdentificacion", "%" + identificacion + "%");
            query.SetParameter("pNombre", "%" + nombre + "%");
            return ConvertObject.SerializeDeserialize<IList<CoreEnteDto>>(query.List<Coreente>());
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreente CrearEnte(RequestMantener request, Coreente ente) {
            Coreente e = ente;
            e.Cusuariocre = request.Cusuario;
            e.Fcreacion = request.Freal;
            e.Tipoente = "P";

            SessionCore.GetSession().Save(e);
            return e;
        }

        public static Coreente ActualizarEnte(RequestMantener request, Coreente ente) {
            Coreente e = ente;
            e.Cusuariomod = request.Cusuario;
            e.Fmodificacion = request.Freal;

            SessionCore.GetSession().Merge(e);
            return e;
        }

        #endregion
    }

}
