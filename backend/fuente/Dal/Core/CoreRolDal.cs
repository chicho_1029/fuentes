﻿using Dal.Base;
using Modelo.Dto.General;
using Modelo.Entities;
using NHibernate;
using NHibernate.Transform;
using System.Collections.Generic;
using System.Linq;
using Util.Request;

namespace Dal.Core {

    public static class CoreRolDal {

        #region METODOS

        public static Corerol GetRol(short crol) {
            return SessionCore.GetSession().Get<Corerol>(crol);
        }

        public static IList<Corerol> GetRoles() {
            return SessionCore.GetSession().Query<Corerol>().ToList();
        }

        public static IList<Corerol> GetRolesActivos() {
            return SessionCore.GetSession().Query<Corerol>()
                                           .Where(x => x.Activado).ToList();
        }

        public static IList<Corerol> GetRoles(string cusuario) {
            string querycmd = "Select r From Corerol r, Coreusuariorol t where t.Crol = r.Crol and t.Cusuario = :pUsuario";
            IQuery queryhb = SessionCore.GetSession().CreateQuery(querycmd);
            queryhb.SetParameter("pUsuario", cusuario);
 
            return queryhb.List<Corerol>();
         }

        /// <summary>
        /// Entrega los roles.
        /// </summary>
        public static List<short> GetRolesLogeado(short cmodulologin, string cusuario, string token) {
            Coreusuariosesionhistoria historia = CoreUsuarioSesionHistoriaDal.GetUsuarioSesionHistoriaIngreso(cusuario, token);
            List<RadicacionModulo> lradmodulos = ConvertObject.Deserialize<List<RadicacionModulo>>(historia.Radicacion);
            RadicacionModulo radicacionModulo = lradmodulos.SingleOrDefault(x => x.Cmodulologin == cmodulologin);

            List<short> lroles = new List<short>();
            foreach (RadicacionRol radicacionRol in radicacionModulo.lroles) {
                lroles.Add(radicacionRol.Crol);
            }
            return lroles;
        }

        #endregion

        #region MANTENIMIENTO

        public static Corerol ActualizarRol(Corerol obj) {
            Corerol rol = GetRol(obj.Crol);
            rol.Nombre = obj.Nombre;
            //rol.Perfil = obj.Perfil;
            rol.Activado = obj.Activado;
            return rol;
        }

        #endregion

        #region CATALOGOS

        public static IList<CatalogoDto> GetCatalogoRoles() {
            CatalogoDto catalogoDto = null;

            return SessionCore.GetSession().QueryOver<Corerol>()
                                           .Where(x => x.Activado == true)
                                           .SelectList(list => list
                                               .Select(x => x.Crol).WithAlias(() => catalogoDto.Value)
                                               .Select(x => x.Nombre).WithAlias(() => catalogoDto.Label))
                                           .TransformUsing(Transformers.AliasToBean<CatalogoDto>()).List<CatalogoDto>();
        }

        #endregion

    }

}
