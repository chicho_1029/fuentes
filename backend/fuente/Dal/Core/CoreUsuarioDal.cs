﻿using Dal.Base;
using Modelo.Entities;
using System.Collections.Generic;
using System.Linq;
using Util.General;

namespace Dal.Core {

    public static class CoreUsuarioDal {

        #region METODOS

        public static Coreusuario GetUsuario(string cusuario) {
            return SessionCore.GetSession().Get<Coreusuario>(cusuario);
        }

        public static Coreusuario GetUsuario(string cusuario, string ccanal) {
            Coreusuario obj;
            obj = SessionCore.GetSession().QueryOver<Coreusuario>().Where(x => x.Cusuario == cusuario
            && x.Ccanal == ccanal).SingleOrDefault();
            return obj;
        }

        public static IList<Coreusuario> GetUsuarios() {
            return SessionCore.GetSession().Query<Coreusuario>().ToList();
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreusuario CrearUsuario(Coreusuario obj) {
            obj.Fultimoingreso = Fecha.GetFechaActual();

            return SessionCore.SaveChanges(obj);
        }

        public static Coreusuario ActualizarUsuario(Coreusuario obj) {
            return SessionCore.UpdateChanges(obj);
        }

        #endregion

    }

}
