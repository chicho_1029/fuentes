﻿using Dal.Base;
using Modelo.Dto.General;
using Modelo.Entities;
using NHibernate.Transform;
using System.Collections.Generic;

namespace Dal.Core {

    /// <summary>
    /// Clase que permite realizar la administración de la tabla CoreCanal.
    /// </summary>
    public static class CoreCanalDal {

        #region METODOS

        public static Corecanal GetCanal(string ccanal) {
            return SessionCore.GetSession().Get<Corecanal>(ccanal);
        }

        #endregion

        #region MANTENIMIENTO

        public static Corecanal ActualizarCanal(Corecanal obj) {
            Corecanal canal = GetCanal(obj.Ccanal);
            canal.Activado = obj.Activado;
            canal.Diasmensajeinvalidez = obj.Diasmensajeinvalidez;
            canal.Diasvalidez = obj.Diasvalidez;
            canal.Especiales = obj.Especiales;
            canal.Intentos = obj.Intentos;
            canal.Longitud = obj.Longitud;
            canal.Mayusculas = obj.Mayusculas;
            canal.Minusculas = obj.Minusculas;
            canal.Nombre = obj.Nombre;
            canal.Numeros = obj.Numeros;
            canal.Repeticiones = obj.Repeticiones;
            canal.Requiereotp = obj.Requiereotp;
            canal.Tiemposesion = obj.Tiemposesion;
            return canal;
        }

        #endregion

        #region CATALOGOS

        public static IList<CatalogoDto> GetCatalogoCanal() {
            CatalogoDto catalogoDto = null;

            return SessionCore.GetSession().QueryOver<Corecanal>()
                                           .Where(x => x.Activado == true)
                                           .SelectList(list => list
                                               .Select(x => x.Ccanal).WithAlias(() => catalogoDto.Value)
                                               .Select(x => x.Nombre).WithAlias(() => catalogoDto.Label))
                                           .TransformUsing(Transformers.AliasToBean<CatalogoDto>()).List<CatalogoDto>();
        }

        #endregion

    }
}
