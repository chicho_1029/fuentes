﻿using Dal.Base;
using Modelo.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dal.Core {

    public static class CoreTransaccionDal {

        #region METODOS

        public static Coretransaccion GetTransaccion(short cmodulo, int ctransaccion) {
            return SessionCore.GetSession().Query<Coretransaccion>()
                                           .FirstOrDefault(x => x.Ctransaccion == ctransaccion && x.Cmodulo == cmodulo);
        }

        public static IList<Coretransaccion> GetTransacciones(short cmodulo) {
            return SessionCore.GetSession().Query<Coretransaccion>()
                                           .Where(x => x.Cmodulo == cmodulo).ToList();
        }

        private static String SQL_RUTAS = "SELECT m.Cmodulo, m.Ctransaccion, "
                                        + "(select t.Ruta from Coretransaccion t "
                                        + "where t.Ctransaccion = m.Ctransaccion "
                                        + "and t.Cmodulo = m.Cmodulo) as ruta "
                                        + "FROM Coremenu m, CoreMenuItem mi "
                                        + "WHERE m.Cmenu = mi.Cmenu "
                                        + "AND mi.Crol in (:pLroles) "
                                        + "AND m.Activado = 1 "
                                        + "AND m.ctransaccion IS NOT NULL  "
                                        + "ORDER BY mi.Orden";

        /// <summary>
        /// Entrega una lista de rutas por transaccion.
        /// </summary>
        public static IList<object> GetRutas(List<short> lroles) {

            IQuery qry = SessionCore.GetSession().CreateSQLQuery(SQL_RUTAS)
                .AddScalar("cmodulo", NHibernateUtil.Int16)
                .AddScalar("ctransaccion", NHibernateUtil.Int32)
                .AddScalar("ruta", NHibernateUtil.String);

            qry.SetParameterList("pLroles", lroles);
            return qry.List<object>();
        }

        #endregion

        #region CATALOGOS

        public static IQuery GetCatalogoTransaccionesPorRuta() {
            string cmd = "Select Cmodulo as Cmodulo, Ctransaccion As Ctransaccion, Nombre as Nombre From Coretransaccion " +
                          "Where Ruta != null ";
            return SessionCore.GetSession().CreateQuery(cmd);
        }

        #endregion
    }
}
