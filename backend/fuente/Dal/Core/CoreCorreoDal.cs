﻿using Dal.Base;
using Modelo.Entities;
using NHibernate;

namespace Dal.Core {

    public static class CoreCorreoDal {

        #region METODOS

        public static Corecorreo GetCorreo(short cmail) {
            return SessionCore.GetSession().Get<Corecorreo>(cmail);
        }

        #endregion

        #region MANTENIMIENTO

        public static Corecorreo ActualizarCorreo(Corecorreo obj) {
            Corecorreo mail = GetCorreo(obj.Ccorreo);
            mail.Nombre = obj.Nombre;
            mail.Direccion = obj.Direccion;
            mail.Remitente = obj.Remitente;
            mail.Password = obj.Password;
            mail.Hostsmtp = obj.Hostsmtp;
            mail.Puertosmtp = obj.Puertosmtp;
            mail.Hostimap = obj.Hostimap;
            mail.Puertoimap = obj.Puertoimap;
            mail.Ssl = obj.Ssl;
            return mail;
        }

        #endregion

        #region CONSULTAS

        public static IQuery GetConsultaCorreos() {
            string cmd = "Select Ccorreo as Ccorreo, Remitente as Nombreremitente From Corecorreo ";
            return SessionCore.GetSession().CreateQuery(cmd);
        }

        #endregion
    }

}
