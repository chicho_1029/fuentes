﻿using Dal.Base;
using Modelo.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Dal.Core {

    public static class CoreUsuarioSesionTransaccionDal {

        #region METODOS

        public static Coreusuariosesiontransaccion GetUsuarioTransaccion(string cusuario, string token, string operaciontransaccion) {
            return SessionCore.GetSession().Query<Coreusuariosesiontransaccion>()
                                           .FirstOrDefault(x => x.Cusuario == cusuario &&
                                                                x.Token == token &&
                                                                x.Operaciontransaccion == operaciontransaccion);
        }

        public static IList<Coreusuariosesiontransaccion> GetUsuariosTransaccion(string cusuario) {
            return SessionCore.GetSession().Query<Coreusuariosesiontransaccion>()
                                           .Where(x => x.Cusuario == cusuario).ToList();
        }

        #endregion

        #region MANTENIMIENTO

        public static void CrearUsuarioTransaccion(string cusuario, string token, string operaciontransaccion) {
            Coreusuariosesiontransaccion sesiontransaccion = new Coreusuariosesiontransaccion();
            sesiontransaccion.Cusuario = cusuario;
            sesiontransaccion.Token = token;
            sesiontransaccion.Operaciontransaccion = operaciontransaccion;

            ThreadInsert.Save(sesiontransaccion);
        }

        #endregion

    }

}
