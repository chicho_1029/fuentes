﻿using Dal.Base;
using Modelo.Entities;

namespace Dal.Core {

    public static class CoreNotificacionDal {

        #region METODOS

        public static Corenotificacion Getnotificacion(short cnotificacion) {
            return SessionCore.GetSession().Get<Corenotificacion>(cnotificacion);
        }

        #endregion

    }

}
