﻿using Dal.Base;
using Modelo.Entities;
using NHibernate;
using Util.General;

namespace Dal.Core {

    public static class CoreUsuarioOtpDal {

        #region METODOS

        public static Coreusuariootp GetUsuarioOtp(long cotp) {
            return SessionCore.GetSession().Get<Coreusuariootp>(cotp);
        }

        public static long GetGetUsuarioOtpMax(string cusuario) {
            long max = 0;
            string hql = "Select max(t.Cotp) From Coreusuariootp t Where t.Cusuario=:pUsuario";
            IQuery query = SessionCore.GetSession().CreateQuery(hql);
            query.SetParameter("pUsuario", cusuario);
            max = query.UniqueResult<long>();
            return max;
        }

        #endregion

        #region MANTENIMIENTO

        public static Coreusuariootp Crear(string cusuario, short cmodulo, int ctransaccion, string codigootp) {
            Coreusuariootp otp = new Coreusuariootp();
            otp.Cusuario = cusuario;
            otp.Cmodulo = cmodulo;
            otp.Ctransaccion = ctransaccion;
            otp.Codigootp = codigootp;
            otp.Fcreacion = Fecha.GetFechaActual();
            otp.Fcaducidad = otp.Fcreacion.AddMinutes(CoreParametroDal.GetValueInteger("OTPCADUCA"));
            otp.Validado = false;

            return SessionCore.SaveChanges(otp);
        }

        public static Coreusuariootp Actualizar(Coreusuariootp obj) {
            obj.Validado = true;
            obj.Fvalidado = Fecha.GetFechaActual();

            return SessionCore.UpdateChanges(obj);
        }

        #endregion

    }

}
