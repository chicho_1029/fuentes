﻿using Dal.Base;
using Modelo.Entities;
using NHibernate;

namespace Dal.Core {

    public static class CoreSecuenciaDal {

        #region METODOS

        public static Coresecuencia GetSecuencia(int csecuencia) {
            return SessionCore.GetSession().Get<Coresecuencia>(csecuencia);
        }

        public static Coresecuencia GetSecuenciaProximoValor(ISession session, int csecuencia) {
            Coresecuencia secuencia = session.Get<Coresecuencia>(csecuencia);
            secuencia.Valoractual += 1;

            return secuencia;
        }

        #endregion

    }
}
