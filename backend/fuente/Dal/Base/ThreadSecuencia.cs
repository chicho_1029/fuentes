﻿using Dal.Core;
using Modelo.Entities;
using NHibernate;
using System;
using System.Threading;

namespace Dal.Base {

    public class ThreadSecuencia {

        private static int codigosecuencia = 0;
        private static long valorsecuencia = 0;

        /// <summary>
        /// Entrega el proximo numero de secuencia dado el codigo de secuencia.
        /// </summary>
        public static long GetProximovalor(int csecuencia) {
            codigosecuencia = csecuencia;

            ThreadSecuencia secuencia = new ThreadSecuencia();
            Thread thread = new Thread(secuencia.Ejecutar);
            thread.Start();
            thread.Join();

            return valorsecuencia;
        }

        /// <summary>
        /// Obtiene y actualiza en la base el proximo codigo de secuencia.
        /// </summary>
        private void Ejecutar() {

            ISession session = null;
            try {
                using (session = SessionHelper.SessionFactory.OpenSession())
                using (var transaction = session.BeginTransaction()) {
                    Coresecuencia secuencia = CoreSecuenciaDal.GetSecuenciaProximoValor(session, codigosecuencia);
                    valorsecuencia = secuencia.Valoractual;

                    session.Merge(secuencia);
                    session.Flush();
                    transaction.Commit();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            } finally {
                if (session != null && session.IsOpen) {
                    session.Close();
                }
            }

        }
    }
}
