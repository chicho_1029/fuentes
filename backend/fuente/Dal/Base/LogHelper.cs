﻿using Microsoft.Extensions.Logging;
using Modelo.Entities;
using NHibernate;
using System;
using System.Reflection;
using System.Threading;
using Util.Configuracion;
using Util.Request;

namespace Dal.Base {

    /// <summary>
    /// Clase que conecta a bdd de log
    /// </summary>
    public class LogHelper {

        private readonly ILogger log = SettingsManager.GetLogger();
        private Corelog coreLog;

        /// <summary>
        /// Metodo que permite grabar el log.
        /// </summary>
        public void GrabarLog(string recurso, object data, Exception exception = null, bool error = false) {
            try {
                coreLog = ConvertObject.SerializeDeserialize<Corelog>(SessionCore.GetRequest());
                coreLog.Freal = DateTime.Now;
                coreLog.Error = error;
                coreLog.Recurso = recurso;
                coreLog.Mensaje = FormatObject.LogTexto(FormatObject.Log(data));
                coreLog.Excepcion = exception?.GetBaseException().Message;

                Thread thread = new Thread(EjecutarLog);
                thread.Start();
            } catch (Exception ex) {
                log.LogError($"Log: {ex}");
            }
        }

        /// <summary>
        /// Metodo que permite grabar el log.
        /// </summary>
        public void GrabarLog(MethodBase method, object data, Exception exception = null, bool error = false) {
            try {
                coreLog = ConvertObject.SerializeDeserialize<Corelog>(SessionCore.GetRequest());
                coreLog.Freal = DateTime.Now;
                coreLog.Error = error;
                coreLog.Recurso = string.Join(".", method.DeclaringType.FullName, method.Name);
                coreLog.Mensaje = FormatObject.LogTexto(FormatObject.Log(data));
                coreLog.Excepcion = exception?.GetBaseException().Message;

                Thread thread = new Thread(EjecutarLog);
                thread.Start();
            } catch (Exception ex) {
                log.LogError($"Log: {ex}");
            }
        }

        /// <summary>
        /// Graba registro de log.
        /// </summary>
        private void EjecutarLog() {
            ISession session = null;

            try {
                using (session = SessionHelper.SessionFactory.OpenSession())
                using (var transaction = session.BeginTransaction()) {
                    session.Save(coreLog);
                    session.Flush();
                    transaction.Commit();
                }
            } catch (Exception ex) {
                log.LogError($"LogThread: {ex}");
            } finally {
                if (session != null && session.IsOpen) {
                    session.Close();
                }
            }

        }

    }
}
