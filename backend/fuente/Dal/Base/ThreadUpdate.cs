﻿using Modelo.Util;
using NHibernate;
using System.Collections.Generic;
using System.Threading;

namespace Dal.Base {

    /// <summary>
    /// Clase que permite realizar actualizaciones en hilo diferente de la transacción.
    /// </summary>
    public class ThreadUpdate {

        /// <summary>
        /// Actualiza el registro en la base de datos.
        /// </summary>
        public static void Update(IEntity entity) {
            Dictionary<string, object> map = new Dictionary<string, object> {
                ["bean"] = entity
            };
            ThreadUpdate upd = new ThreadUpdate();
            Thread thread = new Thread(upd.Execute);
            thread.Start(map);
        }

        /// <summary>
        /// Actuliza en la base de datos los datos del registro.
        /// </summary>
        public void Execute(object datos) {
            ISession session = null;
            try {

                using (session = SessionHelper.SessionFactory.OpenSession())
                using (var transaction = session.BeginTransaction()) {
                    Dictionary<string, object> map = (Dictionary<string, object>)datos;
                    IEntity entity = (IEntity)map["bean"];

                    session.Merge(entity);
                    session.Flush();
                    transaction.Commit();
                }

            } finally {
                if (session != null && session.IsOpen) {
                    session.Close();
                }
            }
        }

    }
}
