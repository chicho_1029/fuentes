﻿using FluentNHibernate.Cfg;
using Microsoft.Extensions.Logging;
using Modelo.Entities;
using NHibernate;
using NHibernate.Cfg;
using System;
using Util.Configuracion;
using Util.Request;

namespace Dal.Base {

    public class SessionHelper {

        private static readonly ILogger log = SettingsManager.GetLogger();
        private static ISessionFactory _sessionFactory;

        #region PROPIEDADES

        /// <summary>
        /// Manejo de session
        /// </summary>
        public static ISession GetSession() {
            return SessionCore.GetSession();
        }

        public static void SetSession(RequestBase request) {
            ISession session = SessionFactory.OpenSession();
            session.DefaultReadOnly = true;
            if (request != null) {
                request.Csession = Convert.ToString(session.GetSessionImplementation().SessionId);
            }

            SessionCore.SetSession(session, request);
        }

        public static void CloseSession() {
            SessionCore.CloseSession();
        }

        #endregion

        #region METODOS

        /// <summary>
        /// Método que crea la session a la base de datos
        /// </summary>
        public static ISessionFactory SessionFactory {
            get {
                try {
                    if (_sessionFactory == null) {

                        Connection _connection = SettingsManager.GetConnection();
                        string connectionString = string.Format(@"Server={0};Initial Catalog={1};User Id={2};Password={3}", _connection.Server, _connection.Base, _connection.User, _connection.Password);

                        Configuration configuration = new Configuration();
                        configuration.SetProperty(NHibernate.Cfg.Environment.ConnectionProvider, "NHibernate.Connection.DriverConnectionProvider");
                        configuration.SetProperty(NHibernate.Cfg.Environment.ConnectionDriver, "NHibernate.Driver.SqlClientDriver");
                        configuration.SetProperty(NHibernate.Cfg.Environment.Dialect, "NHibernate.Dialect.MsSql2012Dialect");
                        configuration.SetProperty(NHibernate.Cfg.Environment.ConnectionString, connectionString);
                        configuration.SetProperty(NHibernate.Cfg.Environment.ShowSql, "false");
                        configuration.SetProperty(NHibernate.Cfg.Environment.PrepareSql, "true");
                        configuration.SetProperty(NHibernate.Cfg.Environment.BatchSize, "100");

                        //Configuración de FluentNHibernate
                        FluentConfiguration f = Fluently.Configure(configuration);

                        //Mapeo de clases, con solo hacer una referencia a una clase nos mapeara todas las clases
                        f.Mappings(m => m.FluentMappings.AddFromAssemblyOf<Coreauditoria>());

                        //Session factory
                        _sessionFactory = f.BuildSessionFactory();
                    }

                    return _sessionFactory;
                } catch (Exception ex) {
                    log.LogError($"SessionHelper: {ex}");

                    return null;
                }
            }

        }

        #endregion
    }
}

