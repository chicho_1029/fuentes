﻿using Modelo.Entities;
using NHibernate;
using NHibernate.Persister.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using Util.Enumeracion;
using Util.General;
using Util.Request;

namespace Dal.Base {

    /// <summary>
    /// Calse utilitaria que se encarga del manejo de una conexion a la base de datos.
    /// </summary>
    public static class SessionCore {

        /// <summary>
        /// Thread local que almacena una conexion a la base de datos.
        /// </summary>
        [ThreadStatic]
        private static ISession _threadSession;

        /// <summary>
        /// Thread local que almacena la peticion de la sesion.
        /// </summary>
        [ThreadStatic]
        private static RequestBase _threadRequest;

        /// <summary>
        /// Thread local que almacena el listado de entidades de la base de datos.
        /// </summary>
        [ThreadStatic]
        private static IList<Coreentidad> _threadEntities;

        /// <summary>
        /// Fija una session en el thread local.
        /// </summary>
        public static void SetSession(ISession session, RequestBase request) {
            _threadSession = session;
            _threadRequest = request;
            _threadEntities = session.Query<Coreentidad>().ToList();
        }

        /// <summary>
        /// Obtiene y entrega una session del thread local.
        /// </summary>
        public static ISession GetSession() {
            ISession session = _threadSession;
            return ValidateSession(session);
        }

        /// <summary>
        /// Verifica que la session del thread local se encuentre abierta.
        /// </summary>
        private static ISession ValidateSession(ISession session) {
            if (session == null) {
                SessionHelper.SetSession(_threadRequest);
                return _threadSession;
            } else {
                return session;
            }
        }

        /// <summary>
        /// Cierra la conexion a la base de datos.
        /// </summary>
        public static void CloseSession() {
            ISession session = _threadSession;
            _threadSession = null;
            if (session != null && session.IsOpen) {
                session.Close();
                session.Dispose();
            }
        }

        /// <summary>
        /// Obtiene y entrega un request del thread local.
        /// </summary>
        public static RequestBase GetRequest() {
            return _threadRequest;
        }

        /// <summary>
        /// Guarda registro en la base de datos sin validacion y auditoria.
        /// </summary>
        public static T SaveChanges<T>(T obj) where T : class {
            _threadSession.Save(obj);
            return obj;
        }

        /// <summary>
        /// Actualiza registro en la base de datos sin validacion y auditoria.
        /// </summary>
        public static T UpdateChanges<T>(T obj) where T : class {
            _threadSession.Evict(obj);
            _threadSession.Update(obj);
            return obj;
        }

        /// <summary>
        /// Elimina registro en la base de datos sin validacion y auditoria.
        /// </summary>
        public static void DeleteChanges<T>(T obj) where T : class {
            _threadSession.Delete(_threadSession.Merge(obj));
        }

        /// <summary>
        /// Guarda registro en la base de datos.
        /// </summary>
        public static T Save<T>(T obj) where T : class {
            SetCreated(obj);

            _threadSession.Save(obj);

            SetAudit(EnumOperacion.INSERT, obj, null);
            return obj;
        }

        /// <summary>
        /// Actualiza registro en la base de datos.
        /// </summary>
        public static T Update<T>(T obj) where T : class {
            SetModified(obj);

            _threadSession.Evict(obj);
            _threadSession.Update(obj);

            SetAudit(EnumOperacion.UPDATE, obj, null);
            return obj;
        }

        /// <summary>
        /// Elimina registro en la base de datos.
        /// </summary>
        public static void Delete<T>(T obj) where T : class {
            AbstractEntityPersister entity = _threadSession.SessionFactory.GetClassMetadata(obj.GetType()) as AbstractEntityPersister;
            var removed = _threadSession.Get(obj.GetType().Name, entity.GetIdentifier(obj));

            _threadSession.Delete(removed);

            SetAudit(EnumOperacion.DELETE, removed, null);
        }

        /// <summary>
        /// Asigna valores de creacion al objeto.
        /// </summary>
        private static void SetCreated<T>(T obj) {
            if (_threadRequest == null) {
                return;
            }

            var u = obj.GetType().GetProperty(EnumCampos.USUARIO_CREACION);
            var f = obj.GetType().GetProperty(EnumCampos.FECHA_CREACION);

            if (u != null) obj.GetType().GetProperty(EnumCampos.USUARIO_CREACION).SetValue(obj, _threadRequest.Cusuario);
            if (f != null) obj.GetType().GetProperty(EnumCampos.FECHA_CREACION).SetValue(obj, DateTime.Now);
        }

        /// <summary>
        /// Asigna valores de modificacion al objeto.
        /// </summary>
        private static void SetModified<T>(T obj) {
            if (_threadRequest == null) {
                return;
            }

            var u = obj.GetType().GetProperty(EnumCampos.USUARIO_MODIFICACION);
            var f = obj.GetType().GetProperty(EnumCampos.FECHA_MODIFICACION);

            if (u != null) obj.GetType().GetProperty(EnumCampos.USUARIO_MODIFICACION).SetValue(obj, _threadRequest.Cusuario);
            if (f != null) obj.GetType().GetProperty(EnumCampos.FECHA_MODIFICACION).SetValue(obj, DateTime.Now);
        }

        /// <summary>
        /// Almacena de auditoria de objeto.
        /// </summary>
        private static void SetAudit(string type, object original, object modified) {
            if (_threadRequest == null) {
                return;
            }

            Coreentidad entity = GetEntity(original.GetType().Name);
            if (entity != null && entity.Manejaauditoria) {

                Coreauditoria auditoria = ConvertObject.SerializeDeserialize<Coreauditoria>(_threadRequest);
                auditoria.Centidad = entity.Centidad;
                auditoria.Freal = Fecha.GetFechaActual();
                auditoria.Tipo = type;
                auditoria.Roriginal = FormatObject.Objeto(original);
                auditoria.Rmodificado = FormatObject.Objeto(modified);

                _threadSession.Save(auditoria);
            }
        }

        /// <summary>
        /// Entrega datos de la entidad.
        /// </summary>
        private static Coreentidad GetEntity(string name) {
            return _threadEntities.FirstOrDefault(x => x.Nombre.ToLower() == name.ToLower());
        }

    }
}