﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Util.Excepcion;

namespace Util.Componentes {

    /// <summary>
    /// Clase utilitaria de consulta.
    /// </summary>
    public class Query {

        private string objeto;
        private string codigoentidad;
        private string metodo;
        private Dictionary<string, object> parametros;

        /// <summary>
        /// Map que contiene objetos necesarios para ejecutar una consulta.
        /// </summary>
        [JsonProperty(PropertyName = "o")]
        public string Objeto { get => objeto; set => objeto = value; }
        [JsonProperty(PropertyName = "a")]
        public string CodigoEntidad { get => codigoentidad; set => codigoentidad = value; }
        [JsonProperty(PropertyName = "m")]
        public string Metodo { get => metodo; set => metodo = value; }
        [JsonProperty(PropertyName = "p")]
        public Dictionary<string, object> Parametros { get => parametros; set => parametros = value; }

        /// <summary>
        /// Entrega un elemento string de datos.
        /// </summary>
        public string GetParametroString(string key) {
            ExistKey(key);
            string s = (string)parametros[key];
            return s;
        }

        private void ExistKey(string key) {
            if (!parametros.ContainsKey(key)) {
                throw new CoreExcepcion("4000", $"DATO NO EXISTE: {key}");
            }
        }
    }
}
