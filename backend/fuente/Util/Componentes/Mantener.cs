﻿using Newtonsoft.Json;
using System;
using Util.Excepcion;

namespace Util.Componentes {

    /// <summary>
    /// Clase utilitaria de mantenimiento.
    /// </summary>
    public class Mantener {

        public string OperacionTransaccion;
        private string operacion;
        private string codigoentidad;
        private string permiso;
        private object registro;
        private string insertar;
        private string actualizar;
        private string eliminar;

        /// <summary>
        /// Map que contiene objetos necesarios para ejecutar una consulta.
        /// </summary>
        [JsonProperty(PropertyName = "o")]
        public string Operacion { get => CompletarOperacion(); set => operacion = value; }
        [JsonProperty(PropertyName = "a")]
        public string CodigoEntidad { get => codigoentidad; set => codigoentidad = value; }
        [JsonProperty(PropertyName = "m")]
        public string Permiso { get => permiso; set => permiso = value; }
        [JsonProperty(PropertyName = "p")]
        public object Registro { get => registro; set => registro = value; }
        [JsonProperty(PropertyName = "n")]
        public string Insercion { get => insertar; set => insertar = value; }
        [JsonProperty(PropertyName = "c")]
        public string Actualizacion { get => actualizar; set => actualizar = value; }
        [JsonProperty(PropertyName = "e")]
        public string Eliminacion { get => eliminar; set => eliminar = value; }

        /// <summary>
        /// Metodo de validacion de operacion
        /// </summary>
        private string CompletarOperacion() {
            if (operacion != null) return operacion;

            if (insertar != null) operacion = insertar;
            if (actualizar != null) operacion = actualizar;
            if (eliminar != null) operacion = eliminar;
            OperacionTransaccion = operacion;

            int valor1 = Convert.ToInt16(operacion.Substring(2, 1));
            int valor2 = Convert.ToInt16(operacion.Substring(7, 1));
            if ((valor1 - valor2) < 0 || (valor1 - valor2) > 2) {
                throw new CoreExcepcion("1032", $"OPERACION INCORRECTA: {operacion}");
            }

            operacion = (valor1 - valor2).ToString();
            return operacion;
        }

    }
}
