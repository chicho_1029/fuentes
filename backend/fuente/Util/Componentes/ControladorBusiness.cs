﻿using Util.Request;

namespace Util.Componentes {

    /// <summary>
    /// Clase a extender por las consultas de negocio que se ejecutan a nivel de transaccion.
    /// </summary>
    public abstract class ClaseBusinessQuery {

        public abstract Response Ejecutar(RequestQuery rq);

    }


    /// <summary>
    /// Clase a extender por los mantenimients de negocio que se ejecutan a nivel de transaccion.
    /// </summary>
    public abstract class ClaseBusinessMantener {

        public abstract Response Ejecutar(RequestMantener rq);

    }

}
