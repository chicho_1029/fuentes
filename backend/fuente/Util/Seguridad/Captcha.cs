﻿using System.Net;
using Util.Excepcion;
using Util.Request;

namespace Util.Seguridad {

    public static class Captcha {

        public static void ValidateCaptcha(string response, string secret) {

            var client = new WebClient();
            var jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            CaptchaResponse validate = ConvertObject.Deserialize<CaptchaResponse>(jsonResult.ToString());
            if (!validate.Success) {
                throw new CoreExcepcion("1031", $"CAPTCHA INCORRECTO: {validate.ErrorMessage}");
            }

        }

    }

}
