﻿using System;

namespace Util.Seguridad {

    public static class Token {

        private static readonly Random getrandom = new Random();
        private static readonly object syncLock = new object();

        /// <summary>
        /// Entrega token.
        /// </summary>
        public static string GetToken() {
            return Password.Encriptar(Convert.ToString(General.Fecha.GetFechaActual().Ticks)).ToUpper();
        }

        /// <summary>
        /// Entrega codigo otp aleatorio.
        /// </summary>
        public static string GetOtp() {
            lock (syncLock) {
                return getrandom.Next(100000, 999999).ToString();
            }
        }

    }

}
