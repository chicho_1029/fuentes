﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Util.Seguridad {

    public static class Password {

        /// <summary>
        /// Entrega un string encriptado.
        /// </summary>
        public static string Encriptar(string password) {
            SHA1 sha1 = new SHA1CryptoServiceProvider();

            return BitConverter.ToString(sha1.ComputeHash(Encoding.UTF8.GetBytes(password))).ToLower().Replace("-", string.Empty);
        }

    }

}
