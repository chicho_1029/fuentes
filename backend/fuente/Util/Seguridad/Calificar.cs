﻿using System;

namespace Util.Seguridad {

    public static class Calificar {

        public static string Encriptar(string cadena) {
            return Guid.NewGuid().ToString("N").Substring(0, 6) + "Av" + cadena;
        }

        public static string DesEncriptar(string cadena) {
            string split = cadena.Split("Av")[1].ToString();
            return cadena.Substring(8, split.Length);
        }

    }

}