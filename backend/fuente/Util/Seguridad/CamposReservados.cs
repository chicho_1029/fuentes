﻿using System;
using System.Collections.Generic;
using System.Linq;
using Util.Enumeracion;
using Util.Request;

namespace Util.Seguridad {

    /// <summary>
    /// Clase que permite la verificacion de campos reservados
    /// </summary>
    public static class CamposReservados {

        public static object RemoverCamposReservados(object result) {
            if (result is null || result is string || result is decimal || result is int || result is long || result is bool || result is DateTime) {
                return result;
            } else {
                Dictionary<string, object> obj = ConvertObject.SerializeDeserialize<Dictionary<string, object>>(result);
                obj.Keys.Intersect(EnumCampos.GetCampos).ToList().ForEach(key => obj.Remove(key));
                return obj;
            }
        }

        public static IList<Dictionary<string, object>> RemoverCamposReservados(IList<object> result) {
            IList<Dictionary<string, object>> objresult = ConvertObject.SerializeDeserialize<IList<Dictionary<string, object>>>(result);
            IList<Dictionary<string, object>> obj = new List<Dictionary<string, object>>();
            foreach (var r in objresult) obj.Add(r.Where(x => !EnumCampos.GetCampos.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value));
            return obj;
        }

        public static IList<Dictionary<string, object>> RemoverCamposReservados(IEnumerable<object> result) {
            IList<Dictionary<string, object>> objresult = ConvertObject.SerializeDeserialize<IList<Dictionary<string, object>>>(result);
            IList<Dictionary<string, object>> obj = new List<Dictionary<string, object>>();
            foreach (var r in objresult) obj.Add(r.Where(x => !EnumCampos.GetCampos.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value));
            return obj;
        }
    }
}
