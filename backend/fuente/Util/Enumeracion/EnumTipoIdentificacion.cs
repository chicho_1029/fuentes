﻿namespace Util.Enumeracion {

    /// <summary>
    /// Enumeracion que almacena el tipo de identificacion.
    /// </summary>
    public struct EnumTipoIdentificacion {

        public static string CEDULA => "C";
        public static string RUC => "R";
        public static string PASAPORTE => "P";

    }

}


