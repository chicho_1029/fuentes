﻿namespace Util.Enumeracion {

    /// <summary>
    /// Estructura que almacena mensajes de error.
    /// </summary>
    public struct EnumMensaje {

        public static string ERROR => "999999";
        public static string ERROR_CORE => "2004";

    }

}


