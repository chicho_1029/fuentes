﻿namespace Util.Enumeracion {

    /// <summary>
    /// Enumeracion que almacena la operacion de mantenimiento a ejecutar.
    /// </summary>
    public struct EnumOperacion {

        public const string INSERT = "0";
        public const string UPDATE = "1";
        public const string DELETE = "2";

    }

}


