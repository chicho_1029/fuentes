﻿namespace Util.Enumeracion {

    /// <summary>
    /// Enumeracion que almacena el tipo de clase a ejecutar.
    /// </summary>
    public struct EnumTipoClase {

        public static string CONSULTA => "G";
        public static string MANTENER => "M";
        public static string FAVORITOS => "F";
        public static string PUBLICO => "P";
        public static string CONSULTA_NEGOCIO => "C";
        public static string MANTENER_NEGOCIO => "B";

    }

}


