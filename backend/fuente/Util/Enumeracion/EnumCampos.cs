﻿using System.Collections.Generic;

namespace Util.Enumeracion {

    /// <summary>
    /// Estructura que almacena los campos del sistema.
    /// </summary>
    public struct EnumCampos {

        public static string USUARIO_CREACION => "Cusuariocre";
        public static string FECHA_CREACION => "Fcreacion";
        public static string USUARIO_MODIFICACION => "Cusuariomod";
        public static string FECHA_MODIFICACION => "Fmodificacion";
        public static string PASSWORD => "Password";
        public static string CLAVE => "Clave";
        public static string FECHA_SISTEMA => "Fsistema";
        public static string FECHA_REAL => "Freal";
        public static string TOKEN => "Token";
        public static string TERMINAL => "Terminal";
        public static string TERMINAL_LOCAL => "Terminallocal";
        public static string CODIGO_CLASE => "Cclase";


        /// <summary>
        /// Entrega el listado de campos.
        /// </summary>
        public static IEnumerable<string> GetCampos {
            get {
                yield return USUARIO_CREACION;
                yield return FECHA_CREACION;
                yield return USUARIO_MODIFICACION;
                yield return FECHA_MODIFICACION;
                yield return PASSWORD;
                yield return CLAVE;
                yield return FECHA_SISTEMA;
                yield return FECHA_REAL;
                yield return TOKEN;
                yield return TERMINAL;
                yield return TERMINAL_LOCAL;
                yield return CODIGO_CLASE;
            }
        }

    }

}


