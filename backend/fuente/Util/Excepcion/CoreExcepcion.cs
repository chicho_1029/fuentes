﻿using System;

namespace Util.Excepcion {

    /// <summary>
    /// Clase utilitaria del manejo de excepciones de la aplicación
    /// </summary>
    [Serializable]
    public class CoreExcepcion : Exception {

        public string Codigo { get; set; }
        public object[] Parametros { get; set; }

        public CoreExcepcion(string codigo, string mensaje) : base(mensaje) {
            Codigo = codigo;
        }

        public CoreExcepcion(string codigo, string mensaje, params object[] parametros) : base(mensaje) {
            Codigo = codigo;
            Parametros = parametros;
        }

        public CoreExcepcion(string codigo, string mensaje, Exception inner, params object[] parametros) : base(mensaje, inner) {
            Codigo = codigo;
            Parametros = parametros;
        }

    }

}