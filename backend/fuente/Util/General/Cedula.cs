﻿using System;

namespace Util.General {

    /// <summary>
    /// Clase que se encarga de validar una cedula.
    /// </summary>
    public static class Cedula {

        public static bool Validar(string cedula) {
            int digitos = 10;
            bool cedulaCorrecta = false;
            if (cedula == null || cedula.Length != digitos) {
                return cedulaCorrecta;
            }
            try {
                int tercerDigito = Convert.ToInt32(cedula.Substring(2, 1));
                if (tercerDigito < 6) {
                    // Coeficientes de validación cédula
                    // El decimo digito se lo considera dígito verificador
                    int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
                    int verificador = Convert.ToInt32(cedula.Substring(9, 1));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < cedula.Length - 1; i++) {
                        digito = Convert.ToInt32(cedula.Substring(i, 1)) * coefValCedula[i];
                        suma += digito % 10 + digito / 10;
                    }

                    if (suma % 10 == 0 && suma % 10 == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        if (10 - suma % 10 == verificador) {
                            cedulaCorrecta = true;
                        }
                    }
                }
            } catch (Exception) {
                cedulaCorrecta = false;
            }
            return cedulaCorrecta;
        }
    }
}
