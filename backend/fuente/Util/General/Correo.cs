﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Util.General {

    /// <summary>
    /// Clase que se encarga de validar el correo electronico.
    /// </summary>
    public static class Correo {

        public static bool Validar(string correo) {
            bool correoCorrecto = false;

            if (!string.IsNullOrEmpty(correo)) {
                try {
                    correoCorrecto = new EmailAddressAttribute().IsValid(correo.Trim());
                } catch (FormatException) {
                    correoCorrecto = false;
                }
            }
            return correoCorrecto;
        }

    }

}
