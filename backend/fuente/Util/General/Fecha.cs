﻿using System;
using System.Globalization;
using System.Threading;
using Util.Excepcion;

namespace Util.General {

    public static class Fecha {

        /// <summary>
        /// Entrega la fecha actual.
        /// </summary>
        public static DateTime GetFechaActual() {
            return DateTime.Now;
        }

        /// <summary>
        /// Entrega la fecha del sistema / fecha proceso.
        /// </summary>
        public static DateTime GetFecha(int fecha) {
            return new DateTime(GetAnio(fecha), GetMes(fecha), GetDia(fecha));
        }

        /// <summary>
        /// Entrega el anio en formato entero.
        /// </summary>
        private static int GetAnio(int fecha) {
            return Convert.ToInt32(fecha.ToString().Substring(0, 4));
        }

        /// <summary>
        /// Entrega el mes en formato entero.
        /// </summary>
        private static int GetMes(int fecha) {
            string mes = fecha.ToString().Substring(4, 2);
            if (mes.Substring(0, 1).Equals("0")) {
                mes = mes.Substring(1, 1);
            }
            return Convert.ToInt32(mes);
        }

        /// <summary>
        /// Entrega el dia en formato entero.
        /// </summary>
        public static int GetDia(int fecha) {
            return Convert.ToInt32(fecha.ToString().Substring(6, 2));
        }

        /// <summary>
        /// Entrega el periodo de una fecha.
        /// </summary>
        public static int GetPeriodo(int fecha) {
            return Convert.ToInt32(GetAnio(fecha) + fecha.ToString().Substring(4, 2));
        }

        /// <summary>
        /// Entrega la hora en formato entero.
        /// </summary>
        public static int GetHora(DateTime fecha) {
            return Convert.ToInt32(fecha.Hour);
        }

        /// <summary>
        /// Entrega en formato YYYYMMDD dado una fecha. 
        /// </summary>
        public static int GetFechaEntero(DateTime fecha) {
            string anio = fecha.Year.ToString();
            string mes = fecha.Month.ToString();
            string dia = fecha.Day.ToString();
            return Convert.ToInt32(anio + ((mes.Length == 1) ? +0 + mes : mes) + ((dia.Length == 1) ? +0 + dia : dia));
        }

        /// <summary>
        /// Entrega fecha actual en formato YYYYMMDD. 
        /// </summary>
        public static int GetFechaActualEntero() {
            DateTime fecha = GetFechaActual();
            string anio = fecha.Year.ToString();
            string mes = fecha.Month.ToString();
            string dia = fecha.Day.ToString();
            return Convert.ToInt32(anio + ((mes.Length == 1) ? +0 + mes : mes) + ((dia.Length == 1) ? +0 + dia : dia));
        }

        /// <summary>
        /// Entrega fecha en formato MM/DD/YYYY. 
        /// </summary>
        public static string GetFechaCadena(DateTime fecha) {
            string anio = fecha.Year.ToString();
            string mes = fecha.Month.ToString();
            string dia = fecha.Day.ToString();
            return string.Join("/", ((mes.Length == 1) ? +0 + mes : mes), ((dia.Length == 1) ? +0 + dia : dia), anio);
        }

        /// <summary>
        /// Entrega fecha en formato MM/DD/YYYY HH:MM:SS. 
        /// </summary>
        public static string GetFechaCompleta(DateTime fecha) {
            string hora = string.Join(":", fecha.Hour, fecha.Minute, fecha.Second);
            return string.Join(" ", GetFechaCadena(fecha), hora);
        }

        /// <summary>
        /// Entrega la hora actual en formato hhmmss.
        /// </summary>
        public static long GetHoraEntero() {
            DateTime fecha = DateTime.Now;
            return Convert.ToInt64(string.Concat(fecha.Hour, fecha.Minute, fecha.Second));
        }

        /// <summary>
        /// Entrega fecha en formato HH:MM:SS. 
        /// </summary>
        public static string GetHoraCompleta(DateTime fecha) {
            string hora = fecha.Hour.ToString();
            string minuto = fecha.Minute.ToString();
            string segundo = fecha.Second.ToString();
            return string.Join(":", (hora.Length == 1) ? +0 + hora : hora, (minuto.Length == 1) ? +0 + minuto : minuto, (segundo.Length == 1) ? +0 + segundo : segundo);
        }

        /// <summary>
        /// Adiciona dias calendario una fecha.
        /// </summary>
        public static DateTime AddDias(DateTime fecha, int dias) {
            return fecha.AddDays(dias);
        }

        /// <summary>
        /// Adiciona dias calendario una fecha.
        /// </summary>
        public static DateTime AddDias(int fecha, int dias) {
            DateTime dt = GetFecha(fecha);
            return dt.AddDays(dias);
        }

        /// <summary>
        /// Adiciona meses calendario una fecha.
        /// </summary>
        public static DateTime AddMeses(int fecha, int meses) {
            DateTime dt = GetFecha(fecha);
            return dt.AddMonths(meses);
        }

        /// <summary>
        /// Retorna fecha fin de mes de una fecha.
        /// </summary>
        public static DateTime GetFinMes(DateTime fecha) {
            DateTime firstDayOfTheMonth = new DateTime(fecha.Year, fecha.Month, 1);
            return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// Retorna fecha incial de mes de una fecha.
        /// </summary>
        public static DateTime GetInicioMes(DateTime fecha) {
            return new DateTime(fecha.Year, fecha.Month, 1);
        }

        /// <summary>
        /// Retorna fecha incial de mes de una fecha.
        /// </summary>
        public static DateTime GetCobroMes(int fechainicio, int mes, int diacobro) {
            DateTime fsumames = AddMeses(fechainicio, mes);
            DateTime ffinmes = GetFinMes(fsumames);
            DateTime fcobro = new DateTime();
            bool cambiofecha = false;

            if (diacobro >= ffinmes.Day) {
                fcobro = ffinmes;
                cambiofecha = true;
            }

            if (!cambiofecha) {
                fcobro = new DateTime(fsumames.Year, fsumames.Month, diacobro);
            }
            return fcobro;
        }

        /// <summary>
        /// Retorna fecha inicio y fin del mes.
        /// </summary>
        public static void GetFechasMes(int fecha, out int iniciomes, out int finmes) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-PE");
            DateTime dt = GetFecha(fecha);
            DateTime finiciomes = new DateTime(dt.Year, dt.Month, 1);
            DateTime ffinmes = finiciomes.AddMonths(1).AddDays(-1);
            iniciomes = GetFechaEntero(finiciomes);
            finmes = GetFechaEntero(ffinmes);
        }

        /// <summary>
        /// Retorna fecha inicio y fin de la semana.
        /// </summary>
        public static void GetFechasSemana(int fecha, out int lunes, out int domingo) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-PE");
            DateTime dt = GetFecha(fecha);
            int semana = GetNumeroSemana(dt);
            DateTime fsemana = GetPrimerDiaSemana(semana == 52 ? dt.AddYears(-1).Year : dt.Year, semana, CultureInfo.CurrentCulture);

            if (DayOfWeek.Monday == CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(fsemana)) {
                lunes = GetFechaEntero(fsemana);
                domingo = GetFechaEntero(fsemana.AddDays(6));
            } else {
                lunes = GetFechaEntero(fsemana.AddDays(1));
                domingo = GetFechaEntero(fsemana.AddDays(7));
            }
        }

        /// <summary>
        /// Retorna fecha inicio y fin de la semana.
        /// </summary>
        public static int GetNumeroSemana(DateTime fecha) {
            DayOfWeek dia = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(fecha);
            if (dia >= DayOfWeek.Monday && dia <= DayOfWeek.Wednesday) {
                fecha = fecha.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(fecha, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// Retorna fecha inicio y fin de la semana.
        /// </summary>
        public static DateTime GetPrimerDiaSemana(int anio, int semana, CultureInfo ci) {
            DateTime anio1 = new DateTime(anio, 1, 1);
            int daysOffset = (int)ci.DateTimeFormat.FirstDayOfWeek - (int)anio1.DayOfWeek;
            DateTime dia1 = anio1.AddDays(daysOffset);
            int semana1 = ci.Calendar.GetWeekOfYear(anio1, ci.DateTimeFormat.CalendarWeekRule, ci.DateTimeFormat.FirstDayOfWeek);
            if ((semana1 <= 1 || semana1 >= 52) && daysOffset >= -3) {
                semana -= 1;
            }
            return dia1.AddDays(semana * 7);
        }

        public static String GetTimestampString(DateTime fecha) {
            return fecha.ToString("yyyyMMddHHmmss");
        }

        public static Int64 GetTimestampLong(DateTime fecha) {
            return Convert.ToInt64(fecha.ToString("yyyyMMddHHmmss"));
        }

        /// <summary>
        /// Entrega la fecha actual sumado los minutos.
        /// </summary>
        public static DateTime GetFechaCaducidad(int minutos) {
            return DateTime.Now.AddMinutes(minutos);
        }

        /// <summary>
        /// Metodo que Resta fechas considerando dias calendario, o dias del anio 365.
        /// </summary>
        public static short Resta365(int fechafinal, int fechainicial) {
            if (fechafinal < fechainicial) {
                throw new CoreExcepcion("SER-003", "FECHA A RESTAR: {0} NO PUEDE SER MAYOR A: {1}", fechainicial, fechafinal);
            }
            DateTime ffinal = Fecha.GetFecha(fechafinal);
            DateTime finicial = Fecha.GetFecha(fechainicial);
            return Fecha.Resta365(ffinal, finicial);
        }

        /// <summary>
        /// Metodo que Resta fechas considerando dias calendario, o dias del anio 365.
        /// </summary>
        public static short Resta365(DateTime ffinal, DateTime finicial) {
            int anios = ffinal.Year - finicial.Year;
            if (anios == 0) {
                return Convert.ToInt16(ffinal.DayOfYear - finicial.DayOfYear);
            }
            DateTime fechatemporal = finicial;
            int dias = 0;
            for (int i = 0; i <= anios; i++) {
                int diasAnio = (DateTime.IsLeapYear(fechatemporal.Year)) ? 366 : 365;
                if (i == 0) {
                    dias = dias + (diasAnio - finicial.DayOfYear);
                } else if (i == anios) {
                    dias = dias + ffinal.DayOfYear;
                } else {
                    dias = dias + diasAnio;
                }
                fechatemporal = fechatemporal.AddYears(1);
            }
            return Convert.ToInt16(dias);
        }

    }

}
