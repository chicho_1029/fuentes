﻿namespace Util.Configuracion {

    public class AppSettings {
        public string UrlApi { get; set; }
        public Connection Connection { get; set; }
        public AuthorizationBasic AuthorizationBasic { get; set; }
        public AlianzaValle AlianzaValle { get; set; }
        public NotificationService NotificationService { get; set; }
        public Public Public { get; set; }
    }

    public class Connection {
        public string Server { get; set; }
        public string Base { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }

    public class AuthorizationBasic {
        public string UsrWebAPI { get; set; }
        public string PwdWebAPI { get; set; }
        public string UsrWebCORE { get; set; }
        public string PwdWebCORE { get; set; }
        public string UsrWebPUBLIC { get; set; }
        public string PwdWebPUBLIC { get; set; }
    }

    public class AlianzaValle {
        public string ServerCentralIn { get; set; }
        public Biometrika Biometrika { get; set; }
    }

    public class Biometrika {
        public string Token { get; set; }
        public string Nivel { get; set; }
    }

    public class NotificationService {
        public string HoraInicio { get; set; }
        public string HoraFin { get; set; }
        public string MinutosIntervalo { get; set; }
        public string Usuario { get; set; }
    }

    public class Public {
        public string Canal { get; set; }
        public string CaptchaKey { get; set; }
        public string FilesPath { get; set; }
        public string FilesUrl { get; set; }
        public Facebook Facebook { get; set; }
        public Instagram Instagram { get; set; }
        public Outlook Outlook { get; set; }
        public Whatsapp Whatsapp { get; set; }
    }

    public class Facebook {
        public string Usuario { get; set; }
        public string ApiToken { get; set; }
        public string IdReceptor { get; set; }
        public string FacebookUrl { get; set; }
        public string FacebookToken { get; set; }
        public string FacebookPageToken { get; set; }
    }

    public class Instagram {
        public string Usuario { get; set; }
        public string UsuarioInstagram { get; set; }
        public string ApiToken { get; set; }
        public string InstagramToken { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }

    public class Outlook {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string TenantId { get; set; }
        public string Instance { get; set; }
        public string GraphResource { get; set; }
        public string Address { get; set; }
        public string GraphResourceEndPoint { get; set; }
    }

    public class Whatsapp {
        public string Usuario { get; set; }
        public string UserAnalytics { get; set; }
        public string KeyApi { get; set; }
    }
}
