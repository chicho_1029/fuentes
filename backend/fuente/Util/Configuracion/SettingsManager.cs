﻿using Microsoft.Extensions.Logging;
using System.IO;

namespace Util.Configuracion {

    public class SettingsManager {

        private static AppSettings _settings;

        private static ILogger _logger;

        /// <summary>
        /// Fija la configuracion en el thread local.
        /// </summary>
        public static void SetConfiguration(AppSettings settings, ILogger logger) {
            if (_settings == null) {
                _settings = settings;
                _logger = logger;
            }
        }

        /// <summary>
        /// Obtiene y entrega una instancia de la configuracion.
        /// </summary>
        public static AppSettings GetConfiguration() {
            return _settings;
        }

        /// <summary>
        /// Obtiene y entrega una instancia de la configuracion.
        /// </summary>
        public static ILogger GetLogger() {
            return _logger;
        }

        /// <summary>
        /// Obtiene y entrega el directorio de ejecucion.
        /// </summary>
        public static string GetLocation() {
            return Directory.GetCurrentDirectory();
        }

        /// <summary>
        /// Obtiene y entrega una instancia de la conexion.
        /// </summary>
        public static Connection GetConnection() {
            return _settings.Connection;
        }

        /// <summary>
        /// Obtiene y entrega una instancia de Alianza del Valle.
        /// </summary>
        public static AlianzaValle GetAlianzaValle() {
            return _settings.AlianzaValle;
        }

    }
}
