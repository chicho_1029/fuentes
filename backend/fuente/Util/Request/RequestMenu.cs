﻿using Newtonsoft.Json;

namespace Util.Request {

    public class RequestMenu : RequestBase {

        /// <summary>
        /// Indicador de sesion iniciada. FALSE: Menú desde login / TRUE: Menú desde el sistema F5
        /// </summary>
        private bool sesioniniciada;

        [JsonProperty(PropertyName = "sesion")]
        public bool SesionIniciada { get => sesioniniciada; set => sesioniniciada = value; }

    }

}
