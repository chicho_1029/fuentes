﻿using System.Text.RegularExpressions;

namespace Util.Request {

    public class FormatObject {

        private static readonly string CARACTERES = "[@&'(\\s)?!<>#^_$%.,{}\\-\"\\/]";
        private static readonly string LOG = "[&'(\\s)?<>^,{}\\-\"\\/]";

        public static string Texto(string texto) {
            if (!string.IsNullOrEmpty(texto)) {
                try {
                    string txtjson = Regex.Replace(Regex.Replace(texto, "[\\s|]", string.Empty), @"\\r\\n", "|");
                    texto = Regex.Replace(Regex.Replace(Regex.Replace(txtjson, ",{", "||"), ",\"", "|"), CARACTERES, string.Empty);
                } catch {
                    return texto;
                }
            }
            return texto;
        }

        public static string Objeto(object objeto) {
            string texto = ConvertObject.Serialize(objeto);
            if (!string.IsNullOrEmpty(texto)) {
                try {
                    texto = Regex.Unescape(texto);
                    string txtjson = Regex.Replace(Regex.Replace(texto, "[\\s|]", string.Empty), @"\\r\\n", "|");
                    texto = Regex.Replace(Regex.Replace(Regex.Replace(txtjson, ",{", "||"), ",\"", "|"), CARACTERES, string.Empty);
                } catch {
                    return texto;
                }
            }
            return texto;
        }

        public static string Log(object objeto) {
            string texto = ConvertObject.Serialize(objeto);
            if (!string.IsNullOrEmpty(texto)) {
                try {
                    texto = Regex.Unescape(texto);
                    string txtjson = Regex.Replace(texto, @"\\r\\n", "|");
                    texto = Regex.Replace(Regex.Replace(Regex.Replace(txtjson, ",{", "||"), ",\"", "|"), LOG, string.Empty);
                } catch {
                    return texto;
                }
            }
            return texto;
        }

        public static string LogTexto(string texto) {
            if (!string.IsNullOrEmpty(texto)) {
                try {
                    texto = Regex.Replace(texto, LOG, string.Empty);
                } catch {
                    return texto;
                }
            }
            return texto;
        }

    }

}
