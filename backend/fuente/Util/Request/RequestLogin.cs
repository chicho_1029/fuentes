﻿using Newtonsoft.Json;

namespace Util.Request {

    public class RequestLogin : RequestBase {

        /// <summary>
        /// Clave de usuario
        /// </summary>
        private string password;

        /// <summary>
        /// Tiempo de sesion
        /// </summary>
        private int tiemposesion;

        [JsonProperty(PropertyName = "password")]
        public string Password { get => password; set => password = value; }

        [JsonProperty(PropertyName = "tiemposesion")]
        public int TiempoSesion { get => tiemposesion; set => tiemposesion = value; }

    }

}
