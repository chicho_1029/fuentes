﻿using System;
using System.Collections.Generic;

namespace Util.Request {

    public class Radicacion {

        /// <summary>
        /// Codigo de usuario.
        /// </summary>
        public string Cusuario { get; set; }

        /// <summary>
        /// Token del sistema
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Codigo del canal
        /// </summary>
        public string Ccanal { get; set; }

        /// <summary>
        /// Codigo del Modulo login.
        /// </summary>
        public short? Cmodulologin { get; set; }

        /// <summary>
        /// Fecha del sistema
        /// </summary>
        public int Fsistema { get; set; }

        /// <summary>
        /// Codigo de terminal
        /// </summary>
        public string Terminal { get; set; }

        /// <summary>
        /// Codigo de terminal local
        /// </summary>
        public string TerminalLocal { get; set; }

        /// <summary>
        /// Navegador
        /// </summary>
        public string Navegador { get; set; }

        /// <summary>
        /// Lista de modulos
        /// </summary>
        public List<RadicacionModulo> Lmodulos { get; set; } = new List<RadicacionModulo>();

    }

    public class RadicacionLogin {

        /// <summary>
        /// Codigo de usuario.
        /// </summary>
        public string Cusuario { get; set; }

        /// <summary>
        /// Token del sistema
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Lista de modulos
        /// </summary>
        public List<RadicacionModulo> Lmodulos { get; set; } = new List<RadicacionModulo>();

    }

    public class RadicacionSesion {

        /// <summary>
        /// Identificacion de usuario.
        /// </summary>
        public string IdentificacionUsuario { get; set; }

        /// <summary>
        /// Nombre de usuario.
        /// </summary>
        public string NombreUsuario { get; set; }

        /// <summary>
        /// Correo de usuario.
        /// </summary>
        public string CorreoUsuario { get; set; }

        /// <summary>
        /// Fecha del sistema
        /// </summary>
        public int FechaSistema { get; set; }

        /// <summary>
        /// Fecha del ultimo ingreso
        /// </summary>
        public DateTime FechaUltimoIngreso { get; set; }

        /// <summary>
        /// Timeoutminutos
        /// </summary>
        public short TiempoSesion { get; set; }

        /// <summary>
        /// Nombre de oficina
        /// </summary>
        public string NombreOficina { get; set; }

    }

    public class RadicacionModulo {

        /// <summary>
        /// Codigo de modulo login.
        /// </summary>
        public short Cmodulologin { get; set; }

        /// <summary>
        /// Icono del modulo.
        /// </summary>
        public string Icono { get; set; }

        /// <summary>
        /// Nombre del modulo.
        /// </summary>
        public string Nombre { get; set; }

        public IList<RadicacionRol> lroles { get; set; } = new List<RadicacionRol>();

    }

    public class RadicacionRol {
        /// <summary>
        /// Codigo del rol
        /// </summary>
        public short Crol { get; set; }
        /// <summary>
        /// Nombre del rol
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Codigo del perfil
        /// </summary>
        public int? Cperfil { get; set; }
    }

}
