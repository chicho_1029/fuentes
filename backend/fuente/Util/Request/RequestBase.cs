﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Util.Excepcion;
using Util.Seguridad;

namespace Util.Request {

    public class RequestBase {

        /// <summary>
        /// Codigo de sesion de conexion.
        /// </summary>
        private string csession;

        /// <summary>
        /// Codigo de clase de negocio de transaccion.
        /// </summary>
        private string cclase;

        /// <summary>
        /// Numero de token que se asocia a una transaccion.
        /// </summary>
        private string token;

        /// <summary>
        /// Codigo de usuario que ejecuta una transaccion.
        /// </summary>
        private string cusuario;

        /// <summary>
        /// Codigo del modulo login que ejecuta una transaccion.
        /// </summary>
        private short cmodulologin;

        /// <summary>
        /// Codigo del modulo al que pertenece la transaccion.
        /// </summary>
        private short cmodulo;

        /// <summary>
        /// Codigo de transaccion a ejecutar.
        /// </summary>
        private int ctransaccion;

        /// <summary>
        /// Codigo de terminal asociado al ip de la maquina desde la cual se ejecuta una transaccion.
        /// </summary>
        private string terminal;

        /// <summary>
        /// Codigo de terminal local asociado al ip de la maquina desde la cual se ejecuta una transaccion.
        /// </summary>
        private string terminallocal;

        /// <summary>
        /// Codigo del navegador
        /// </summary>
        private string navegador;

        /// <summary>
        /// Codigo de canal desde el cual se ejecuta una transaccion.
        /// </summary>
        private string ccanal;

        /// <summary>
        /// Fecha real que se ejecuta la transaccion.
        /// </summary>
        private DateTime freal;

        /// <summary>
        /// Fecha del sistema que se ejecuta la transaccion.
        /// </summary>
        private int fsistema;

        /// <summary>
        /// Diccionario que contiene datos adicionales para la transaccion.
        /// </summary>
        private Dictionary<string, object> datos = new Dictionary<string, object>();

        /// <summary>
        /// Objeto que contiene la respuesta de ejecucion de un servicio.
        /// </summary>
        private Response response = new Response();


        [JsonProperty(PropertyName = "csession")]
        public string Csession { get => csession; set => csession = value; }

        [JsonProperty(PropertyName = "cclase")]
        public string Cclase { get => cclase; set => cclase = value; }

        [JsonProperty(PropertyName = "token")]
        public string Token { get => token; set => token = value; }

        [JsonProperty(PropertyName = "cusuario")]
        public string Cusuario { get => cusuario; set => cusuario = value; }

        [JsonProperty(PropertyName = "cmodulologin")]
        public short Cmodulologin { get => cmodulologin; set => cmodulologin = value; }

        [JsonProperty(PropertyName = "cmodulo")]
        public short Cmodulo { get => cmodulo; set => cmodulo = value; }

        [JsonProperty(PropertyName = "ctransaccion")]
        public int Ctransaccion { get => ctransaccion; set => ctransaccion = value; }

        [JsonProperty(PropertyName = "terminal")]
        public string Terminal { get => terminal; set => terminal = value; }

        [JsonProperty(PropertyName = "terminallocal")]
        public string Terminallocal { get => terminallocal; set => terminallocal = value; }

        [JsonProperty(PropertyName = "navegador")]
        public string Navegador { get => navegador; set => navegador = value; }

        [JsonProperty(PropertyName = "ccanal")]
        public string Ccanal { get => ccanal; set => ccanal = value; }

        [JsonProperty(PropertyName = "freal")]
        public DateTime Freal { get => freal; set => freal = value; }

        [JsonProperty(PropertyName = "fsistema")]
        public int Fsistema { get => fsistema; set => fsistema = value; }

        [JsonProperty(PropertyName = "datos")]
        public Dictionary<string, object> Datos { get => datos; set => datos = value; }

        [JsonProperty(PropertyName = "response")]
        public Response Response { get => response; set => response = value; }


        /// <summary>
        /// Entrega canal.
        /// </summary>
        public string GetCanal() {
            return ccanal;
        }

        /// <summary>
        /// Asigna canal.
        /// </summary>
        public void SetCanal(string canal) {
            ccanal = canal;
        }

        /// <summary>
        /// Entrega usuario.
        /// </summary>
        public string GetUsuario() {
            return cusuario;
        }

        /// <summary>
        /// Asigna usuario.
        /// </summary>
        public void SetUsuario(string usuario) {
            cusuario = usuario;
        }

        /// <summary>
        /// Adiciona un elemento a datos.
        /// </summary>
        public void AddDatos(string key, object value) {
            datos[key] = value;
        }

        /// <summary>
        /// Elimina un elemento de datos.
        /// </summary>
        public void RemoveDatos(string key) {
            datos.Remove(key);
        }

        /// <summary>
        /// Entrega un objeto de datos.
        /// </summary>
        public object GetDatosObject(string key) {
            ExistKey(key);
            object o = datos[key];
            return o;
        }

        /// <summary>
        /// Entrega un elemento string de datos.
        /// </summary>
        public string GetDatosString(string key) {
            ExistKey(key);
            return Convert.ToString(datos[key]);
        }

        /// <summary>
        /// Entrega un elemento DateTime de datos.
        /// </summary>
        public DateTime GetDatosDate(string key) {
            ExistKey(key);
            return Convert.ToDateTime(datos[key]);
        }

        /// <summary>
        /// Entrega un elemento int de datos.
        /// </summary>
        public short GetDatosShort(string key) {
            ExistKey(key);
            return Convert.ToInt16(datos[key]);
        }

        /// <summary>
        /// Entrega un elemento int de datos.
        /// </summary>
        public int GetDatosInt(string key) {
            ExistKey(key);
            return Convert.ToInt32(datos[key]);
        }

        /// <summary>
        /// Entrega un elemento long de datos.
        /// </summary>
        public long GetDatosLong(string key) {
            ExistKey(key);
            return Convert.ToInt64(datos[key]);
        }

        /// <summary>
        /// Entrega un elemento decimal de datos.
        /// </summary>
        public decimal GetDatosDecimal(string key) {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("es-PE");
            ExistKey(key);
            return Convert.ToDecimal(datos[key]);
        }

        /// <summary>
        /// Entrega un elemento bool de datos.
        /// </summary>
        public bool GetDatosBool(string key) {
            if (!datos.ContainsKey(key)) {
                return false;
            }
            return Convert.ToBoolean(datos[key]);
        }

        private void ExistKey(string key) {
            if (!datos.ContainsKey(key)) {
                throw new CoreExcepcion("4000", $"DATO NO EXISTE: {key}");
            }
        }

        /// <summary>
        /// Adiciona un elemento al response.
        /// </summary>
        public void AddResponse(string key, object value) {
            response[key] = CamposReservados.RemoverCamposReservados(value);
        }

        public void AddResponse(string key, IList<object> value) {
            response[key] = CamposReservados.RemoverCamposReservados(value);
        }

        public void AddResponse(string key, IEnumerable<object> value) {
            response[key] = CamposReservados.RemoverCamposReservados(value);
        }

    }

}
