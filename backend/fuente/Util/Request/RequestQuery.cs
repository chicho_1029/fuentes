﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Util.Componentes;

namespace Util.Request {

    public class RequestQuery : RequestBase {

        /// <summary>
        /// Nombre del recurso de ejecución.
        /// </summary>
        private readonly string recurso = "Consultar";

        /// <summary>
        /// Lista de objetos necesarios para ejecutar una consulta.
        /// </summary>
        private IList<Query> lquery;


        [JsonProperty(PropertyName = "Recurso")]
        public string Recurso { get => string.Join("/", recurso, string.Join("-", Cmodulo, Ctransaccion)); }

        [JsonProperty(PropertyName = "query")]
        public IList<Query> Queries { get => lquery; set => lquery = value; }

    }

}
