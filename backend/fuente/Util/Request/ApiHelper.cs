﻿using CoreSecurity;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Util.Excepcion;

namespace Util.Request {

    /// <summary>
    /// Clase utilitaria de servicios.
    /// </summary>
    public static class ApiHelper {

        public static string GetIp(HttpContext context, HttpRequest request) {
            string ip = context.Connection.RemoteIpAddress.MapToIPv4().ToString();
            if (request.Headers.ContainsKey("X-Forwarded-For"))
                ip = request.Headers["X-Forwarded-For"];
            return ip;
        }

        public static string GetAutenticate(HttpRequest request) {
            string autenticate = null;
            if (request.Headers.ContainsKey("Autenticate"))
                autenticate = request.Headers["Autenticate"];
            if (string.IsNullOrEmpty(autenticate))
                autenticate = null;
            return autenticate;
        }

    }

    /// <summary>
    /// Clase utilitaria que obtiene el request. 
    /// </summary>
    public static class RequestHelper {

        public static string GetRequest(string data) {
            return Convert.ToString(data);
        }

        public async static Task<string> GetRequestAsync(HttpRequest request) {
            try {
                using (StreamReader reader = new StreamReader(request.Body, Encoding.UTF8)) {
                    return await reader.ReadToEndAsync();
                }
            } catch {
                throw new CoreExcepcion(string.Empty, "NO SE PUDO PROCESAR PETICIÓN/RESPUESTA");
            }
        }

    }

    /// <summary>
    /// Clase utilitaria que genera el response. La informacion retorna encriptada.
    /// </summary>
    public static class ResponseHelper {

        public static string GetResponse(Response response, HttpRequest request) {
            string data = null;

            try {
                string autenticate = ApiHelper.GetAutenticate(request);
                string responseData = ConvertObject.Serialize(response);
                if (string.IsNullOrEmpty(autenticate)) {
                    data = SecurityLib.OpenSSLEncrypt(responseData);
                } else {
                    data = SecurityLib.OpenSSLEncrypt(responseData, autenticate);
                }
            } catch {
                return ConvertObject.Serialize(data);
            }

            return ConvertObject.Serialize(response);
        }

        public static string GetResponseService(string response, bool decrypt = true) {
            string data = response;
            if (decrypt) {
                data = SecurityLib.OpenSSLDecrypt(ConvertObject.Deserialize(response).ToString());
                data = ConvertObject.Serialize(data);
            }
            return data;
        }

        public static string GetResponseError(HttpRequest request) {
            Response respuesta = new Response();
            respuesta.SetCod("0990");
            respuesta.SetMsgusu("ERROR");

            return GetResponse(respuesta, request);
        }
    }
}
