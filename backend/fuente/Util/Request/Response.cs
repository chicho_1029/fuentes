﻿using System;
using System.Collections.Generic;

namespace Util.Request {

    [Serializable]
    public class Response : Dictionary<string, object> {

        /// <summary>
        /// Constante que define el codigo de respuesta ok.
        /// </summary>
        private static string cod = "OK";

        /// <summary>
        /// Crea una instancia de Response.
        /// </summary>
        public Response() {
            base["cod"] = cod;
        }

        public void SetCod(string cod) {
            base["cod"] = cod;
        }

        public string GetCod() {
            return (string)base["cod"];
        }

        public void SetMsgusu(string msgusu) {
            base["msgusu"] = msgusu;
        }

        public string GetMsgusu() {
            return (string)(base.ContainsKey("msgusu") ? base["msgusu"] : "");
        }

    }

}
