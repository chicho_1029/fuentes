﻿using Newtonsoft.Json;
using Util.Componentes;

namespace Util.Request {

    public class RequestMantener : RequestBase {

        /// <summary>
        /// Nombre del recurso de ejecución.
        /// </summary>
        private readonly string recurso = "Mantener";

        /// <summary>
        /// Nombre de entidad de la bdd.
        /// </summary>
        private string entidad;

        /// <summary>
        /// Instancia de mantenimiento para ejecucion de transaccion.
        /// </summary>
        private Mantener mantener;


        [JsonProperty(PropertyName = "Recurso")]
        public string Recurso { get => string.Join("/", recurso, string.Join("-", Cmodulo, Ctransaccion)); }

        [JsonProperty(PropertyName = "entidad")]
        public string Entidad { get => entidad; set => entidad = value; }

        [JsonProperty(PropertyName = "mantener")]
        public Mantener Mantener { get => mantener; set => mantener = value; }

    }

}
