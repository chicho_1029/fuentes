﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace Util.Request {

    public class ConvertObject {

        /// <summary>
        /// Serializa objeto.
        /// </summary>
        public static string Serialize(object obj, bool classNames = false) {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.None;
            if (classNames) {
                settings.ContractResolver = new ContractResolver();
                settings.NullValueHandling = NullValueHandling.Ignore;
            }

            return JsonConvert.SerializeObject(obj, settings);
        }

        /// <summary>
        /// Deserializa objeto.
        /// </summary>
        public static object Deserialize(string text, bool classNames = false) {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.None;
            if (classNames) {
                settings.ContractResolver = new ContractResolver();
            }

            return JsonConvert.DeserializeObject(text, settings);
        }

        /// <summary>
        /// Deserializa objeto.
        /// </summary>
        public static T Deserialize<T>(string text, bool classNames = false) {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.None;
            if (classNames) {
                settings.ContractResolver = new ContractResolver();
            }

            return JsonConvert.DeserializeObject<T>(text, settings);
        }

        /// <summary>
        /// Serializa y Deserializa objeto.
        /// </summary>
        public static T SerializeDeserialize<T>(object obj, bool classNames = false) {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.None;
            if (classNames) {
                settings.ContractResolver = new ContractResolver();
            }

            return JsonConvert.DeserializeObject<T>(Serialize(obj, classNames), settings);
        }

        /// <summary>
        /// Metodo que mapea los campos de la clase.
        /// </summary>
        private class ContractResolver : DefaultContractResolver {

            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization) {
                IList<JsonProperty> list = base.CreateProperties(type, memberSerialization);
                foreach (JsonProperty prop in list) {
                    prop.PropertyName = prop.UnderlyingName;
                }

                return list;
            }
        }

    }

}