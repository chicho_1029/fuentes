﻿using Microsoft.AspNetCore.Authorization;

namespace WebCORE.Authentication {
    public class BasicAuthorizationAttribute : AuthorizeAttribute {
        public BasicAuthorizationAttribute() {
            Policy = "BasicAuthentication";
        }
    }
}