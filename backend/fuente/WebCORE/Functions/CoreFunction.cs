﻿using Core.Util.Excepcion;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Net;
using System.Text;
using Util.Configuracion;
using Util.Request;

namespace WebCORE.Functions {

    internal class CoreFunction {

        private readonly string urlApi;
        private readonly AuthorizationBasic authorizationbasic;

        public CoreFunction(string urlApi, AuthorizationBasic authorizationbasic) {
            this.urlApi = urlApi;
            this.authorizationbasic = authorizationbasic;
        }

        internal string Ejecutar(HttpContext context, HttpRequest request, string datos, string metodo, bool post = true) {
            string response = null;
            string URL = string.Join("/", urlApi, "Api", metodo);

            try {
                string datastr = ConvertObject.Serialize(datos);
                var clientIP = ApiHelper.GetIp(context, request);
                string authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(string.Join(":", authorizationbasic.UsrWebAPI, authorizationbasic.PwdWebAPI)));

                HttpWebRequest httpRequest = WebRequest.Create(URL) as HttpWebRequest;

                httpRequest.Headers.Add("clientIP", clientIP);
                httpRequest.Headers["Authorization"] = "Basic " + authInfo;
                httpRequest.Method = "GET";
                httpRequest.Headers["Autenticate"] = ApiHelper.GetAutenticate(request);

                if (post) {
                    byte[] bufferData = Encoding.UTF8.GetBytes(datastr);

                    httpRequest.Method = "POST";
                    httpRequest.KeepAlive = false;
                    httpRequest.UseDefaultCredentials = true;
                    httpRequest.ContentType = "text/json";
                    httpRequest.ContentLength = bufferData.Length;
                    httpRequest.Timeout = 300000;

                    using (Stream streamRequest = httpRequest.GetRequestStream()) {
                        streamRequest.Write(bufferData, 0, bufferData.Length);
                        streamRequest.Flush();
                        streamRequest.Close();
                    }
                }

                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                using (Stream streamResponse = httpResponse.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(streamResponse)) {
                    response = streamReader.ReadToEnd();
                    streamReader.Close();
                    streamResponse.Close();
                    httpResponse.Close();
                }
            } catch (WebException ex) {
                StreamReader stream = new StreamReader(ex.Response.GetResponseStream());
                response = stream.ReadToEnd();
            } catch (Exception ex) {
                string resp = ConvertObject.Serialize(ExcepcionControlador.GetMensajeResponse(new RequestBase(), ex));
                return resp;
            }
            return response;
        }

        internal static string EjecutarHub(string urlApiHub, string datos, string metodo) {
            string response;
            string URL = string.Join("/", urlApiHub, "Api", metodo);
            string datastr = ConvertObject.Serialize(datos);
            HttpWebRequest httpRequest = WebRequest.Create(URL) as HttpWebRequest;

            byte[] bufferData = Encoding.UTF8.GetBytes(datastr);

            httpRequest.Method = "POST";
            httpRequest.KeepAlive = false;
            httpRequest.UseDefaultCredentials = true;
            httpRequest.ContentType = "text/json";
            httpRequest.ContentLength = bufferData.Length;
            httpRequest.Timeout = 300000;

            using (Stream streamRequest = httpRequest.GetRequestStream()) {
                streamRequest.Write(bufferData, 0, bufferData.Length);
                streamRequest.Flush();
                streamRequest.Close();
            }

            try {
                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                using (Stream streamResponse = httpResponse.GetResponseStream())
                using (StreamReader streamReader = new StreamReader(streamResponse)) {
                    response = streamReader.ReadToEnd();
                    streamReader.Close();
                    streamResponse.Close();
                    httpResponse.Close();
                }
            } catch (WebException ex) {
                StreamReader stream = new StreamReader(ex.Response.GetResponseStream());
                response = stream.ReadToEnd();
            } catch (Exception ex) {
                response = ex.Message;
            }
            return response;
        }

    }

}
