﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using WebCORE.Functions;

namespace WebCORE.Providers {

    public class NotificacionHub : Hub {

        public override Task OnConnectedAsync() {
            return base.OnConnectedAsync();
        }

        public void RecibirNotificacion(string data) {
            try {
                CoreFunction.EjecutarHub("http://localhost:8092", data, "MantenerHub");
            } catch { }
        }

    }
}
