using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Util.Configuracion;
using WebCORE.Authentication;
using WebCORE.Providers;

namespace WebCORE {

    public class Startup {

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services) {
            services.Configure<AppSettings>(Configuration.GetSection("Configuration"));

            services.AddCors();
            services.AddMvc().AddNewtonsoftJson();
            services.AddSignalR();
            services.AddControllers();

            services.AddAuthorization(options => {
                options.AddPolicy("BasicAuthentication", new AuthorizationPolicyBuilder("BasicAuthentication").RequireAuthenticatedUser().Build());
            });

            services.AddAuthentication().AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", options => { });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthorization();
            app.UseAuthentication();

            app.UseCors(options => options.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            //app.UseCors(options => options.WithOrigins("http://186.4.217.19:9899").AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            //app.UseCors(options => options.WithOrigins("https://proyectos.wace-it.com:4433").AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            app.UseForwardedHeaders(new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto });

            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
                endpoints.MapHub<NotificacionHub>("HubCore/NotificacionCore/Recibir");
            });

            app.Run(async (context) => {
                await context.Response.WriteAsync("<div class='container'><h2 style = 'color:cornflowerblue'><b> CORE WEB SERVICES </b></h2><br/></div><div class='container body-content'><center><footer style = 'color:black;position:absolute;bottom:0px' ><hr/><p> &copy; 2020</p></footer></center></div>");
            });
        }
    }
}
