﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Util.Configuracion;
using Util.Request;
using WebCORE.Authentication;
using WebCORE.Functions;
using WebCORE.Providers;

namespace WebCORE.Controllers {

    [Route("ApiCore/ServiciosCore")]
    [BasicAuthorization]
    [ApiController]
    public class CoreController : ControllerBase {

        private readonly CoreFunction function;
        private readonly IHubContext<NotificacionHub> hub;

        public CoreController(IOptions<AppSettings> settings, IHubContext<NotificacionHub> hub) {
            function = new CoreFunction(settings.Value.UrlApi, settings.Value.AuthorizationBasic);
            this.hub = hub;
        }

        [Route("Mantener")]
        [HttpPost]

        public IActionResult Mantener([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.Ejecutar(HttpContext, Request, dataRequest, "Mantener");
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }


        [Route("Consultar")]
        [HttpPost]

        public IActionResult Consultar([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.Ejecutar(HttpContext, Request, dataRequest, "Consultar");
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("Menu")]
        [HttpPost]

        public IActionResult Menu([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.Ejecutar(HttpContext, Request, dataRequest, "Menu");
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("Login")]
        [HttpPost]

        public IActionResult Login([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.Ejecutar(HttpContext, Request, dataRequest, "Login");
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }


        [Route("Logout")]
        [HttpPost]

        public IActionResult Logout([FromBody] string data) {
            try {
                string dataRequest = RequestHelper.GetRequest(data);
                string response = function.Ejecutar(HttpContext, Request, dataRequest, "Logout");
                return Ok(ResponseHelper.GetResponseService(response, false));
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

        [Route("Enviar")]
        [HttpPost]
        public async Task<IActionResult> Enviar() {
            try {
                string dataRequest = await RequestHelper.GetRequestAsync(Request);
                await hub.Clients.All.SendAsync("notificarTodos", dataRequest);
                return Ok(new { resp = "ok" });
            } catch {
                return Ok(ResponseHelper.GetResponseError(Request));
            }
        }

    }
}
