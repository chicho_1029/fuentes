const PROXY_CONFIG = [
  {
    context: [
      "/Api",
      "/Hub"
    ],

    //target: "http://localhost:8092",
    target: "https://186.4.217.19:4433/WebAPI",
    secure: false,
    changeOrigin: false,
    logLevel: 'debug'
  }
]

module.exports = PROXY_CONFIG;
