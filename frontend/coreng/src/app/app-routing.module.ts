import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from '../app/modulos/core/home/home.component';
import { VerificaAutorizacionService } from '../app/util/verificaAutorizacion.service';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: HomeComponent, canActivate: [VerificaAutorizacionService] },
      // Core
      // {
      //   path: '102-1', loadChildren: () => import('./../app/modulos/servicioalcliente/bandejacliente/bandejacliente.module').then(m => m.BandejaClienteModule),
      //   canActivate: [VerificaAutorizacionService]
      // },

      { path: '**', redirectTo: '' }
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
