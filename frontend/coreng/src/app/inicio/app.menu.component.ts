import { Component, Input, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { AppService } from '../util/app.service';

@Component({
    selector: 'app-menu',
    template: `
		<ul class="navigation-menu">
			<li app-menuitem *ngFor="let item of menu; let i = index;" [item]="item" [index]="i" [root]="true"></li>
		</ul>
    `
})
export class AppMenuComponent implements OnInit {

  @Input() menu: any;

    constructor(public app: AppComponent, public appService: AppService) { }

    ngOnInit() {
    }

}
