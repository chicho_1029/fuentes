import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
    /* tslint:disable:component-selector */
    selector: '[app-notificacionitem]',
    /* tslint:enable:component-selector */
    template: `
    <ng-container>
    <a (click)="app.bandejaNotificacion(item)" (mouseenter)="hover=true" (mouseleave)="hover=false" class="ui-nopad">
        <div class="ui-g ui-nopad side-menu">
            <div class="ui-g-2 ui-md-2 ui-nopad button-chat correo-color" *ngIf="item.Corigen === 1">
                <div class="ui-g-12 ui-md-12 ui-nopad  " style="text-align: center; font-size:15px">
                   @
                </div>
                <div *ngIf="item.Validado" class="ui-g-12 ui-md-12 ui-nopad validateticket"><i class="fa fa-check-square" style="color: #3be116"></i></div>
            </div>
            <div class="ui-g-2 ui-md-2 ui-nopad button-chat facebook-color" *ngIf="item.Corigen === 2">
                <div class="ui-g-12 ui-md-12 ui-nopad">
                    <img src="assets/imagenes/serviciocliente/messenger.png" alt="" width="12" class="button-canal-icon">
                </div>
                <div *ngIf="item.Validado" class="ui-g-12 ui-md-12 ui-nopad validateticket"><i class="fa fa-check-square" style="color: #3be116"></i></div>
                <div *ngIf="!item.Mensajeleido" class="ui-g-12 ui-md-12 ui-nopad numberofchats"> </div>
            </div>
            <div class="ui-g-2 ui-md-2 ui-nopad button-chat instagram-color" *ngIf="item.Corigen === 3">
                <div class="ui-g-12 ui-md-12 ui-nopad">
                    <img src="assets/imagenes/serviciocliente/instagram.png" alt="" width="12"
                        class="button-canal-icon">
                </div>
                <div *ngIf="item.Validado" class="ui-g-12 ui-md-12 ui-nopad validateticket"><i class="fa fa-check-square" style="color: #3be116"></i></div>
                <div *ngIf="!item.Mensajeleido" class="ui-g-12 ui-md-12 ui-nopad numberofchats"></div>

            </div>
            <div class="ui-g-2 ui-md-2 ui-nopad button-chat whatsapp-color" *ngIf="item.Corigen === 4">
                <div class="ui-g-12 ui-md-12 ui-nopad">
                    <img src="assets/imagenes/serviciocliente/whatsapp.png" alt="" width="12"
                        class="button-canal-icon">
                </div>
                <div *ngIf="item.Validado" class="ui-g-12 ui-md-12 ui-nopad validateticket"><i class="fa fa-check-square" style="color: #3be116"></i></div>
                <div *ngIf="!item.Mensajeleido" class="ui-g-12 ui-md-12 ui-nopad numberofchats"></div>
            </div>
            <div class="ui-g-2 ui-md-2 ui-nopad button-chat telefono-color" *ngIf="item.Corigen === 5">
                <div class="ui-g-12 ui-md-12 ui-nopad  " style="text-align: center; font-size:15px">
                <img src="assets/imagenes/serviciocliente/telefono.png" alt="" width="12"
                class="button-canal-icon">
                </div>
                <div *ngIf="item.Validado" class="ui-g-12 ui-md-12 ui-nopad validateticket"><i class="fa fa-check-square" style="color: #3be116"></i></div>
            </div>
            <div class="ui-g-2 ui-md-2 ui-nopad button-chat facebook-color" *ngIf="item.Corigen === 6">
                <div class="ui-g-12 ui-md-12 ui-nopad">
                    <img src="assets/imagenes/serviciocliente/facebook.png" alt="" width="12" class="button-canal-icon">
                </div>
                <div *ngIf="item.Validado" class="ui-g-12 ui-md-12 ui-nopad validateticket"><i class="fa fa-check-square" style="color: #3be116"></i></div>
                <div *ngIf="!item.Mensajeleido" class="ui-g-12 ui-md-12 ui-nopad numberofchats"> </div>
            </div>
            <div class="ui-g-9 ui-md-10" style=" padding-right: 0; padding-top: 0; width: 92%;">
            <div class="ui-g-10 ui-md-10 ui-nopad" style=" width: 92%;">
            <span class="name">{{item.Nemisor}}
                        
            <i style="font-size: 11px; color: #f6e08b; margin-left: 2px;" *ngIf="item.Destacado" class="fa fa-star"></i>
            </span>
            
                </div>
                <div class="ui-g-10 ui-md-10 ui-nopad" *ngIf="item.Corigen === 1">
                    <span
                        class="message">{{(item.Cabecera.length > 10)? (item.Cabecera | slice:0:20)+'..':(item.Cabecera)}}</span>
                </div>
                <div class="ui-g-10 ui-md-10 ui-nopad"  *ngIf="item.Corigen === 2  || item.Corigen === 3 || item.Corigen === 4 || item.Corigen === 6">
                    <span
                        class="message">{{(item.Detalle.length > 30)? (item.Detalle | slice:0:20)+'..':(item.Detalle)}}</span>
                </div>
                <div class="ui-g-10 ui-md-10 ui-nopad"  *ngIf="item.Corigen === 5">
                <span
                    class="message">{{item.Cticket}}</span>
                </div>
                <div class="ui-g-10 ui-md-10 ui-nopad" *ngIf="app.appService.notificacionService.gestion">
                <span
                    class="message" style="font-size: 10px;">{{item.Netiqueta}}</span>
                </div>
                <div class="ui-g-10 ui-md-10 ui-nopad"  *ngIf="item.Corigen === 1 || item.Corigen === 2 || item.Corigen === 3 || item.Corigen === 4 || item.Corigen === 5 || item.Corigen === 6">
                    <span class="message">{{item.Frecepcion | date:'dd/MM/yyyy'}}</span>
                </div>
               
            </div>
          
        </div>
    </a>
</ng-container>
    `
})
export class AppNotificacionitemComponent implements OnInit, OnDestroy {

    @Input() item: any;
    hover: boolean;

    constructor(public app: AppComponent, public router: Router) {
    }

    ngOnInit() {

    }

    updateActiveStateFromRoute() {

    }

    itemClick(event: Event) {

    }

    ngOnDestroy() {

    }
}
