import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';


@Component({
    /* tslint:disable:component-selector */
    selector: '[app-gestionnotificacionitem]',
    /* tslint:enable:component-selector */
    template: `
    <ng-container>
    <a (click)="app.bandejaotificacion(item)" (mouseenter)="hover=true" (mouseleave)="hover=false" class="ui-nopad">
    <div class="ui-g ui-nopad">
      <div class="ui-g-12 ui-md-2 ui-nopad button-chat correo-color" *ngIf="item.Cinteraccionsolicitud === 1">
        <img src="assets/imagenes/serviciocliente/correo.png" alt="" width="15" class="button-canal-icon">
      </div>
      <div class="ui-g-12 ui-md-2 ui-nopad button-chat facebook-color" *ngIf="item.Cinteraccionsolicitud === 2">
        <img src="assets/imagenes/serviciocliente/messenger.png" alt="" width="15" class="button-canal-icon">
      </div>
      <div class="ui-g-12 ui-md-2 ui-nopad button-chat instagram-color" *ngIf="item.Cinteraccionsolicitud === 3">
        <img src="assets/imagenes/serviciocliente/instagram.png" alt="" width="15" class="button-canal-icon">
      </div>
      <div class="ui-g-12 ui-md-10" style=" padding-right: 0; padding-top: 0">
        <div class="ui-g-12 ui-md-10" style=" padding-right: 0; padding-top: 0">
          <span class="name">{{item.Nombre}}</span>
        </div>
        <div class="ui-g-12 ui-md-10" style=" padding-right: 0; padding-top: 0">
          <span class="message" >{{item.Cabecera}}</span>
        </div>
      </div>
      <div class="ui-g-12 ui-md-12" style="border: 1px solid #4f4f56;
       height: 0;
       padding: 0; ">
      </div>
    </div>
  </a>
  </ng-container>
    `
})
export class AppGestionNotificacionitemComponent implements OnInit, OnDestroy {

    @Input() item: any;
    hover: boolean;

    constructor(public app: AppComponent, public router: Router) {

    }

    ngOnInit() {

    }

    updateActiveStateFromRoute() {

    }

    itemClick(event: Event) {

    }

    ngOnDestroy() {

    }
}
