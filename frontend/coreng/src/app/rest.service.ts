import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Message, MessageService } from 'primeng/api';
import { AlertService } from './util/alert.service';
import { Router } from '@angular/router';
import { CoreSecurityLibService } from 'core-security-lib';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

import { environment } from './../environments/environment';
import { MantenimientoComponent } from './util/shared/mantenimiento.component';

@Injectable()
export class Radicacion {
  public cusuario: string;
  public nusuario: string;
  public ccanal: string;
  public token: string;
  public fsistema: number;
  public fultimoingreso: Date;
  public cmodulologin: number;
  public nmodulo: string;
  public cmodulo: number;
  public ctransaccion: number;
  public coficina: number;
  public noficina: string;

  public timeoutminutos: number;
  public timeoutsegundos: number;

  constructor(cusuario: string, ccanal: string, token: string) {
    this.ccanal = ccanal;
    this.cusuario = cusuario;
    this.token = token;
  }
}

@Injectable()
export class RestService {

  httpOptions = {
    headers: new HttpHeaders(),
    withCredentials: false
  };

  public canal = 'IN';
  public cusuario = '';

  /** Variables generales del aplicativo. */
  public urlBase = '';
  public urlLogin = '';
  public urlConsultar = '';
  public urlMantener = '';
  public ip = ' ';

  /** Variables de timeout. */
  public minutos: number;
  public segundos: number;
  public intervalID: any;

  /** Validaciones de directivas  */
  public msgEmail = false;
  public msgEmailTexto = '';

  /** Objeto que gurada datos temporales utiiados en la transaccion. */
  public mradicacion: Radicacion;
  public logeado = false;

  /** Objeto que contine mensajes aplicativos. */
  msgs: Message[] = [];
  public mostrarDialogoLoading = false;
  public respuesta: any[];
  public resptemp: any[];

  /** Signalr variables */
  public hubConnection: HubConnection;
  public thenable: Promise<void>;
  private urlhub: string;
  public reconectID: any;

  public activeTabIndex = -1;
  public sidebarActive = true;

  emNotifica: EventEmitter<any> = new EventEmitter();

  constructor(private http: HttpClient, private alertService: AlertService, private messageService: MessageService,
    public router: Router, public securityLib: CoreSecurityLibService) {
    this.getLocalIp();
  }

  private timeout() {
    this.resetTimer();
    this.intervalID = setInterval(() => this.tick(), 1000);
  }

  private tick(): void {
    if (--this.segundos < 0) {
      this.segundos = 59;
      if (--this.minutos < 0) {
        this.logout();
      }
    }
  }

  private resetTimer(): void {
    clearInterval(this.intervalID);
    if (this.logeado) {
      this.minutos = this.mradicacion.timeoutminutos;
      this.segundos = 0;
    } else {
      this.minutos = 60;
      this.segundos = 0;
    }
  }

  private setUrlLogin() {
    this.urlLogin = this.urlBase + '/ServiciosCore/Login';
  }

  private setUrlMantener(appName: any, metodo: any) {
    this.urlMantener = '';

    if (appName == null && metodo == null) {
      this.urlMantener = this.urlBase + '/ServiciosCore/Mantener';
    }
    if (appName == null && metodo != null) {
      this.urlMantener = this.urlBase + '/ServiciosCore' + '/' + metodo;
    }

    if (appName != null && metodo == null) {
      this.urlMantener = this.urlBase + '/' + appName + '/Mantener';
    }
    if (appName != null && metodo != null) {
      this.urlMantener = this.urlBase + '/' + appName + '/' + metodo;
    }
  }

  public setUrlConsultar(appName: any, metodo: any) {
    this.urlConsultar = '';

    if (appName == null && metodo == null) {
      this.urlConsultar = this.urlBase + '/ServiciosCore/Consultar';
    }
    if (appName == null && metodo != null) {
      this.urlConsultar = this.urlBase + '/ServiciosCore' + '/' + metodo;
    }

    if (appName != null && metodo == null) {
      this.urlConsultar = this.urlBase + '/' + appName + '/Consultar';
    }
    if (appName != null && metodo != null) {
      this.urlConsultar = this.urlBase + '/' + appName + '/' + metodo;
    }
  }

  /** Servicio de Consulta. */
  Consultar(datajson: any, cargando: boolean = true, appName: any = null, metodo: any = null): Observable<any> {
    this.mostrarDialogoLoading = cargando;
    this.getLocalIp();
    datajson.terminallocal = this.ip;
    datajson.navegador = this.getBrowser();
    this.setUrlConsultar(appName, metodo);
    const data = JSON.stringify(datajson);

    console.log('=> REQUEST Consultar: \n', datajson);

    this.httpOptions.headers = this.securityLib.GetHeaders();
    const ckey = this.securityLib.ckey;
    return this.http.post(this.urlConsultar, JSON.stringify(this.securityLib.Encriptar(data.trim(), ckey)), this.httpOptions)
      .pipe(map(resp => {
        const r = JSON.parse(this.securityLib.Desencriptar(resp, ckey));

        console.log('********* RESPONSE Consultar: \n', JSON.parse(JSON.stringify(r)));

        this.mostrarDialogoLoading = false;
        this.timeout();
        this.resptemp = this.clone(r);
        this.respuesta = this.clone(r);
        return r;
      }));
  }

  /** Servicio de Mantenimiento. */
  Mantenimiento(datajson: any, appName: any = null, metodo: any = null, cargando: boolean = true): Observable<any> {
    this.mostrarDialogoLoading = cargando;
    this.getLocalIp();
    datajson.terminallocal = this.ip;
    datajson.navegador = this.getBrowser();
    this.setUrlMantener(appName, metodo);
    const data = JSON.stringify(datajson);

    console.log('=> REQUEST Mantenimiento: \n', datajson);
    this.httpOptions.headers = this.securityLib.GetHeaders();
    const ckey = this.securityLib.ckey;
    return this.http.post(this.urlMantener, JSON.stringify(this.securityLib.Encriptar(data.trim(), ckey)), this.httpOptions)
      .pipe(map(resp => {
        const r = JSON.parse(this.securityLib.Desencriptar(resp, ckey));

        console.log('********* RESPONSE Mantenimiento: \n', JSON.parse(JSON.stringify(r)));

        this.mostrarDialogoLoading = false;
        this.timeout();
        this.resptemp = this.clone(r);
        this.respuesta = this.clone(r);
        return r;
      }));
  }

  /** Servicio de Login */
  ejecutarLogin(user: string, password: string, channel: string) {
    this.getLocalIp();
    const session: any = {};
    session.cusuario = user;
    session.password = password;
    session.ccanal = channel;
    session.terminallocal = this.ip;
    session.navegador = this.getBrowser();
    this.mostrarDialogoLoading = true;
    this.setUrlLogin();
    const data = JSON.stringify(session);

    console.log('=> REQUEST Login: \n', data);

    this.httpOptions.headers = this.securityLib.GetHeaders();
    const ckey = this.securityLib.ckey;
    return this.http.post(this.urlLogin, JSON.stringify(this.securityLib.Encriptar(data.trim(), ckey)), this.httpOptions)
      .pipe(map(resp => {
        const r = JSON.parse(this.securityLib.Desencriptar(resp, ckey));

        console.log('********* RESPONSE Login: \n', JSON.parse(JSON.stringify(r)));

        this.mostrarDialogoLoading = false;
        this.timeout();
        this.resptemp = this.clone(r);
        this.respuesta = this.clone(r);
        return r;
      }));
  }

  clone(obj: any) {
    return JSON.parse(JSON.stringify(obj));
  }

  public Resp(obj: any): any {
    if (JSON.stringify(obj) === JSON.stringify(this.respuesta)) {
      return obj;
    } else {
      this.encerarCredencialesLogin();
      location.reload();
    }
  }

  public RespPan(obj: any): boolean {
    return JSON.stringify(obj) === JSON.stringify(this.resptemp);
  }

  logout(msg = '') {
    const rqLogin: any = new Object();
    rqLogin.cusuario = this.mradicacion.cusuario;
    rqLogin.token = this.mradicacion.token;
    rqLogin.ccanal = this.mradicacion.ccanal;
    const rq = JSON.stringify(rqLogin);

    if (msg !== '') {
      this.encerarCredencialesLogin();
      location.reload();
    }

    this.Mantenimiento(rqLogin, 'ServiciosCore', 'Logout').subscribe(
      resp => {
        this.llenarMensaje(resp, true);
        this.encerarCredencialesLogin();
        location.reload();
      },
      error => {
        this.manejoError(error);
      }
    );
  }

  public encerarCredencialesLogin() {
    sessionStorage.clear();
  }

  /** Browser */
  public getBrowser() {
    const ua = navigator.userAgent;
    let tem: any;
    let M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return 'Internet Explorer ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
      if (tem != null) {
        return tem.slice(1).join(' ').replace('OPR', 'Opera');
      }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    tem = ua.match(/version\/(\d+)/i);
    if (tem != null) {
      M.splice(1, 1, tem[1]);
    }
    return M.join(' ');
  }

  /** Ip local. */
  public getLocalIp() {
    const win: any = window;
    win.RTCPeerConnection = win.RTCPeerConnection || win.mozRTCPeerConnection || win.webkitRTCPeerConnection;
    if (win.RTCPeerConnection === undefined) {
      return;
    }
    const pc: any = new RTCPeerConnection({ iceServers: [] });
    const noop = function () { this.doSomething(); };
    try {
      pc.createDataChannel('');
      pc.createOffer(pc.setLocalDescription.bind(pc), noop);
      pc.onicecandidate = this.IpRegistry;
    } catch (e) { }
  }

  private IpRegistry = (ice: any) => {
    if (!ice || !ice.candidate || !ice.candidate.candidate) { return; }
    const resp = /([0-9]{1,3}(\.[0-9]{1,3}){3})/.exec(ice.candidate.candidate);
    if (resp != null && resp.length > 0) {
      this.ip = resp[0];
    }
  }

  /** Fija mensaje de respuesta resultado de ejecucion de una peticion al core. */
  public manejoError(resp: any) {
    this.mostrarDialogoLoading = false;
    const mensaje = 'No se pudo realizar la operación solicitada. Inténtelo mas tarde.';
    if (resp.cod === undefined || resp.cod === null) {
      this.messageService.add({ severity: 'error', summary: 'ERROR', sticky: true, detail: mensaje });
    }

    this.msgs = [];
  }

  public llenarMensaje(resp: any, muestaexito: boolean, limpiamsg = true) {

    if (limpiamsg) {
      this.msgs = [];
    }
    if (resp.cod === 'OK') {
      if (muestaexito) {
        this.messageService.add({ severity: 'success', summary: 'OK', sticky: false, detail: 'Información procesada con éxito' });
      }

    } else if (resp.cod === '1021' || resp.cod === '1014' || resp.cod === '1029') {

      let msg = '';
      msg = resp.msgusu !== undefined ? msg = msg + resp.msgusu : msg + '';
      this.messageService.add({ severity: 'error', summary: 'ERROR', sticky: true, detail: msg });
      this.logout(msg);
    } else if (resp.cod === '000') {
      let msg = '';
      msg = resp.msgusu !== undefined ? msg = msg + resp.msgusu : msg + '';
      this.messageService.add({ severity: 'warn', summary: 'ALERTA', sticky: false, detail: msg });
    } else {
      let msg = '';
      msg = resp.cod !== undefined ? msg = msg + resp.cod.startsWith('javax') ? '' : resp.cod + ' ' : msg + ' ';
      msg = resp.msgusu !== undefined ? msg = msg + resp.msgusu : msg + '';
      this.messageService.add({ severity: 'error', summary: 'ERROR', sticky: true, detail: msg });
    }
  }

  // Metodo que inicializa los datos cuando un usuario se autentica exitosamente
  actualizarRadicacion(mradicacion: any): void {
    this.mradicacion = new Radicacion(mradicacion.Cusuario, mradicacion.Ccanal, mradicacion.Token);
    this.mradicacion.fsistema = mradicacion.FechaSistema;
    this.mradicacion.fultimoingreso = new Date(mradicacion.FechaUltimoIngreso);
    this.mradicacion.nusuario = mradicacion.NombreUsuario;
    this.mradicacion.cmodulologin = mradicacion.Cmodulologin;
    this.mradicacion.nmodulo = mradicacion.NombreModulo;
    this.mradicacion.token = mradicacion.Token;
    this.mradicacion.coficina = mradicacion.Coficina;
    this.mradicacion.noficina = mradicacion.NombreOficina;

    this.mradicacion.timeoutminutos = mradicacion.TiempoSesion;
    this.mradicacion.timeoutsegundos = 0;
    this.logeado = true;
  }

  /** Fija mensaje de respuesta resultado de ejecucion de una peticion al core. */
  public mostrarMensaje(msgs: Message[], persistirmsg = false, acumular = false) {
    if (!acumular) {
      this.msgs = msgs;
    } else {
      this.msgs = this.msgs.concat(msgs);
    }
    this.alertService.mostrarMensaje(this.msgs, persistirmsg);
  }

  private noautorizado(resp: any) {
    if (resp.cod === '1017') {
      this.router.navigate([''], { skipLocationChange: true });
    }
  }

  public validaEmail(): boolean {
    if (this.msgEmail) {
      this.msgs = [];
      this.msgs.push({ severity: 'warn', summary: 'Correo electrónico no válido', detail: '' });
      this.alertService.mostrarMensaje(this.msgs);
      return false;
    }
    return true;
  }

  fechaToInteger(fecha: Date): string {
    if (fecha === undefined) {
      return null;
    }
    const f = fecha;
    const currDate = f.getDate();
    const currMonth = f.getMonth() + 1;
    const currYear = f.getFullYear();
    const currHours = f.getHours();
    const currMinutes = f.getMinutes();
    const currSeconds = f.getSeconds();
    let monthstr = '' + currMonth;
    let daystr = '' + currDate;
    let hourstr = '' + currHours;
    let minutestr = '' + currMinutes;
    let secondstr = '' + currSeconds;

    if (currMonth < 10) { monthstr = '0' + currMonth; }
    if (currDate < 10) { daystr = '0' + currDate; }
    if (currHours < 10) { hourstr = '0' + currHours; }
    if (currMinutes < 10) { minutestr = '0' + currMinutes; }
    if (currSeconds < 10) { secondstr = '0' + currSeconds; }
    return (monthstr + '-' + daystr + '-' + currYear + ' ' + hourstr + ':' + minutestr);
  }


  /** Metodo que asigna / clona un objeto */
  public AsignarObjeto(obj: any): any {
    let objAsignado: any;

    // Campo
    if (null == obj || 'object' !== typeof obj) {
      return obj;
    }

    // Fecha (Date)
    if (obj instanceof Date) {
      objAsignado = new Date();
      objAsignado.setTime(obj.getTime());
      return objAsignado;
    }

    // Lista (Array)
    if (obj instanceof Array) {
      objAsignado = [];
      for (let i = 0, len = obj.length; i < len; i++) {
        objAsignado[i] = this.AsignarObjeto(obj[i]);
      }
      return objAsignado;
    }

    // Objeto (Object)
    if (obj instanceof Object) {
      objAsignado = {};
      for (const attr in obj) {
        if (obj.hasOwnProperty(attr)) {
          objAsignado[attr] = this.AsignarObjeto(obj[attr]);
        }
      }
      return objAsignado;
    }
  }
}
