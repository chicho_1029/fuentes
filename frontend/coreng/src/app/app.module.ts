import es from '@angular/common/locales/es-EC';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { registerLocaleData, LocationStrategy, HashLocationStrategy, DatePipe } from '@angular/common';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BasicModule } from './util/shared/basic.module';
import { LoginComponent } from './seguridad/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { RestService } from '../app/rest.service';
//import { NotificacionService } from '../app/util/shared/servicioalcliente/notificacion.service';
import { PanelMenuModule } from 'primeng/panelmenu';
import { FormsModule } from '@angular/forms';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { AppService } from './util/app.service';

import { AppSideBarComponent } from './inicio/app.sidebar.component';
import { AppSideBarTabContentComponent } from './inicio/app.sidebartabcontent.component';

import { AppTopBarComponent } from './inicio/app.topbar.component';
import { AppMenuComponent } from './inicio/app.menu.component';
import { TableModule } from 'primeng/table';
import { HomeComponent } from './modulos/core/home/home.component';
import { ReloadComponent } from './seguridad/login/reload.component';
import { ToolbarModule } from 'primeng/toolbar';
import { AlertService } from './util/alert.service';
import { AlertModule } from './util/alert/alert.module';
import { VerificaAutorizacionService } from './util/verificaAutorizacion.service';
import { CoreSecurityLibService } from 'core-security-lib';
import { AppMenuitemComponent } from './inicio/app.menuitem.component';
import { AppNotificacionitemComponent } from './inicio/app.notificacionitem.component';
import { MenuService } from './inicio/app.menu.service';

// registrar los locales con el nombre que quieras utilizar a la hora de proveer
registerLocaleData(es, 'es');

@NgModule({
  declarations: [
    AppComponent, LoginComponent, ReloadComponent, AppTopBarComponent, AppMenuComponent,
    HomeComponent, AppSideBarComponent, AppSideBarTabContentComponent, AppMenuitemComponent, AppNotificacionitemComponent
  ],
  imports: [
    BrowserModule, RouterModule, HttpClientModule, PanelMenuModule, BasicModule, MessageModule,
    BrowserAnimationsModule, AppRoutingModule, FormsModule, ScrollPanelModule, DialogModule, TableModule,
    ToolbarModule, AlertModule
  ],
  providers: [VerificaAutorizacionService, DatePipe,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: LOCALE_ID, useValue: 'es' },
    RestService, MessageService, AppService, AlertService, CoreSecurityLibService,
    MenuService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
