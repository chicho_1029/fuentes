import { environment } from './../environments/environment';
import { Component, ViewChild, AfterViewInit, OnDestroy, Renderer2, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginComponent } from './seguridad/login/login.component';
import { RestService } from './rest.service';
import { AppService } from './util//app.service';
import { ScrollPanelModule } from 'primeng/scrollpanel';

enum MenuOrientation {
  STATIC,
  OVERLAY
}

@Component({
  selector: 'app-root',
  templateUrl: './inicio/principal.html'
})
export class AppComponent implements AfterViewInit, OnDestroy, OnInit {

  layoutMode: MenuOrientation = MenuOrientation.STATIC;
  topbarMenuActive: boolean;
  overlayMenuActive: boolean;
  staticMenuDesktopInactive: boolean;
  staticMenuMobileActive: boolean;
  rotateMenuButton: boolean;
  sidebarClick: boolean;
  topbarItemClick: boolean;
  menuButtonClick: boolean;
  activeTopbarItem: any;
  documentClickListener: () => void;
  theme = 'indigo';
  index = 0;
  DialogoVisible = true;

  @ViewChild(LoginComponent, { static: true }) logincomponent: LoginComponent;
  @ViewChild('scrollPanel', { static: true }) layoutMenuScrollerViewChild: ScrollPanelModule;
  @HostListener('window:keydown', ['$event'])
  @HostListener('window:keyup', ['$event'])
  eventoTeclaPressDown(e: any) {
    let k = null;
    if (e.which) {
      k = e.which;
    } else if (e.keyCode) {
      k = e.keyCode;
    }

    // permitir altgr
    if (e.ctrlKey && e.altKey) {
      return true;
    }

    // ALT
    if (e.altKey && (k === 37 || k === 39 || k === 64 || k === 115)) {
      e.preventDefault();
      return false;
    }
    if (e.altKey && (k === 64 || k === 92 || k === 164 || k === 165)) {
      return true;
    }
    if (e.altKey) {
      return false;
    }

    // CTRL
    // Control para permitir: ctrl+c  67, ctrl+v  86, ctrl+x  88, ctrl+p  80, ctrl+a  65, ctrl+y  89, ctrl+z  90
    if (e.ctrlKey === true && (k === 65 || k === 67 || k === 80 || k === 86 || k === 88 || k === 89 || k === 90)) {
      return true;
    }
    if (e.ctrlKey) {
      e.stopPropagation();
      e.preventDefault();
      return false;
    }
    // f2-113,  f3-114, f6-117, f11-122, f12-123
    if (k >= 112 && k <= 123) {
      e.stopPropagation();
      e.preventDefault();
      return false;
    }

    return true;
  }

  constructor(public renderer: Renderer2, private router: Router, public restService: RestService, public appService: AppService) {
    this.restService.urlBase = environment.baseUrl;
    this.restService.getLocalIp();
    //this.appService.notificacionService.requestPermission();
  }

  GetTheme(theme) {
    const themeLink: HTMLLinkElement = document.getElementById('theme-css') as HTMLLinkElement;
    const layoutLink: HTMLLinkElement = document.getElementById('layout-css') as HTMLLinkElement;

    const themeHref = 'assets/theme/theme-' + theme + '.css';
    const layoutHref = 'assets/layout/css/layout-' + theme + '.css';

    this.replaceLink(themeLink, themeHref);
    this.replaceLink(layoutLink, layoutHref);
  }

  isIE() {
    return /(MSIE|Trident\/|Edge\/)/i.test(window.navigator.userAgent);
  }

  replaceLink(linkElement, href) {
    if (this.isIE()) {
      linkElement.setAttribute('href', href);
    } else {
      const id = linkElement.getAttribute('id');
      const cloneLinkElement = linkElement.cloneNode(true);

      cloneLinkElement.setAttribute('href', href);
      cloneLinkElement.setAttribute('id', id + '-clone');

      linkElement.parentNode.insertBefore(cloneLinkElement, linkElement.nextSibling);

      cloneLinkElement.addEventListener('load', () => {
        linkElement.remove();
        cloneLinkElement.setAttribute('id', id);
      });
    }
  }

  ngOnInit() {
    this.GetTheme(this.theme);
    this.appService.login = false;
    //this.appService.notificacionService.initSignalR();

    if (sessionStorage.getItem('r')) {
      const rad = JSON.parse(this.restService.securityLib.Desencriptar(JSON.parse(sessionStorage.getItem('r')).toString(), undefined));
      this.appService.titulopagina = this.appService.titulopagina + rad.NombreUsuario;
    }

    if (sessionStorage.getItem('c')) {
      this.appService.login = true;

      const mradicacion = JSON.parse(this.restService.securityLib.Desencriptar(JSON.parse(sessionStorage.getItem('r')).toString(),
        undefined));
      this.inicializarAmbiente(mradicacion);
      sessionStorage.setItem('p', JSON.stringify(this.restService.securityLib.Encriptar(''.trim(), undefined)).toString());
    }
  }


  ngAfterViewInit() {
    this.documentClickListener = this.renderer.listen('body', 'click', (event) => {
      if (!this.topbarItemClick) {
        this.activeTopbarItem = null;
        this.topbarMenuActive = false;
      }

      if (!this.menuButtonClick && !this.sidebarClick && (this.overlay || !this.isDesktop())) {
        this.restService.sidebarActive = false;
      }

      this.topbarItemClick = false;
      this.sidebarClick = false;
      this.menuButtonClick = false;
    });
  }

  openNext() {
    this.index = (this.index === 2) ? 0 : this.index + 1;
  }

  openPrev() {
    this.index = (this.index === 0) ? 2 : this.index - 1;
  }

  /** Fija datos de respuesta cuando el login es exitoso. */
  private inicializarAmbiente(mradicacion: any) {
    // Fija datos de radicacion del usuario en el singleton de servicios.
    this.restService.actualizarRadicacion(mradicacion);
    // ejecuta consulta del menu del primer rol
    this.appService.consultarMenu(mradicacion.Cmodulologin, mradicacion.Cusuario, mradicacion.Ccanal, mradicacion.Token);
  }

  onTabClick(event: Event, index: number, item: any) {
    if (this.restService.activeTabIndex === index) {
      this.restService.sidebarActive = !this.restService.sidebarActive;
    } else {
      this.restService.activeTabIndex = index;
      this.restService.sidebarActive = true;
    }

    const modulo = item.Cmodulo;
    let transaccion = item.Ctransaccion;
    const titulopagina = item.Nombre;

    if (item.Sidebaractive) {
      this.restService.sidebarActive = true;
    } else {
      if (!item.Essubmenu) {
        this.closeSidebar(event);
      }

    }

    if (item.Cargarpantalla) {
      this.appService.cargarPantallaBandeja(titulopagina, modulo, transaccion);
    } else {
      sessionStorage.setItem('p', JSON.stringify(this.restService.securityLib.Encriptar(''.trim(), undefined).toString()));
      this.appService.titulopagina = this.restService.mradicacion.nusuario;
      this.router.navigate([''], { skipLocationChange: true });
    }
    event.preventDefault();
  }

  closeSidebar(event: Event) {
    this.restService.sidebarActive = false;
    event.preventDefault();
  }

  onSidebarClick(event: Event) {
    this.sidebarClick = true;
  }

  onTopbarMenuButtonClick(event: Event) {
    this.topbarItemClick = true;
    this.topbarMenuActive = !this.topbarMenuActive;

    event.preventDefault();
  }

  onMenuButtonClick(event: Event, index: number) {
    this.menuButtonClick = true;
    this.rotateMenuButton = !this.rotateMenuButton;
    this.topbarMenuActive = false;
    this.restService.sidebarActive = !this.restService.sidebarActive;


    if (this.layoutMode === MenuOrientation.OVERLAY) {
      this.overlayMenuActive = !this.overlayMenuActive;
    } else {
      if (this.isDesktop()) {
        this.staticMenuDesktopInactive = !this.staticMenuDesktopInactive;
      } else {
        this.staticMenuMobileActive = !this.staticMenuMobileActive;
      }
    }

    if (this.restService.activeTabIndex < 0) {
      this.restService.activeTabIndex = 0;
    }

    const sidebaritem = this.appService.sidebar.find(x => x.Orden === this.restService.activeTabIndex);

    // if (!this.appService.notificacionService.estaVacio(sidebaritem)) {
    //   if (sidebaritem.Sidebaractive) {
    //     this.restService.sidebarActive = true;
    //   } else {
    //     if (!sidebaritem.Essubmenu) {
    //       this.closeSidebar(event);
    //     }
    //   }
    // }
    event.preventDefault();
  }

  onTopbarItemClick(event: Event, item) {
    this.topbarItemClick = true;

    if (this.activeTopbarItem === item) {
      this.activeTopbarItem = null;
    } else {
      this.activeTopbarItem = item;
    }

    event.preventDefault();
  }

  onTopbarSearchItemClick(event: Event) {
    this.topbarItemClick = true;

    event.preventDefault();
  }

  onTopbarSubItemClick(event) {
    event.preventDefault();
  }

  onClass(notificacion: boolean): string {
    let clase = 'tabmenuitem-link';
    if (notificacion) {
      clase = 'ripplelink tabmenuitem-link';
    }
    return clase;
  }

  logout(event) {
    this.appService.salir(event);
  }

  bandejaNotificacion(item: any) {
    const modulo = '102';
    let transaccion = '1';
    const titulopagina = 'BUZÓN SERVICIO AL CLIENTE';
    // transaccion Corigen 5 Central Telefonica
    if (item.Corigen === 5) {
      if (!item.Gestion) {
        transaccion = '7';
      }

    }

    //this.restService.signalREmit(item.Cticket);
    // this.appService.notificacionService.eliminarmensajeleido(item);
    // this.appService.notificacionService.displayDialogBuscar = false;
    this.appService.cargarPantallaNotificacion(item, titulopagina, modulo, transaccion);
  }

  get overlay(): boolean {
    return this.layoutMode === MenuOrientation.OVERLAY;
  }

  changeToStaticMenu() {
    this.layoutMode = MenuOrientation.STATIC;
  }

  changeToOverlayMenu() {
    this.layoutMode = MenuOrientation.OVERLAY;
  }

  isDesktop() {
    return window.innerWidth > 1024;
  }

  ngOnDestroy() {
    if (this.documentClickListener) {
      this.documentClickListener();
    }
  }
}
