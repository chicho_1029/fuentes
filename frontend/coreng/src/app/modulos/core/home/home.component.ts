import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from './../../../rest.service';

@Component({
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(public router: Router, public rest: RestService) { }


  ngOnInit() {
    if (sessionStorage.getItem('p') !== undefined && sessionStorage.getItem('p') !== null) {
      const path = this.rest.securityLib.Desencriptar(JSON.parse(sessionStorage.getItem('p')).toString(), undefined);
      this.router.navigate([path], { skipLocationChange: true });
    }
  }

}
