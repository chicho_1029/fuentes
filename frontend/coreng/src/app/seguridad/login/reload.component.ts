import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reload',
  template: ``
})
export class ReloadComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    sessionStorage.clear();
    location.reload();
  }

}
