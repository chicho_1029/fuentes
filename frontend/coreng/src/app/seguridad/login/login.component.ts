import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { RestService } from '../../rest.service';
import { AppService } from '../../util/app.service';
import { BaseComponent } from 'src/app/util/shared/base.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {
  user = '';
  pass = '';
  blockSpace: RegExp = /[^\s]/;
  noSymbols: RegExp = /[A-Za-z0-9]/;
  displayDialog = false;

  constructor(public rest: RestService, public route: ActivatedRoute, public appService: AppService, public router: Router, private message: MessageService) {
    super(router, rest, route, message);
  }

  ngOnInit() {
  }

  /** Invoca al core para realizar login de la aplicacion. */
  ejecutalogin() {
    super.encerarMensajes();

    this.rest.getLocalIp();
    this.appService.login = false;
    this.rest.cusuario = this.user;
    this.rest.ejecutarLogin(this.user, this.pass, this.rest.canal)
      .subscribe(
        resp => {
          this.manejaRespuestaLogin(resp);
        },
        error => {
          this.rest.manejoError(error);
        });
  }

  /** Manejo respuesta de ejecucion de login. */
  private manejaRespuestaLogin(resp: any) {
    if (this.rest.Resp(resp).cod === 'OK') {
      this.encerarMensajes();
      this.inicializarAmbiente(this.rest.Resp(resp));
    }
    if (this.rest.Resp(resp).cod !== 'OK') {
      let msg = '';
      msg = this.rest.Resp(resp).cod !== undefined ? msg = msg + resp.cod + ' ' : msg + ' ';
      msg = this.rest.Resp(resp).msgusu !== undefined ? msg = msg + resp.msgusu : msg + '';
      this.rest.llenarMensaje(this.rest.Resp(resp), false);
      return;
    }
  }

  /** Fija datos de respuesta cuando el login es exitoso. */
  private inicializarAmbiente(resp: any) {
    this.appService.mradicacion.Cusuario = this.rest.cusuario;
    this.appService.mradicacion.Ccanal = this.rest.canal;
    this.appService.mradicacion.Token = resp.radicacion.Token;

    this.registros = resp.radicacion.Lmodulos;
    this.displayDialog = true;
  }

  onRowSelect(reg: any) {
    this.displayDialog = false;
    this.consultarMenu(reg.Cmodulologin, this.rest.cusuario, this.rest.canal, this.appService.mradicacion.Token, reg.Nombre);
  }

  /** Invoca al core para realizar login de la aplicacion. */
  public consultarMenu(cmodulologin: any, cusuario: any, ccanal: any, token: any, nmodulo: any) {
    this.appService.mradicacion.Cmodulologin = cmodulologin;
    this.appService.mradicacion.NombreModulo = nmodulo;

    const rqRol: any = {};
    rqRol.cmodulologin = cmodulologin;
    rqRol.cusuario = cusuario;
    rqRol.ccanal = ccanal;
    rqRol.token = token;
    this.rest.Consultar(rqRol, true, null, 'Menu')
      .subscribe(
        resp => {
          this.manejaRespuestaMenu(resp);
        },
        error => {
          this.rest.manejoError(error);
        });
  }

  /** Manejo respuesta de ejecucion de login. */
  private manejaRespuestaMenu(resp: any) {
    if (resp.cod === 'OK') {
      this.appService.setItems(resp.Sidebar);
      this.appService.sidebar = resp.Sidebar;
      this.appService.mradicacion.FechaSistema = resp.radicacion.FechaSistema;
      this.appService.mradicacion.FechaUltimoIngreso = resp.radicacion.FechaUltimoIngreso;
      this.appService.mradicacion.NombreOficina = resp.radicacion.NombreOficina;
      this.appService.mradicacion.NombreUsuario = resp.radicacion.NombreUsuario;
      this.appService.mradicacion.TiempoSesion = resp.radicacion.TiempoSesion;

      const rad = JSON.stringify(this.appService.mradicacion);
      sessionStorage.setItem('c', JSON.stringify(this.rest.securityLib.Encriptar(this.rest.canal, undefined).toString()));
      sessionStorage.setItem('r', JSON.stringify(this.rest.securityLib.Encriptar(rad.trim(), undefined).toString()));

      // Fija datos de radicacion del usuario en el singleton de servicios.
      this.rest.actualizarRadicacion(this.appService.mradicacion);

      this.appService.login = true;
      this.rest.logeado = true;

      this.appService.titulopagina = this.appService.mradicacion.NombreUsuario;

      this.appService.CargarModulos(resp);

      this.appService.ValidarMenu();

      this.message.add({ severity: 'success', summary: 'OK', sticky: false, detail: 'Bienvenido: ' + this.rest.cusuario });
    }
    if (resp.cod !== 'OK') {
      let msg = '';
      msg = resp.cod !== undefined ? msg = msg + resp.cod + ' ' : msg + ' ';
      msg = resp.msgusu !== undefined ? msg = msg + resp.msgusu : msg + '';
      this.rest.llenarMensaje(resp, false);
    }
  }

  btnCancelar() {
    this.displayDialog = false;

  }
}
