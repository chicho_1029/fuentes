import { Directive, Output, EventEmitter, HostListener } from '@angular/core';
import { BaseComponent } from '../../util/shared/base.component';
import { Router, ActivatedRoute } from '@angular/router';
import { RestService } from './../../rest.service';
import { MessageService } from 'primeng/api';

@Directive({
  selector: '[ngModel][email]'
})

export class EmailDirective extends BaseComponent {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  optionsTemplate = {
    align: 'right'
  };

  constructor(router: Router, route: ActivatedRoute, rest: RestService, private message: MessageService) {
    super(router, rest, route, message);
  }

  @HostListener('focusout', ['$event.target'])
  focusout(target: any) {
    this.value = target.value;
    this.rest.msgEmail = false;
    this.rest.msgEmailTexto = '';
    if (this.value && this.value !== '') {
      const patt = new RegExp(/^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/i).test(this.value);
      if (!patt) {
        this.rest.msgEmail = true;
        this.rest.msgEmailTexto = 'Formato de correo incorrecto';
      }
    }
  }
}
