import { Directive, Output, EventEmitter, ElementRef, AfterViewInit, HostListener } from '@angular/core';

@Directive({
  selector: '[ngModel][numeros]'
})

// npm i ng2-currency-mask    esta es la directiva original.
export class NumerosDirective implements AfterViewInit {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  optionsTemplate = {
    align: 'right'
  };

  constructor(private elementRef: ElementRef) {
  }
  ngAfterViewInit() {
    this.elementRef.nativeElement.style.textAlign = this.optionsTemplate.align;
  }

  @HostListener('keyup', ['$event.target'])
  onKeyup(target: any) {
    this.value = target.value;
    if (this.value) {
      this.value = this.value.replace(/[^0-9]/g, '');
    }
    this.ngModelChange.emit(this.value);
  }

}
