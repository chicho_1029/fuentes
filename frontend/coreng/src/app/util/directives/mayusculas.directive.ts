import { Directive, Output, EventEmitter, ElementRef, OnInit, HostListener, Renderer2 } from '@angular/core';
import { InputHandler } from './input/input.handler';

@Directive({
  selector: '[ngModel][mayusculas]'
})
export class MayusculasDirective implements OnInit {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  inputHandler: InputHandler;
  value: string;

  constructor(private el: ElementRef, private render: Renderer2) { }

  ngOnInit() {
    this.inputHandler = new InputHandler(this.el.nativeElement, Object.assign({}, {}, {}));
  }

  @HostListener('input', ['$event.target'])
  onInputChange(target: any) {
    const pos1 = this.el.nativeElement.selectionStart;
    const pos2 = this.el.nativeElement.selectionEnd;
    this.value = target.value.toLowerCase();
    this.value = this.inputHandler.reserveWord(this.value);
    this.value = this.value.toUpperCase();

    this.render.setStyle(this.el.nativeElement, 'value', this.value);
    this.ngModelChange.emit(this.value);
    this.el.nativeElement.setSelectionRange(pos1, pos2, 'none');
  }
}

