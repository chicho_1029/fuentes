import { Directive, Output, EventEmitter, ElementRef, OnInit, HostListener } from '@angular/core';
import { InputHandler } from './input/input.handler';

@Directive({
  selector: '[ngModel][minusculas]'
})
export class MinusculasDirective implements OnInit {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  inputHandler: InputHandler;
  value: string;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    this.inputHandler = new InputHandler(this.elementRef.nativeElement, Object.assign({}, {}, {}));
  }

  @HostListener('input', ['$event.target'])
  onInputChange(target: any) {
    this.value = target.value.toLowerCase();
    this.value = this.inputHandler.reserveWord(this.value);
    this.ngModelChange.emit(this.value);
  }

}
