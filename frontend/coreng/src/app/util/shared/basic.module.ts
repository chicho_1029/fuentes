import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EmailDirective } from '../directives/email.directive';
import { MayusculasDirective } from '../directives/mayusculas.directive';
import { MinusculasDirective } from '../directives/minusculas.directive';
import { MonedaDirective } from '../directives/moneda.directive';
import { NumerosDirective } from '../directives/numeros.directive';
import { CuentaDirective } from '../directives/cuenta.directive';
import { LightboxModule } from 'primeng/lightbox';
import { ConfirmationService } from 'primeng/api';
import { MenuModule } from 'primeng/menu';
import { MessagesModule } from 'primeng/messages';
import { InputMaskModule } from 'primeng/inputmask';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { EditorModule } from 'primeng/editor';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ListboxModule } from 'primeng/listbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { DataViewModule } from 'primeng/dataview';
import { CardModule } from 'primeng/card';
import { FieldsetModule } from 'primeng/fieldset';
import { PanelModule } from 'primeng/panel';
import { ToolbarModule } from 'primeng/toolbar';
import { FileUploadModule } from 'primeng/fileupload';
import { TooltipModule } from 'primeng/tooltip';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AccordionModule } from 'primeng/accordion';
import { TabMenuModule } from 'primeng/tabmenu';
import { StepsModule } from 'primeng/steps';
import { KeyFilterModule } from 'primeng/keyfilter';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ToastModule } from 'primeng/toast';
import { SpinnerModule } from 'primeng/spinner';
import { DragDropModule } from 'primeng/dragdrop';
import { MultiSelectModule } from 'primeng/multiselect';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ChartModule } from 'primeng/chart';
import { SidebarModule } from 'primeng/sidebar';
import { CarouselModule } from 'primeng/carousel';
import { BlockUIModule } from 'primeng/blockui';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { MessageModule } from 'primeng/message';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [EmailDirective, MayusculasDirective, MinusculasDirective, MonedaDirective, NumerosDirective, CuentaDirective],
  exports: [StepsModule, CommonModule, FormsModule, InputTextModule, MenuModule, ButtonModule, MessagesModule, MessageModule, InputMaskModule,
    DialogModule, FieldsetModule, PanelModule, DropdownModule, CheckboxModule, RadioButtonModule, TabViewModule,
    AccordionModule, CalendarModule, FileUploadModule, ToolbarModule, CardModule, TableModule, EditorModule, InputTextareaModule,
    ListboxModule, SelectButtonModule, DataViewModule, TooltipModule, ConfirmDialogModule, TabMenuModule, KeyFilterModule,
    SplitButtonModule, InputSwitchModule, EmailDirective, MayusculasDirective, MinusculasDirective, MonedaDirective, NumerosDirective,
    CuentaDirective, ToastModule, SpinnerModule, DragDropModule, MultiSelectModule, LightboxModule, PanelMenuModule, ChartModule,
    SidebarModule, CarouselModule, BlockUIModule, OverlayPanelModule, FullCalendarModule],
  providers: [ConfirmationService]
})
export class BasicModule { }
