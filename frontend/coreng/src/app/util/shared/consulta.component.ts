class Consulta {
  public o: string;
  public a: string;
  public m: string;
  public p: any = {};
}

export class ConsultaComponent {
  consulta: Consulta;
  componente: any;
  cclase: string;

  AgregarCodigoClase(componente: any, cclase: string) {
    this.componente = componente;
    this.cclase = cclase;
  }

  AgregarConsulta(componente: any, clase: string, objeto: string, aliasEntidad: string, metodo: string, parametros: any = {}) {
    this.componente = componente;
    const consulta: Consulta = new Consulta();
    consulta.a = aliasEntidad;
    consulta.o = objeto;
    consulta.m = metodo;
    consulta.p = parametros;

    this.cclase = clase;
    this.consulta = consulta;
  }
}

class ConsultaCatalogo {
  public o: string;
  public a: string;
  public m: string;
  public p: any = {};
}

export class ConsultaCatalogoComponent {
  componente: any;
  lconsultas: ConsultaCatalogo[];

  constructor() {
    this.lconsultas = [];
  }

  AgregarCatalogo(componente: any, objeto: string, aliasEntidad: string, metodo: string, parametros: any = {}) {
    const consultacatalogo: ConsultaCatalogo = new ConsultaCatalogo();
    consultacatalogo.o = objeto;
    consultacatalogo.a = aliasEntidad;
    consultacatalogo.m = metodo;
    consultacatalogo.p = parametros;
    this.componente = componente;
    this.lconsultas.push(consultacatalogo);
  }
}
