import { v4 as uuid } from 'uuid';

class Mantenimiento {
  public a: string;
  public m: string;
  public n: any;
  public c: any;
  public e: any;
  public p: any = {};
}

export class MantenimientoComponent {
  mantener: Mantenimiento;
  componente: any;
  cclase: string;

  AgregarMantener(componente: any, operacion: string, clase: string, aliasEntidad: string, seguridad: string, registro: any) {
    let ope: number;
    this.componente = componente;
    const mantener: Mantenimiento = new Mantenimiento();
    mantener.a = aliasEntidad;
    mantener.m = seguridad;
    mantener.p = registro;
    if (operacion === 'I') { ope = 0; } // Insert
    if (operacion === 'U') { ope = 1; } // Update
    if (operacion === 'D') { ope = 2; } // Delete

    this.cclase = clase;
    this.mantener = this.completarTrn(mantener, ope);
  }

  protected completarTrn(mantener: Mantenimiento, ope: number): Mantenimiento {
    const uid = uuid();
    const ide = uid.substring(16, uid.length);
    const rnd = Math.floor((Math.random() * (9 - 2 + 1)) + 2);
    const key = ide.substr(0, 2) + rnd + ide.substr(3, 4) + (rnd - ope) + ide.substr(8, ide.length);

    switch (ope) {
      case 0:
        mantener.n = key; break;
      case 1:
        mantener.c = key; break;
      case 2:
        mantener.e = key; break;
    }
    return mantener;
  }
}

