import { RestService } from '../../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { ConsultaComponent, ConsultaCatalogoComponent } from './consulta.component';
import { MantenimientoComponent } from './mantenimiento.component';

export class Archivo {
  Codigo: number = null;
  Nombre = '';
  Archivobytes: any = null;
  Extension: string = null;
  Tamanio: number = null;
}


export abstract class BaseComponent {

  /** Variable que indica el numero de filas a mostrar en tabla de registros.  */
  public filas = 15;
  /** Datos de trabajo. */
  // public datos: any = {};

  public datos: any = { larchivos: [] };

  /** Campos de control. */
  public rqradicacion: any = { datos: {}, query: {} };

  /**  */
  public marchivosngstr = {};

  /** Formato de fecha para el componente calendar de primeng */
  public formatofecha = 'mm/dd/yy';
  public anioactual = null;
  public mesactual = null;
  public fechaactual = null;
  public formatofechahora = 'MM/dd/yyyy HH:mm';

  /** Variable globales  */
  public OK = 'OK';
  public INGRESAR = 'I';
  public ACTUALIZAR = 'U';
  public ELIMINAR = 'D';

  /** Variable calendario es español */
  public es = {
    firstDayOfWeek: 1,
    dayNames: ['DOMINGO', 'LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES', 'SABADO'],
    dayNamesShort: ['DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB'],
    dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'],
    today: 'Hoy',
    clear: 'Borrar'
  }

  /** Items de impresion */
  tipoimpresion: MenuItem[] = [
    { label: 'PDF', command: () => { this.imprimir('pdf'); } },
    { label: 'Excel', command: () => { this.imprimir('xls'); } },
    { label: 'Word', command: () => { this.imprimir('doc'); } }
  ];

  /** Lista de tipo de identificacion */
  public ltipoidentificacion: any = [{ label: 'Seleccione..', value: '' }, { label: 'CÉDULA', value: 'C' },
  { label: 'RUC', value: 'R' }, { label: 'PASAPORTE', value: 'P' }];

  /** Objetos */
  public registros: any = [];
  public registro: any = { datos: {} };
  public registronotificacion: any = {};
  public registroClonado: any = {};
  public consulta: ConsultaComponent;
  public consultaCatalogo: ConsultaCatalogoComponent;
  public mantener: MantenimientoComponent;
  public componente: any;

  constructor(public router: Router, public rest: RestService, public route: ActivatedRoute, private messageservices: MessageService) {
    this.fechaactual = new Date();
    this.anioactual = this.fechaactual.getFullYear();
    this.mesactual = this.fechaactual.getMonth();
    this.consulta = new ConsultaComponent();
    this.consultaCatalogo = new ConsultaCatalogoComponent();
    this.mantener = new MantenimientoComponent();

    // Radicacion
    if (this.rest.mradicacion !== undefined) {
      this.rqradicacion = this.rest.mradicacion;
    }

    this.encerarMensajes();
    window.scroll(0, 0);
  }

  consultar(consulta: ConsultaComponent) {
    const c = consulta.componente;
    const rq = this.clonarRadicacion(this.rqradicacion);
    rq.datos = this.datos;
    rq.cclase = consulta.cclase;
    rq.query = [];
    rq.query.push(consulta.consulta);
    this.rest.Consultar(rq).subscribe(
      (data: any = {}) => {
        this.rest.llenarMensaje(data, false, true);
        if (data.cod === this.OK) {
          for (const i in data) {
            if (i !== 'cod') {
              c[i] = data[i];
            }
          }
        }
      },
      error => { this.rest.manejoError(error); }
    );
  }

  consultarCatalogo(consultaCatalogo: ConsultaCatalogoComponent) {
    const c = consultaCatalogo.componente;
    const rq = this.clonarRadicacion(this.rqradicacion);
    rq.query = consultaCatalogo.lconsultas;
    rq.cclase = 'CATALOGO';
    this.rest.Consultar(rq).subscribe(
      (data: any = {}) => {
        this.rest.llenarMensaje(data, false, false); // solo presenta errores.
        if (data.cod === this.OK) {
          for (const i in data) {
            if (i !== 'cod') {
              c[i] = data[i];
            }
          }
        }
      },
      error => { this.rest.manejoError(error); }
    );
  }

  grabar(autoconsulta: boolean = false, autorecarga: boolean = false) {
    this.encerarMensajes();
    const rq = this.clonarRadicacion(this.rqradicacion);
    this.rest.Mantenimiento(this.completarMantener(rq)).subscribe(
      (data: any = {}) => {
        this.rest.llenarMensaje(data, true, false);
        if (data.cod === this.OK) {
          if (autoconsulta) {
            this.consultar(this.consulta);
          }
          if (autorecarga) {
            this.limpiar();
          }
        }
      },
      error => { this.rest.manejoError(error); }
    );
  }

  eliminar(autoconsulta: boolean = true, autorecarga: boolean = false) {
    this.encerarMensajes();
    const rq = this.clonarRadicacion(this.rqradicacion);
    this.rest.Mantenimiento(this.completarMantener(rq)).subscribe(
      (data: any = {}) => {
        this.rest.llenarMensaje(data, true, false);
        if (data.cod === this.OK) {
          if (autoconsulta) {
            this.consultar(this.consulta);
          }
          if (autorecarga) {
            this.limpiar();
          }
        }
      },
      error => { this.rest.manejoError(error); }
    );
  }

  imprimir(tipo: any, verreporte: boolean = false) {
    this.encerarConsulta();
    this.datos.tipo = tipo;
    const rq = this.clonarRadicacion(this.rqradicacion);
    rq.datos = this.datos;
    rq.cclase = 'IMPRIMEREPORTE';
    this.rest.Consultar(rq).subscribe(
      (data: any = {}) => {
        this.rest.llenarMensaje(data, false, true);
        if (data.cod === this.OK) {
          this.descargarReporte(data.reporte, tipo, verreporte);
        }
      },
      error => { this.rest.manejoError(error); }
    );
  }

  protected completarMantener(rq: any, datos: boolean = false): any {
    if (datos) {
      rq.datos = this.datos;
    }
    rq.mantener = this.mantener.mantener;
    rq.cclase = this.mantener.cclase;
    return rq;
  }

  protected clonarRadicacion(obj: any): any {
    const rq: any = {};
    rq.ccanal = obj.ccanal;
    rq.cclase = obj.cclase;
    rq.cmodulo = obj.cmodulo;
    rq.cmodulologin = obj.cmodulologin;
    rq.ctransaccion = obj.ctransaccion;
    rq.cusuario = obj.cusuario;
    rq.fsistema = obj.fsistema;
    rq.token = obj.token;
    return rq;
  }

  protected clonarRegistro(obj: any) {
    const registroClonado = {};
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        registroClonado[prop] = obj[prop];
      }
    }
    this.registroClonado = registroClonado;
  }

  clone(obj: any) {
    return JSON.parse(JSON.stringify(obj));
  }

  /** Encera mensajes. */
  public encerarMensajes(): void {
    this.messageservices.clear();
  }

  /** Fija un mensaje de error. */
  protected mostrarMensajeError(texto: string) {
    this.messageservices.add({ severity: 'error', summary: texto, detail: '' });
  }

  /** Fija un mensaje tipo warning. */
  protected mostrarMensajeWarn(texto: string) {
    this.messageservices.add({ severity: 'warn', summary: texto, detail: '' });
  }

  /** Adiciona un mensaje informativo. */
  protected mostrarMensajeInfo(texto: string) {
    this.messageservices.add({ severity: 'info', summary: texto, detail: '' });
  }

  /** Adiciona un mensaje de exisot. */
  protected mostrarMensajeSuccess(texto: string) {
    this.messageservices.add({ severity: 'success', summary: texto, detail: '' });
  }

  public estaVacio(obj: any): boolean {
    if (obj === undefined || obj === null || obj === ''
      || (typeof obj === 'object' && !(obj instanceof Date) && Object.keys(obj).length === 0) || obj.length <= 0) {
      return true;
    }
    return false;
  }

  /** Encerar lista de consulta. */
  encerarConsulta(): void {
    this.consulta.cclase = undefined;
    this.consulta.componente = undefined;
    this.consulta.consulta = undefined;
  }

  /** Encerar lista de mantenimiento. */
  encerarMantener(): void {
    this.mantener.cclase = undefined;
    this.mantener.componente = undefined;
    this.mantener.mantener = undefined;
  }

  /** Encerar lista de consulta de catalogo. */
  encerarConsultaCatalogo(): void {
    this.encerarConsulta();
    this.consultaCatalogo.componente = undefined;
    this.consultaCatalogo.lconsultas = [];
  }

  /** Limpia pantalla ingresada */
  limpiar() {
    this.rest.msgEmail = false;
    this.rest.msgEmailTexto = '';
    this.router.navigate([''], { skipLocationChange: true });
  }

  /** Transforma una fecha entero en formato MMddyyyy */
  public integerToFormatoFecha(valor: number): string {
    if (this.estaVacio(valor)) {
      return null;
    }
    const anio = valor.toString().substring(0, 4);
    const mes = valor.toString().substring(4, 6);
    const dia = valor.toString().substring(6, 8);
    const fecha = mes + '-' + dia + '-' + anio;
    return fecha;
  }

  /** Transforma una fecha entero en formato yyyyyMMdd a una fecha en formato dd-MM-yyyy */
  public integerToFecha(valor: number): Date {
    if (this.estaVacio(valor)) {
      return null;
    }
    const anio = valor.toString().substring(0, 4);
    const mes = valor.toString().substring(4, 6);
    let dia = valor.toString().substring(6, 8);
    if (this.estaVacio(dia)) {
      dia = '01';
    }
    return new Date(Number(anio), (Number(mes) - 1), Number(dia));
  }

  /** Transforma una fecha en formato yyyyy-MM-dd a una fecha */
  public stringToFecha(valor: string): Date {
    const anio = +valor.substring(0, 4);
    const mes = +valor.substring(5, 7) - 1;
    const dia = +valor.substring(8, 10);
    const fecha: Date = new Date(anio, mes, dia);
    return fecha;
  }

  fechaToInteger(fecha: Date): number {
    if (this.estaVacio(fecha)) {
      return null;
    }
    const f = fecha;
    const currDate = f.getDate();
    const currMonth = f.getMonth() + 1;
    const currYear = f.getFullYear();
    let monthstr = '' + currMonth;
    let daystr = '' + currDate;
    if (currMonth < 10) { monthstr = '0' + currMonth; }
    if (currDate < 10) { daystr = '0' + currDate; }
    return Number(currYear + '' + monthstr + '' + daystr);
  }

  public calendarToFechaString(fecha: Date): string {
    if (this.estaVacio(fecha)) {
      return null;
    }
    const f = fecha;
    const currDate = f.getDate();
    const currMonth = f.getMonth() + 1;
    const currYear = f.getFullYear();
    let monthstr = '' + currMonth;
    let daystr = '' + currDate;
    if (currMonth < 10) { monthstr = '0' + currMonth; }
    if (currDate < 10) { daystr = '0' + currDate; }
    return (currYear + '-' + monthstr + '-' + daystr);
  }


  /** Redondea el valor con la precision enviada */
  public redondear(value, precision): number {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  descargarReporte(bytes: any, tipo: any, verreporte: boolean): void {
    const linkElement = document.createElement('a');
    linkElement.target = '_blank';
    let contenttype = '';
    if (tipo === 'pdf') {
      contenttype = 'application/pdf';
    } else if (tipo === 'xls') {
      contenttype = 'application/vnd.ms-excel';
    } else {
      contenttype = 'application/octet-stream';
    }
    const blob = new Blob([this.convertirBytes(bytes)], { type: contenttype });
    const bloburl = URL.createObjectURL(blob);

    linkElement.href = bloburl;
    if (!verreporte) {
      linkElement.download = 'reporte.' + tipo;
    }

    document.body.appendChild(linkElement);
    const clickEvent = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: false
    });
    linkElement.dispatchEvent(clickEvent);
  }

  descargarArchivo(bytes: any, tipo: any, nombrearchivo: any): void {
    const linkElement = document.createElement('a');
    linkElement.target = '_blank';
    let contenttype = '';
    if (tipo === 'pdf') {
      contenttype = 'application/pdf';
    } else if (tipo === 'docx' || tipo === 'doc') {
      contenttype = 'application/vnd.ms-word';
    } else {
      contenttype = 'application/octet-stream';
    }
    const blob = new Blob([this.convertirBytes(bytes)], { type: contenttype });
    const bloburl = URL.createObjectURL(blob);

    linkElement.href = bloburl;
    linkElement.download = nombrearchivo + '.' + tipo;

    document.body.appendChild(linkElement);
    const clickEvent = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: false
    });
    linkElement.dispatchEvent(clickEvent);
  }

  descargar(linkElement: any) {
    const clickEvent = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: false
    });
    linkElement.dispatchEvent(clickEvent);
  }

  convertirBytes(base64) {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
  }

  protected actualizaArchivoNg(key, strimagen) {
    if (this.estaVacio(key)) {
      return;
    }
    this.marchivosngstr[key] = 'data:image/jpg;base64,' + strimagen;
  }

  crearNuevoArchivo() {
    return new Archivo();
  }

  public abrirArchivo(data: any) {
    window.open(data.Url, '_blank');
  }

}
