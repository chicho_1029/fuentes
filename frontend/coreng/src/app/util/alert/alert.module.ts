import { NgModule } from '@angular/core';
import { AlertRoutingModule } from './alert.routing';

import { AlertComponent } from './componentes/alert.component';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';


@NgModule({
  imports: [AlertRoutingModule, DialogModule, MessagesModule],
  declarations: [AlertComponent],
  exports: [AlertComponent],

})
export class AlertModule { }

