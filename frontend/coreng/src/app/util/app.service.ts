import { Injectable } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { RestService } from '../rest.service';
//import { NotificacionService } from '../util/shared/servicioalcliente/notificacion.service';
import { MessageService } from 'primeng/api';
import { Message } from 'primeng/api';
import { MenuItem } from 'primeng/primeng';
import { VerificaAutorizacionService } from '../util/verificaAutorizacion.service';
import { ReloadComponent } from '../seguridad/login/reload.component';
import { HomeComponent } from '../modulos/core/home/home.component';
import { ConsultaComponent } from './shared/consulta.component';


@Injectable()
export class AppService {
  /** Objeto que contine mensajes aplicativos. */
  msgs: Message[] = [];
  /** Menu asociado a un rol del usuario. */
  lmenu: any = [];
  /** false, indica que el usuario no ingresa a la aplicacion, true el usuario esta en la aplicacion y se muestra el menu. */
  public login = false;
  /** Varible que valida la la clave del otp */
  public validarotp = true;
  /** Variable que valida el olvido contrasenia */
  public validarclavetemp = false;
  /** Varible que valida la la clave del otp */
  public cambiopassword = false;
  /** Codigo de modulo activo */
  public cmodulo: any;
  /** Nombre de rol activo */
  public nmodulo: any;
  /** Menu asociado al rol */
  menu: MenuItem[];

  sidebar: any;

  public titulopagina = '';
  public isTransaction = false;

  public mradicacion: any = {};

  /** Rutas por defecto */
  public appRutasDefecto: Routes = [
    { path: '', component: HomeComponent, canActivate: [VerificaAutorizacionService] },
    { path: 'reload', component: ReloadComponent },
    { path: '**', redirectTo: '' },
  ];

  constructor(public router: Router, public restService: RestService,
    public message: MessageService) {
  }

  /** Metodo que se ejecuta en el bootstrap de la aplicacion, o al salir de la aplicacion. */
  public salir(event = null): void {
    this.msgs = [];
    if (event !== null && event !== undefined) {
      event.preventDefault();
    }
    this.restService.logout();
    this.login = false;
  }

  /** Invoca al core para realizar login de la aplicacion. */
  public consultarMenu(Cmodulologin: any, cusuario: any, ccanal: any, token: any) {
    this.msgs = [];
    const rqRol: any = {};
    rqRol.cmodulologin = Cmodulologin;
    rqRol.cusuario = cusuario;
    rqRol.ccanal = ccanal;
    rqRol.token = token;
    rqRol.sesion = true;
    this.mradicacion.Cmodulologin = Cmodulologin;
    this.restService.Consultar(rqRol, true, null, 'Menu')
      .subscribe(
        resp => {
          this.manejaRespuestaMenu(resp);

        },
        error => {
          this.restService.manejoError(error);
        });
  }

  /** Manejo respuesta de ejecucion de login. */
  private manejaRespuestaMenu(resp: any) {
    this.msgs = [];
    if (resp.cod === 'OK') {
      this.setItems(resp.Sidebar);

      this.sidebar = resp.Sidebar;

      this.login = true;
      this.restService.logeado = true;

      this.CargarModulos(resp);
      this.ValidarMenu();
    }

    if (resp.cod !== 'OK') {
      let msg = '';
      msg = resp.cod !== undefined ? msg = msg + resp.cod + ' ' : msg + ' ';
      msg = resp.msgusu !== undefined ? msg = msg + resp.msgusu : msg + '';
      this.restService.llenarMensaje(resp, false);
    }


  }

  public CargarModulos(resp: any) {
    switch (this.mradicacion.Cmodulologin) {
      case 102:
        break;
    }
  }

  public ValidarMenu() {
    let sidebaritem;
    let modulo;
    let transaccion;
    switch (this.mradicacion.Cmodulologin) {
      case 102:
        break;
    }

    for (const i in this.sidebar) {
      if (this.sidebar.hasOwnProperty(i)) {
        const itemaux = this.sidebar[i];
        if (itemaux.Mostrar === true) {
          this.restService.activeTabIndex = (itemaux.Orden);
          if (itemaux.Sidebaractive) {
            this.restService.sidebarActive = true;
          } else {
            this.restService.sidebarActive = false;
          }

          if (itemaux.Cargarpantalla && itemaux.Mostrar) {
            const path = '/' + itemaux.Cmodulo + '-' + itemaux.Ctransaccion;
            this.router.navigate([path], { skipLocationChange: true });
            break;
          }
        }
      }
    }

  }

  public cargarNotificacionesAtencion(ldata: any): any {
    let lnoficacionAtencion: any[];
    lnoficacionAtencion = [];
    for (const i in ldata) {
      if (ldata.hasOwnProperty(i)) {
        const itemaux = ldata[i];
        if (itemaux.Corigen !== 5) {
          lnoficacionAtencion.push(itemaux);
        }
      }
    }
    return lnoficacionAtencion;
  }

  public cargarNotificacionesCentral(ldata: any): any {
    let lnoficacionAtencion: any[];
    lnoficacionAtencion = [];
    for (const i in ldata) {
      if (ldata.hasOwnProperty(i)) {
        const itemaux = ldata[i];
        if (itemaux.Corigen === 5) {
          lnoficacionAtencion.push(itemaux);
        }
      }
    }
    return lnoficacionAtencion;
  }

  setItems = (sidebar: any) => {
    for (const i in sidebar) {
      if (sidebar.hasOwnProperty(i)) {
        const reg: any = sidebar[i];
        if (reg.Essubmenu) {
          const x = reg.lmenu;
          for (const j in x) {
            if (x.hasOwnProperty(j)) {
              this.assignCallback(x[j]);
            }
          }
          reg.lmenu = x[0].items;
        }
      }
    }
  }

  assignCallback(item: MenuItem) {
    if (item.items != null && item.items.length > 0) {
      for (const i in item.items) {
        if (item.items.hasOwnProperty(i)) {
          const reg: any = item.items[i];
          this.assignCallback(reg);
        }
      }
    }
    if (item.target !== '') {
      item.command = (event) => { this.itemClick(event); };
    }
  }

  public itemClick(Event) {
    const item: any[] = Event.item.target.replace('/', '').split('-');

    if (item[0] === '0' && item[1] === '0') {
      return;
    }
    this.titulopagina = Event.item.label;
    this.restService.mradicacion.cmodulo = item[0];
    this.restService.mradicacion.ctransaccion = item[1];
    this.isTransaction = true;
    const path = '/' + item[0] + '-' + item[1];

    let pathreload = '';

    if (sessionStorage.getItem('p') !== undefined && sessionStorage.getItem('p') !== null) {
      pathreload = this.restService.securityLib.Desencriptar(JSON.parse(sessionStorage.getItem('p')).toString(),
        undefined);
    }

    sessionStorage.setItem('p', JSON.stringify(this.restService.securityLib.Encriptar(path.trim(), undefined).toString()));

    if (path === pathreload) {
      this.router.navigate([''], { skipLocationChange: true });
    } else {
      this.router.navigate([path], { skipLocationChange: true });
    }
  }

  public cargarPantallaBandeja(titulopagina: string, modulo: string, transaccion: string) {
    this.titulopagina = titulopagina;
    this.restService.mradicacion.cmodulo = Number(modulo);
    this.restService.mradicacion.ctransaccion = Number(transaccion);
    const path = '/' + modulo + '-' + transaccion;
    let pathreload = '';

    if (sessionStorage.getItem('p') !== undefined && sessionStorage.getItem('p') !== null) {
      pathreload = this.restService.securityLib.Desencriptar(JSON.parse(sessionStorage.getItem('p')).toString(),
        undefined);
    }

    sessionStorage.setItem('p', JSON.stringify(this.restService.securityLib.Encriptar(path.trim(), undefined).toString()));

    if (path === pathreload) {
      this.router.navigate([''], { skipLocationChange: true });
    } else {
      this.router.navigate([path], { skipLocationChange: true });
    }
  }

  public cargarPantallaNotificacion(item: any, titulopagina: string, modulo: string, transaccion: string) {
    this.titulopagina = titulopagina;
    this.restService.mradicacion.cmodulo = Number(modulo);
    this.restService.mradicacion.ctransaccion = Number(transaccion);
    //this.notificacionService.notifyenbandeja = true;
    const path = modulo + '-' + transaccion;
    this.router.navigate([path], {
      skipLocationChange: true, queryParams: {
        notificacion: JSON.stringify(item)
      }
    });
  }
}
