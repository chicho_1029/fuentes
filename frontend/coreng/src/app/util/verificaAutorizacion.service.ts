import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

/**
 * Clase que se encarga de verificar si el usuario ha realizado login en la aplicacion.
 * si no ha realizado un login carga la pagina de login.
 */
@Injectable()
export class VerificaAutorizacionService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // contien los datos minimos con los que se ejecuta una transacción en el core.
    if (sessionStorage.getItem('c') && sessionStorage.getItem('c') !== '') {
      return true;
    }
    if (route.component === undefined || route.component['name'.toString()] !== 'InicioComponent') {
      // si no esta logeado, muestra la pagina de recargar
    }
    sessionStorage.clear();
    return false;
  }
}
